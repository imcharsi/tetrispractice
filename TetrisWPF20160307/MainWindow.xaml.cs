﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TetrisWPF20160307
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Subject<TetrisLibrary.DefinitionMod.GameEvent> recursionSubject = new Subject<TetrisLibrary.DefinitionMod.GameEvent>();
        private Subject<TetrisLibrary.DefinitionMod.MouseMod.MouseEvent> recursionSubject4Mouse = new Subject<TetrisLibrary.DefinitionMod.MouseMod.MouseEvent>();
        private IDisposable disposable = System.Reactive.Disposables.Disposable.Empty;
        private Subject<TetrisLibrary.DefinitionMod.GameEvent> recursionSubject2 = new Subject<TetrisLibrary.DefinitionMod.GameEvent>();
        private Subject<TetrisLibrary.DefinitionMod.MouseMod.MouseEvent> recursionSubject4Mouse2 = new Subject<TetrisLibrary.DefinitionMod.MouseMod.MouseEvent>();
        private IDisposable disposable2 = System.Reactive.Disposables.Disposable.Empty;
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_OnLoaded;
            Unloaded += MainWindow_OnUnloaded;
        }
        /*
    let mapF4KeyEvent (e : KeyEventArgs) : GameEvent = 
        match e.Key with
        | Key.Left -> GameEvent.Movement(Movement.Left)
        | Key.Right -> GameEvent.Movement(Movement.Right)
        | Key.Down -> GameEvent.Movement(Movement.Down)
        | Key.Space -> GameEvent.Movement(Movement.DropDown)
        | Key.Z -> GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
        | Key.X -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | Key.Up -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | _ -> 
            let log = LogMod.Log.Error "mapF4KeyEvent failed: unexpected key."
            GameEvent.NoOp(Some(LogMod.writeLog log (LogMod.emptyLogger)))
    
    let mapF4KeyEvent2 (e : KeyEventArgs) : GameEvent = 
        match e.Key with
        | Key.NumPad4 -> GameEvent.Movement(Movement.Left)
        | Key.NumPad6 -> GameEvent.Movement(Movement.Right)
        | Key.NumPad2 -> GameEvent.Movement(Movement.Down)
        | Key.NumPad0 -> GameEvent.Movement(Movement.DropDown)
        | Key.NumPad1 -> GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
        | Key.NumPad3 -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | Key.NumPad8 -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | _ -> 
            let log = LogMod.Log.Error "mapF4KeyEvent failed: unexpected key."
            GameEvent.NoOp(Some(LogMod.writeLog log (LogMod.emptyLogger)))
            */

        public void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            disposable = TetrisLibrary.SideEffectMod.subscribe(
                recursionSubject, recursionSubject2, recursionSubject4Mouse, startButton, stopButton, this, background, FindResource,
                background.AddVisual, background.RemoveVisual,
                nextBlock.AddVisual, nextBlock.RemoveVisual,
                receivedLines.AddVisual, receivedLines.RemoveVisual, TetrisLibrary.SideEffectMod.mapFKeyEventDelegate);
            disposable2 = TetrisLibrary.SideEffectMod.subscribe(
                recursionSubject2, recursionSubject, recursionSubject4Mouse2, startButton, stopButton, this, background2, FindResource,
                background2.AddVisual, background2.RemoveVisual,
                nextBlock2.AddVisual, nextBlock2.RemoveVisual,
                receivedLines2.AddVisual, receivedLines2.RemoveVisual, TetrisLibrary.SideEffectMod.mapFKeyEvent2Delegate);
        }
        public void MainWindow_OnUnloaded(object sender, RoutedEventArgs e)
        {
            disposable.Dispose();
        }
    }
}
