﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace TetrisWPF20160307
{
    class CustomCanvas : Canvas
    {
        private List<Visual> visualList = new List<Visual>();
        public void RemoveVisual(Visual visual)
        {
            visualList.Remove(visual);
            RemoveVisualChild(visual);
        }
        public void AddVisual(Visual visual)
        {
            visualList.Add(visual);
            AddVisualChild(visual);
        }
        protected override int VisualChildrenCount
        {
            get
            {
                return visualList.Count;
            }
        }
        protected override Visual GetVisualChild(int index)
        {
            return visualList.ElementAt(index);
        }
        public static readonly DependencyProperty GamePanelSizeProperty = DependencyProperty.Register(
          "GamePanelSize",
          typeof(Size),
          typeof(CustomCanvas),
          new FrameworkPropertyMetadata(new Size(0, 0), FrameworkPropertyMetadataOptions.AffectsParentMeasure)
        );
        public Size GamePanelSize
        {
            get { return (Size)GetValue(GamePanelSizeProperty); }
            set { SetValue(GamePanelSizeProperty, value); }
        }
        protected override Size MeasureOverride(Size constraint)
        {
            foreach (UIElement child in this.Children)
            {
                if (child != null)
                    child.Measure(new Size(0, 0));
            }
            Size gamePanelSize = GamePanelSize;
            return base.MeasureOverride(new Size(gamePanelSize.Width * 10, gamePanelSize.Height * 10));
        }
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            foreach (UIElement child in this.Children)
            {
                if (child != null)
                    child.Arrange(new Rect(new Point(0, 0), child.DesiredSize));
            }
            Size gamePanelSize = GamePanelSize;
            /*
            Size size1 = Size.Empty;
            if(gamePanelSize.Width>arrangeSize.Width)
            {
                size1.Width = arrangeSize.Width;
                double x = size1.Width / gamePanelSize.Width;
                size1.Height = x * gamePanelSize.Height;
            }*/
            return new Size(gamePanelSize.Width * 10, gamePanelSize.Height * 10);
        }


    }
}
