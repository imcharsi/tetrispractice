﻿namespace TetrisLibrary

module TestBasic = 
    // coverage 검사는 아래 명령과 같이 하면 된다.
    // https://github.com/OpenCover/opencover
    // https://github.com/OpenCover/opencover/wiki/Usage
    // packages\OpenCover.4.6.519\tools\OpenCover.Console.exe -target:"c:\Program Files (x86)\NUnit.org\nunit-console\nunit3-console.exe" -register:user -targetargs:"--test:CoverageTest.Test.test1,CoverageTest.Test.test3 CoverageTest\bin\Debug\CoverageTest.dll" -filter:"+[*]* -[*]CoverageTest.Mod2 -[*]CoverageTest.Test"
    // 설명에도 나와 있듯이, target 에서는 test runner 를 써야 한다.
    // nunit3-console command option 중 --test 는 실행할 검사를 지정한다.
    // -filter 명령의 +/- 는 coverage 검사에서  coverage 대상을 넣거나 빼겠다는 말이고,
    // [] 안은 assembly, 바로 다음은 namespace 나 class 등이다.
    // f# 함수는 method 로서 다루어지고, module 은 class 로서 다루어진다.
    // 보고서는 아래와 같이 만든다.
    // https://github.com/danielpalme/ReportGenerator
    // packages\ReportGenerator.2.4.4.0\tools\ReportGenerator.exe -reports:"results.xml" -targetdir:coverage
    // 많이 불편하지만, 이와 같이 하면 어디가 빠졌는지 정확하게 찾을 수는 있다.
    open NUnit.Framework
    open DefinitionMod
    open FunctionalUtil
    
    let prepareBlockCell (x : int) (y : int) (blockType : int) (drawingObjectId : Option<int>) : PointRec * BlockCell = 
        (Point x y), 
        (BlockCell.NormalBlockCell({ blockType = blockType
                                     drawingObjectId = drawingObjectId }))
    
    let addBlockCell (point : PointRec, blockCell : BlockCell) (map : BlockMap) : BlockMap = Map.add point blockCell map
    
    [<Test>]
    let testSequenceRotationPoint() : unit = 
        let actual = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map fst
            |> Seq.take 17
            |> Set.ofSeq
        
        let next = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map (snd
                        >> snd
                        >> snd)
            |> Seq.take 17
            |> Set.ofSeq
        
        let prev = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map (snd
                        >> snd
                        >> fst)
            |> Seq.take 17
            |> Set.ofSeq
        
        // 17를 만들고 16개 남는다면, 그 중 하나는 중복이라는 뜻이고, 중복이 있다는 말은, 열에 반복주기가 있다는 뜻이다.
        Assert.AreEqual(16, Set.count actual)
        Assert.AreEqual(next, actual)
        Assert.AreEqual(prev, actual)
        Assert.AreEqual(next, prev)
        let actual = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map fst
            |> Seq.take 17
            |> List.ofSeq
        
        let prev = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map (snd
                        >> snd
                        >> fst)
            |> Seq.skip 1
            |> Seq.take 17
            |> List.ofSeq
        
        // 1,(0,2) 2,(1,3) 3,(2,4) ... 의 fst 와
        // 위의 열에서 skip 1 한 뒤 나머지의 snd 의 fst 인 1 2 3 이 같은지 확인한다.
        // 1 2 3 4 5 ...
        //   1 2 3 4 5 ... 이 열은, 앞 열의 첫번째 좌표 1 을 뺀 나머지 좌표 2 3 4 5 ... 의 바로 앞 좌표를 뜻한다.
        // 따라서, 2 의 바로 앞 좌표는 1, 3 의 바로 앞 좌표는 2 가 된다.
        Assert.AreEqual(prev, actual)
        Assert.AreEqual(Set.ofList prev, Set.ofList actual)
        let actual = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map fst
            |> Seq.skip 1
            |> Seq.take 17
            |> List.ofSeq
        
        let prev = 
            BlockManipulationMod.sequenceRotationPoint 5 (Point 0 0) (Point 0 0)
            |> Seq.map (snd
                        >> snd
                        >> snd)
            |> Seq.take 17
            |> List.ofSeq
        
        // 1,(0,2) 2,(1,3) 3,(2,4) ... 에서 skip 1 한 결과와
        // 위의 열에서 snd 의 snd 인 2 3 4 가 같은지 확인한다.
        //   2 3 4 5 6 ...
        // 2 3 4 5 6 ... 이 열은, 좌표열 1 2 3 4 5 ... 의 바로 뒤 좌표를 뜻한다.
        // 따라서, 1 의 바로 뒤 좌표는 2, 2 의 바로 앞 좌표는 3 가 된다.
        Assert.AreEqual(prev, actual)
        Assert.AreEqual(Set.ofList prev, Set.ofList actual)
        // 3x3 크기에 대해서도 검사한다.
        let actual = 
            BlockManipulationMod.sequenceRotationPoint 3 (Point 1 1) (Point 1 1)
            |> Seq.map fst
            |> Seq.take 9
            |> Set.ofSeq
        
        let next = 
            BlockManipulationMod.sequenceRotationPoint 3 (Point 1 1) (Point 1 1)
            |> Seq.map (snd
                        >> snd
                        >> snd)
            |> Seq.take 9
            |> Set.ofSeq
        
        let prev = 
            BlockManipulationMod.sequenceRotationPoint 3 (Point 1 1) (Point 1 1)
            |> Seq.map (snd
                        >> snd
                        >> fst)
            |> Seq.take 9
            |> Set.ofSeq
        
        Assert.AreEqual(8, Set.count actual)
        Assert.AreEqual(next, actual)
        Assert.AreEqual(prev, actual)
        Assert.AreEqual(next, prev)
    
    [<Test>]
    let testLeft() : unit = 
        // 성공하는 검사.
        // IIII
        // I..I
        // I.1I
        // IIII 를
        // IIII
        // I..I
        // I1.I
        // IIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.left 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
        // 실패하는 검사
        // IIII
        // I..I
        // I1.I
        // IIII 를 왼쪽으로 이동시도한다.
        let currentBlock : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let block, gameState = 
            BlockManipulationMod.left 
            |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | None, _ -> ()
        | _, _ -> Assert.Fail()
        // 성공하는 검사.
        // IIIII
        // I...I
        // I..1I
        // IIIII 를
        // IIIII
        // I...I
        // I1..I
        // IIIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 3 2), Map.empty |> addBlockCell (prepareBlockCell 3 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 3 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 3 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.leftN 2 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
        // 성공하는 검사
        // IIIII
        // I...I
        // I.1.I
        // IIIII 를 2칸 왼쪽으로 이동하여
        // IIIII
        // I...I
        // I1..I
        // IIIII 와 같이 이동시킨다. 1칸만 이동했지만 실패가 아니라 성공이다.
        let currentBlock : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 3 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 3 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let block, gameState = 
            BlockManipulationMod.leftN 2 
            |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testRight() : unit = 
        // 성공하는 검사.
        // IIII
        // I..I
        // I1.I
        // IIII 를
        // IIII
        // I..I
        // I.1I
        // IIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.right 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
        // 실패하는 검사
        // IIII
        // I..I
        // I.1I
        // IIII 를 오른쪽으로 이동시도한다.
        let currentBlock : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let block, gameState = 
            BlockManipulationMod.right 
            |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | None, _ -> ()
        | _, _ -> Assert.Fail()
        // 성공하는 검사.
        // IIIII
        // I...I
        // I1..I
        // IIIII 를
        // IIIII
        // I...I
        // I..1I
        // IIIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 3 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 3 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.rightN 2 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 3 2), Map.empty |> addBlockCell (prepareBlockCell 3 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
        // 성공하는 검사
        // IIIII
        // I...I
        // I.1.I
        // IIIII 를 2칸 오른쪽으로 이동하여
        // IIIII
        // I...I
        // I..1I
        // IIIII 와 같이 이동시킨다. 1칸만 이동했지만 실패가 아니라 성공이다.
        let currentBlock : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 3 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 3 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let block, gameState = 
            BlockManipulationMod.rightN 2 
            |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | Some(block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 3 2), Map.empty |> addBlockCell (prepareBlockCell 3 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testDownN() : unit = 
        // 성공하는 검사.
        // IIII
        // I1.I
        // I..I
        // IIII 를
        // IIII
        // I..I
        // I1.I
        // IIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 1 1), Map.empty |> addBlockCell (prepareBlockCell 1 1 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        // 바닥에 닿지 않기.
        match BlockManipulationMod.downN 1 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(false, block), ((GameState.Play(playRec), io), logger) -> 
            BlockManipulationMod.printBlock 1 block
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
        // 바닥에 닿기.
        match BlockManipulationMod.downN 2 
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(true, block), ((GameState.Play(playRec), io), logger) -> 
            BlockManipulationMod.printBlock 1 block
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testMapF4TryLeftRightRotation() : unit = 
        // 성공하는 검사.
        let currentBlock : Block = ((Point 2 2), Map.empty |> addBlockCell (prepareBlockCell 2 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.left
              |> StateOptionMod.bind MovementMod.mapF4TryLeftRightRotation
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let expected = [ IOMod.IOCommand.MoveBackground(prepareBlockCell 1 2 1 (Some 1)) ] |> Set.ofList
            Assert.AreEqual(expected, 
                            io ([], ())
                            |> snd
                            |> fst
                            |> Set.ofList)
        | _, _ -> Assert.Fail()
        // 실패하는 검사.
        let currentBlock : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.left
              |> StateOptionMod.bind MovementMod.mapF4TryLeftRightRotation
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | None, _ -> ()
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testMapF4TryLeftRightRotationAndKeepState() : unit = 
        // 중간단계에서는 실패했지만 결론은 성공하는 검사.
        let currentBlock : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        match BlockManipulationMod.left
              |> StateOptionMod.bind MovementMod.mapF4TryLeftRightRotation
              |> StateOptionMod.keepStateAndSomeness (fun _ -> ())
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            Assert.IsTrue(List.isEmpty (io ([], ())
                                        |> snd
                                        |> fst))
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testSplitFilledResultAndNotFilledResult() : unit = 
        // 1111
        // I.3I
        // I.4I
        // I5.I
        // I6.I
        // 1111 를
        // 1111
        // I.3I
        // I14I
        // I5.I
        // I62I
        // 1111 와 같이 이동시켜 2번 줄과 4번줄이 깨지는 줄이고, 1번줄과 3번 줄이 남는 줄이 되어야 한다.
        let currentBlock : Block = 
            ((Point 1 1), 
             Map.empty
             |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
             |> addBlockCell (prepareBlockCell 2 4 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 4
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 4) Map.empty
                  |> addBlockCell (prepareBlockCell 2 1 3 (Some 3))
                  |> addBlockCell (prepareBlockCell 2 2 4 (Some 4))
                  |> addBlockCell (prepareBlockCell 1 3 5 (Some 5))
                  |> addBlockCell (prepareBlockCell 1 4 6 (Some 6))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let unionedBackground = FSharpx.Collections.Map.union detailState.background (snd currentBlock)
        let prepareLookupMap = MovementMod.prepareLookupMap detailState (List.rev [ 1..(detailState.backgroundSize.y) ])
        let filled, notFilled = MovementMod.splitFilledResultAndNotFilledResult unionedBackground detailState
        let filledExpected = [ 4; 2 ]
        let notFilledExpected = [ 3; 1 ]
        Assert.AreEqual(filledExpected, filled)
        Assert.AreEqual(notFilledExpected, notFilled)
    
    [<Test>]
    let testCalculateRemovedLines() : unit = 
        // 1111
        // I.3I
        // I.4I
        // I5.I
        // I6.I
        // 1111 를
        // 1111
        // I.3I
        // I14I
        // I5.I
        // I62I
        // 1111 와 같이 이동시켜
        // 1111
        // 1..1
        // 1..1
        // I.3I
        // I5.I
        // 1111 가 남아야 하고, 
        // 1111
        // I..I
        // I..I
        // I.4I
        // I6.I
        // 1111 와 같이 넘어갈 줄이 만들어 져야 하고
        // [1 4 6 2] 번 block cell 이 지워져야 한다.
        let currentBlock : Block = 
            ((Point 1 1), 
             Map.empty
             |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
             |> addBlockCell (prepareBlockCell 2 4 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 4
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 4) Map.empty
                  |> addBlockCell (prepareBlockCell 2 1 3 (Some 3))
                  |> addBlockCell (prepareBlockCell 2 2 4 (Some 4))
                  |> addBlockCell (prepareBlockCell 1 3 5 (Some 5))
                  |> addBlockCell (prepareBlockCell 1 4 6 (Some 6))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let unionedBackground = FSharpx.Collections.Map.union detailState.background (snd currentBlock)
        let (beClearedLines, beRemovedLines), newBackground = 
            MovementMod.calculateRemovedLines unionedBackground detailState
        
        let beClearedLinesExpected : BlockList = 
            [ (prepareBlockCell 1 4 6 (Some 6))
              (prepareBlockCell 2 3 4 (Some 4)) ]
        
        // drawing object 를 지우는 단계에서는 좌표는 필요없고, object id 만 있으면 된다.
        let beRemovedLinesExpected : List<BlockCell> = 
            [ (prepareBlockCell 1 2 1 (Some 1))
              (prepareBlockCell 2 2 4 (Some 4))
              (prepareBlockCell 1 4 6 (Some 6))
              (prepareBlockCell 2 4 2 (Some 2)) ]
            |> List.map snd
        
        let newBackgroundExpected : BlockMap = 
            BlockManipulationMod.drawBackgroundBorder (Point 2 4) Map.empty
            |> addBlockCell (prepareBlockCell 2 3 3 (Some 3))
            |> addBlockCell (prepareBlockCell 1 4 5 (Some 5))
        
        Assert.AreEqual(Set.ofList beClearedLinesExpected, Set.ofList beClearedLines)
        Assert.AreEqual(Set.ofList beRemovedLinesExpected, 
                        beRemovedLines
                        |> List.map snd
                        |> Set.ofList)
        Assert.AreEqual
            (newBackgroundExpected, BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize newBackground)
    
    [<Test>]
    let testFoldF4TryDown() : unit = 
        // 1111
        // I.3I
        // I.4I
        // I5.I
        // I6.I
        // 1111 를
        // 1111
        // I.3I
        // I14I
        // I5.I
        // I62I
        // 1111 와 같이 이동시켜 drawing command 의 실행결과 검사용 pixel map 에
        // 1111
        // 1..1
        // 1..1
        // I.3I
        // I5.I
        // 1111 가 남아야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 4
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 4) Map.empty
                  |> addBlockCell (prepareBlockCell 2 1 3 (Some 3))
                  |> addBlockCell (prepareBlockCell 2 2 4 (Some 4))
                  |> addBlockCell (prepareBlockCell 1 3 5 (Some 5))
                  |> addBlockCell (prepareBlockCell 1 4 6 (Some 6))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let beClearedLinesExpected : BlockList = 
            [ (prepareBlockCell 1 4 6 (Some 6))
              (prepareBlockCell 2 3 4 (Some 4)) ]
        
        let beRemovedLinesExpected : BlockList = 
            [ (prepareBlockCell 1 2 1 (Some 1))
              (prepareBlockCell 2 2 4 (Some 4))
              (prepareBlockCell 1 4 6 (Some 6))
              (prepareBlockCell 2 4 2 (Some 2)) ]
        
        let newBackgroundExpected : BlockMap = 
            Map.empty
            |> addBlockCell (prepareBlockCell 2 3 3 (Some 3))
            |> addBlockCell (prepareBlockCell 1 4 5 (Some 5))
        
        let pixelMap = 
            Map.empty
            |> Map.add 3 (prepareBlockCell 2 1 3 (Some 3))
            |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
            |> Map.add 4 (prepareBlockCell 2 2 4 (Some 4))
            |> Map.add 5 (prepareBlockCell 1 3 5 (Some 5))
            |> Map.add 2 (prepareBlockCell 2 4 2 (Some 2))
            |> Map.add 6 (prepareBlockCell 1 4 6 (Some 6))
        
        // 깨진 줄의 벽돌 4개는 지워지고, 남은 줄의 벽돌 2개는 이동한 결과는 아래와 같다.
        let pixelMapExpected = 
            Map.empty
            |> Map.add 3 (prepareBlockCell 2 3 3 (Some 3))
            |> Map.add 5 (prepareBlockCell 1 4 5 (Some 5))
        
        // 넘겨주는 줄에는 drawing object id 가 지워져야 한다.
        let beClearedLinesExpected : BlockList = 
            [ (prepareBlockCell 1 4 6 None)
              (prepareBlockCell 2 3 4 None) ]
        
        let gameState = 
            MovementMod.foldF4TryDown Map.empty detailState IOMod.nothingToDo LogMod.emptyLogger 
                ((beClearedLinesExpected, beRemovedLinesExpected), newBackgroundExpected)
        match gameState with
        | Some(_), ((GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger) -> 
            let ioCommandList = 
                io ([], ())
                |> snd
                |> fst
            
            let newPixelMap = List.fold BlockManipulationMod.processIOCommand4Background pixelMap ioCommandList
            // 모든 drawing command 를 일일이 나열하듯이 검사하면 번거롭다. 차라리 pixel map 에 모든 점을 찍은 후 결과만 보는 식으로 하는 것이 낫겠다.
            Assert.AreEqual(pixelMapExpected, newPixelMap)
            Assert.IsTrue(List.exists ((=) (IOMod.IOCommand.Timer(TimerCommand.SetAsImmediate))) ioCommandList)
            ioCommandList
            |> List.exists (fun ioCommand -> 
                   match ioCommand with
                   | IOMod.IOCommand.TransferLines(transferLines) -> 
                       Set.ofList transferLines = Set.ofList beClearedLinesExpected
                   | x -> false)
            |> Assert.IsTrue
        | _ -> Assert.Fail()
    
    [<Test>]
    let testBindF4TryDown() : unit = 
        // 성공하는 검사.
        // IIII
        // I12I
        // I.3I
        // IIII 를
        // IIII
        // I.2I
        // I13I
        // IIII 와 같이 이동시킨다.
        let currentBlock : Block = ((Point 1 1), Map.empty |> addBlockCell (prepareBlockCell 1 1 1 (Some 1)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
                  |> addBlockCell (prepareBlockCell 2 1 2 (Some 2))
                  |> addBlockCell (prepareBlockCell 2 2 3 (Some 3))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = currentBlock
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let pixelMap = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
            |> Map.add 2 (prepareBlockCell 2 1 2 (Some 2))
            |> Map.add 3 (prepareBlockCell 2 2 3 (Some 3))
        
        let pixelMapExpected = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
            |> Map.add 2 (prepareBlockCell 2 1 2 (Some 2))
            |> Map.add 3 (prepareBlockCell 2 2 3 (Some 3))
        
        // 바닥에 닿지 않기. 여기서는 예상낙하지점을 계산하지 않기 때문에, 이에 관한 검사도 하지 않는다.
        // 바닥에 닿지 않는 경우이므로, 결국 왼쪽오른쪽/회전과 같은 경우로서 다루어지고,
        // 따라서 expectedBlock 가 이후에 실행되면서 예상낙하지점을 계산하게 된다.
        match BlockManipulationMod.downN 1
              |> StateOptionMod.bind MovementMod.bindF4TryDown
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background pixelMap (io ([], ())
                                                                                     |> snd
                                                                                     |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
        | _, _ -> Assert.Fail()
        // 성공하는 검사.
        // IIII
        // I12I
        // I.3I
        // IIII 를
        // IIII
        // I..I
        // I.2I
        // IIII 와 같이 이동시킨다.
        // 바닥에 닿기.
        // 검사 순서가 약간 이상한데, 줄이 깨졌을 경우 예상낙하지점 drawing object 가 같이 지워지는지 확인하는 검사는 여기서 해야 한다.
        // 왜 검사순서가 이상하다고 생각했는가.
        // expectedBlock 는 tryLeft... 나 tryDown 이후에 실행되므로,
        // tryDown 다음의 expectedBlock 에서 예상낙하지점에 관한 drawing object 가 처리될 거라 예상하는 것은 당연한데,
        // 예상낙하지점의 drawing object 삭제는 expectedBlock 에서 다루어지지 않고, 줄 깨기 처리단계에서 다루어진다.
        // expectedBlock 은, 내려오고 있는 벽돌의 예상낙하지점을 계산하는 단계이므로, 내려오고 있는 벽돌이 없으면 계산을 하지 않는다.
        // 줄이 깨졌고 그래서 내려오고 있는 벽돌이 없으므로, expectedBlock 의 구상의도에 맞는 예상낙하지점에 관한 처리를 할 수는 없다.
        // 그래서, 줄 깨기 단계에서 예상낙하지점 drawing object 를 지우게 되고
        // 이에 관한 검사를 예상낙하지점계산에 관한 검사 일반에서 하지 않고 줄깨기 검사에서 같이 하는 것이다.
        // 요약하면, 예상낙하지점계산에 관한 검사를, 예상낙하지점계산에 관한 검사 일반에서 하지 않았기 때문에, 순서가 이상하다는 것이다.
        let expectedBlockCell, detailState = 
            BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList (snd currentBlock) detailState
        let playRec : PlayRec = { playRec with expectedBlockCell = expectedBlockCell }
        let pixelMapExpected = Map.empty |> Map.add 2 (prepareBlockCell 2 2 2 (Some 2))
        let expectedPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
        let expectedPixelMapExpected = Map.empty
        match BlockManipulationMod.downN 2
              |> StateOptionMod.bind MovementMod.bindF4TryDown
              |> StateMod.run (((GameState.Play playRec), IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background pixelMap (io ([], ())
                                                                                     |> snd
                                                                                     |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
            let newExpectedPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ExpectedBlock expectedPixelMap (io ([], ())
                                                                                                |> snd
                                                                                                |> fst)
            Assert.AreEqual(expectedPixelMapExpected, newExpectedPixelMap)
        | _, _ -> Assert.Fail()
    
    [<Test>]
    let testExpectedBlock() : unit = 
        // 1111
        // I2.I
        // I..I
        // I1.I
        // I..I
        // I..I
        // 1111 의 예상낙하지점은
        // 1111
        // I..I
        // I2.I
        // I..I
        // I..I
        // I..I
        // 1111 가 되어야 한다.
        let currentBlock : Block = ((Point 1 1), Map.empty |> addBlockCell (prepareBlockCell 1 1 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 5
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 5) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 3 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let expectedBlockCell, detailState = 
            BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList (snd currentBlock) detailState
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = expectedBlockCell
                             detailState = detailState })
        
        let pixelMap = Map.empty |> Map.add 3 (prepareBlockCell 0 0 2 (Some 3))
        let pixelMapExpected = Map.empty |> Map.add 3 (prepareBlockCell 1 2 2 (Some 3))
        let _, gameState = 
            MovementMod.expectedBlock IOMod.IOCommand.MoveExpectedBlock 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match gameState with
        | ((GameState.Play(playRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ExpectedBlock pixelMap (io ([], ())
                                                                                        |> snd
                                                                                        |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
        | _ -> Assert.Fail()
        // 1111
        // I.2I
        // I..I
        // I1.I
        // I..I
        // I..I
        // 1111 의 예상낙하지점은
        // 1111
        // I..I
        // I..I
        // I..I
        // I..I
        // I.2I
        // 1111 가 되어야 한다.
        let currentBlock : Block = ((Point 2 1), Map.empty |> addBlockCell (prepareBlockCell 2 1 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 5
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 5) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 3 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let expectedBlockCell, detailState = 
            BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList (snd currentBlock) detailState
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = expectedBlockCell
                             detailState = detailState })
        
        let pixelMap = Map.empty |> Map.add 3 (prepareBlockCell 0 0 2 (Some 3))
        let pixelMapExpected = Map.empty |> Map.add 3 (prepareBlockCell 2 5 2 (Some 3))
        let _, gameState = 
            MovementMod.expectedBlock IOMod.IOCommand.MoveExpectedBlock 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match gameState with
        | ((GameState.Play(playRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ExpectedBlock pixelMap (io ([], ())
                                                                                        |> snd
                                                                                        |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
        | _ -> Assert.Fail()
        // 1111
        // I..I
        // I..I
        // I1.I
        // I2.I
        // I..I
        // 1111 의 예상낙하지점은
        // 1111
        // I..I
        // I..I
        // I..I
        // I..I
        // I2.I
        // 1111 가 되어야 한다.
        let currentBlock : Block = ((Point 1 4), Map.empty |> addBlockCell (prepareBlockCell 1 4 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 5
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 5) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 3 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let expectedBlockCell, detailState = 
            BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList (snd currentBlock) detailState
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = expectedBlockCell
                             detailState = detailState })
        
        let pixelMap = Map.empty |> Map.add 3 (prepareBlockCell 0 0 2 (Some 3))
        let pixelMapExpected = Map.empty |> Map.add 3 (prepareBlockCell 1 5 2 (Some 3))
        let _, gameState = 
            MovementMod.expectedBlock IOMod.IOCommand.MoveExpectedBlock 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match gameState with
        | ((GameState.Play(playRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ExpectedBlock pixelMap (io ([], ())
                                                                                        |> snd
                                                                                        |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
        | _ -> Assert.Fail()
        // 새 벽돌을 만드는 단계에서 예상낙하지점은 그려지지 않았으므로, 새로 그려져야 한다.
        let currentBlock : Block = ((Point 1 4), Map.empty |> addBlockCell (prepareBlockCell 1 4 2 (Some 2)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 5
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 5) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 3 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let expectedBlockCell, detailState = 
            BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList (snd currentBlock) detailState
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = expectedBlockCell
                             detailState = detailState })
        
        let pixelMap = Map.empty
        let pixelMapExpected = Map.empty |> Map.add 3 (prepareBlockCell 1 5 2 (Some 3))
        let _, gameState = 
            MovementMod.expectedBlock IOMod.IOCommand.AddExpectedBlock 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match gameState with
        | ((GameState.Play(playRec), io), logger) -> 
            let newPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ExpectedBlock pixelMap (io ([], ())
                                                                                        |> snd
                                                                                        |> fst)
            Assert.AreEqual(pixelMapExpected, newPixelMap)
        | _ -> Assert.Fail()
        // PlayNoCurrentBlock 상태에서 expectedBlock 을 실행하면 실패함을 검사해야 한다.
        let gameStateExpected = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        match MovementMod.expectedBlock IOMod.IOCommand.MoveExpectedBlock 
              |> StateMod.run ((gameStateExpected, IOMod.nothingToDo), LogMod.emptyLogger) with
        | None, _ -> ()
        | _ -> Assert.Fail()
        // PlayNoCurrentBlock 상태에서 expectedBlock 을 실행하지만 상태유지에 의해 상태가 복구됨을 검사해야 한다.
        let gameStateExpected = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        match MovementMod.expectedBlock IOMod.IOCommand.AddExpectedBlock
              |> StateOptionMod.keepStateAndSomeness (fun _ -> ())
              |> StateMod.run ((gameStateExpected, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((gameState, io), logger) -> Assert.AreEqual(gameStateExpected, gameState)
        | _ -> Assert.Fail()
    
    [<Test>]
    let testRotationPointMap() : unit = 
        seq { yield Point }
        |> SeqMod.apply (seq { 0..4 })
        |> SeqMod.apply (seq { 0..4 })
        |> Seq.forall (fun point -> Map.containsKey point BlockManipulationMod.rotationPointMap)
        |> Assert.IsTrue
    
    [<Test>]
    let testFindSingleRotationArea() : unit = 
        match BlockManipulationMod.findSingleRotationArea BlockManipulationMod.rotationPointMap 
                  RotationDirection.Clockwise (Point 0 0) |> StateMod.run ((), LogMod.emptyLogger) with
        | Some(lastPoint, pointList), (_, logger) -> 
            Assert.AreEqual((Point 4 0), lastPoint)
            let expectedPointSet = 
                seq { yield Point }
                |> SeqMod.apply (seq { 0..3 })
                |> SeqMod.apply (seq { yield 0 })
                |> Seq.take 4
                |> Set.ofSeq
            Assert.AreEqual(expectedPointSet, Set.ofList pointList)
        | _ -> Assert.Fail()
        match BlockManipulationMod.findSingleRotationArea BlockManipulationMod.rotationPointMap 
                  RotationDirection.Clockwise (Point 1 1) |> StateMod.run ((), LogMod.emptyLogger) with
        | Some(lastPoint, pointList), (_, logger) -> 
            Assert.AreEqual((Point 3 1), lastPoint)
            let expectedPointSet = 
                seq { yield Point }
                |> SeqMod.apply (seq { 1..2 })
                |> SeqMod.apply (seq { yield 1 })
                |> Seq.take 2
                |> Set.ofSeq
            Assert.AreEqual(expectedPointSet, Set.ofList pointList)
        | _ -> Assert.Fail() // TODO 우선, 회전기능 자체와 이에 대한 검사, 줄 넘겨받기 기능에 관한 검사를 이어서 하기.
    
    [<Test>]
    let testFindAllRotationArea() : unit = 
        let point : PointRec = Point 1 1
        
        let blockMap : BlockMap = 
            Map.empty
            |> Map.add (Point 1 1) BlockCell.InvisibleBorder
            |> Map.add (Point 2 2) BlockCell.InvisibleBorder
            |> Map.add (Point 3 3) BlockCell.InvisibleBorder
        match BlockManipulationMod.findAllRotationArea BlockManipulationMod.rotationPointMap RotationDirection.Clockwise 
                  (point, blockMap) |> StateMod.run ((), LogMod.emptyLogger) with
        | Some(map), (_, logger) -> 
            match Map.tryFind (Point 1 1) map with
            | Some(lastPoint, pointList) -> 
                Assert.AreEqual((Point 5 1), lastPoint)
                let expectedPointSet = 
                    seq { yield Point }
                    |> SeqMod.apply (seq { 1..4 })
                    |> SeqMod.apply (seq { yield 1 })
                    |> Seq.take 4
                    |> Set.ofSeq
                Assert.AreEqual(expectedPointSet, Set.ofList pointList)
            | None -> Assert.Fail()
            match Map.tryFind (Point 2 2) map with
            | Some(lastPoint, pointList) -> 
                Assert.AreEqual((Point 4 2), lastPoint)
                let expectedPointSet = 
                    seq { yield Point }
                    |> SeqMod.apply (seq { 2..3 })
                    |> SeqMod.apply (seq { yield 2 })
                    |> Seq.take 2
                    |> Set.ofSeq
                Assert.AreEqual(expectedPointSet, Set.ofList pointList)
            | None -> Assert.Fail()
            Map.tryFind (Point 3 3) map
            |> Option.isSome
            |> Assert.IsTrue
        | _ -> Assert.Fail()
    
    [<Test>]
    let testRotate() : unit = 
        // internalRotate 에 대한 검사는 생략한다.
        // 원래는, internalRotate 내용을 rotate 안에 썼었는데, generateNextBlock 에서 회전기능이 필요해서 분리했다.
        // internalRotate 를 제외하면 rotate 에서는 사실상 하는 것이 없다.
        // 따라서, 이 검사는 사실상 internalRotate 에 관한 검사이다.
        // ..1..
        // ..1..
        // ..1..
        // ..1..
        // ..... 을 
        // .2...
        // .....
        // .....
        // .....
        // ..... 배경에 대해 시계방향으로 회전하여
        // .2...
        // .....
        // .1111
        // .....
        // ..... 이 되어야 한다.
        let currentBlock : Block = 
            ((Point 3 3), 
             Map.empty
             |> addBlockCell (prepareBlockCell 5 3 1 (Some 1))
             |> addBlockCell (prepareBlockCell 5 4 1 (Some 2))
             |> addBlockCell (prepareBlockCell 5 5 1 (Some 3))
             |> addBlockCell (prepareBlockCell 5 6 1 (Some 4)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 10 10
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 10 10) Map.empty 
                  |> addBlockCell (prepareBlockCell 4 3 2 (Some 5))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 6
              receivedLines = [] }
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = []
                             detailState = detailState })
        
        let blockExpected : Block = 
            ((Point 3 3), 
             Map.empty
             |> addBlockCell (prepareBlockCell 7 5 1 (Some 1))
             |> addBlockCell (prepareBlockCell 6 5 1 (Some 2))
             |> addBlockCell (prepareBlockCell 5 5 1 (Some 3))
             |> addBlockCell (prepareBlockCell 4 5 1 (Some 4)))
        
        let block, gameState = 
            BlockManipulationMod.rotate BlockManipulationMod.rotationPointMap RotationDirection.Clockwise 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | Some(block), ((GameState.Play(playRec), io), logger) -> Assert.AreEqual(blockExpected, block)
        | _ -> Assert.Fail()
        // ..1..
        // ..1..
        // ..1..
        // ..1..
        // ..... 을 
        // ...2.
        // .....
        // .....
        // .....
        // ..... 배경에 대해 시계방향으로 회전하여 실패해야 한다.
        let currentBlock : Block = 
            ((Point 3 3), 
             Map.empty
             |> addBlockCell (prepareBlockCell 5 3 1 (Some 1))
             |> addBlockCell (prepareBlockCell 5 4 1 (Some 2))
             |> addBlockCell (prepareBlockCell 5 5 1 (Some 3))
             |> addBlockCell (prepareBlockCell 5 6 1 (Some 4)))
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 10 10
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 10 10) Map.empty 
                  |> addBlockCell (prepareBlockCell 6 3 2 (Some 5))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 6
              receivedLines = [] }
        
        let gameState = 
            GameState.Play({ currentBlock = currentBlock
                             nextBlock = Map.empty
                             expectedBlockCell = []
                             detailState = detailState })
        
        let block, gameState = 
            BlockManipulationMod.rotate BlockManipulationMod.rotationPointMap RotationDirection.Clockwise 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match block, gameState with
        | Some(block), ((GameState.Play(playRec), io), logger) -> Assert.Fail()
        | _ -> ()
    
    [<Test>]
    let testSplitBackgroundAndOverflow() : unit = 
        // IIII
        // I2.I
        // I1.I
        // IIII 에서 
        // ....
        // ....
        // .3..
        // .... 을 채워 올려서
        // IIII
        // I1.I
        // I3.I
        // IIII 가 되어야 한다.
        let receivedLines = 
            [ (prepareBlockCell 1 2 1 (Some 1))
              (prepareBlockCell 1 1 2 (Some 2)) ]
        
        let backgroundSize = Point 2 2
        let newReceivedLines = [ (prepareBlockCell 1 2 3 None) ]
        let remainedReceivedLinesExpected = [ (prepareBlockCell 1 1 1 (Some 1)) ] |> Map.ofList
        let remainedReceivedLines, _ = 
            TimeElapsedMod.splitBackgroundAndOverflow (Map.ofList receivedLines) backgroundSize newReceivedLines 
                IOMod.IOCommand.RemoveReceivedLines
        // drawing command 검사는 mergeReceivedLinesAndNewReceivedLines 에서 같이 한다.
        Assert.AreEqual(remainedReceivedLinesExpected, remainedReceivedLines)
    
    [<Test>]
    let testMergeReceivedLinesAndNewReceivedLines() : unit = 
        // IIII
        // I2.I
        // I.1I
        // IIII 에서 
        // ....
        // ....
        // .3..
        // .... 을 채워 올려서
        // IIII
        // I.1I
        // I3.I
        // IIII 가 되어야 한다.
        let receivedLines = 
            [ (prepareBlockCell 2 2 1 (Some 1))
              (prepareBlockCell 1 1 2 (Some 2)) ]
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = receivedLines }
        
        let newReceivedLines = [ (prepareBlockCell 1 2 3 None) ]
        
        let receivedLinesExpected = 
            [ (prepareBlockCell 2 1 1 (Some 1))
              (prepareBlockCell 1 2 3 (Some 3)) ]
        
        let pixelMap = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 2 2 1 (Some 1))
            |> Map.add 2 (prepareBlockCell 1 1 2 (Some 2))
        
        let pixelMapExpected = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 2 1 1 (Some 1))
            |> Map.add 3 (prepareBlockCell 1 2 3 (Some 3))
        
        let actualDetailState, io = 
            ReceivedLinesMod.mergeReceivedLinesAndNewReceivedLines detailState IOMod.nothingToDo newReceivedLines
        Assert.AreEqual(receivedLinesExpected |> Set.ofList, actualDetailState.receivedLines |> Set.ofList)
        let pixelMapActual = 
            List.fold BlockManipulationMod.processIOCommand4ReceivedLines pixelMap (io ([], ())
                                                                                    |> snd
                                                                                    |> fst)
        Assert.AreEqual(pixelMapExpected, pixelMapActual)
    
    [<Test>]
    let testTryReceivedLines() : unit = 
        // 단순히, 예상된 상태가 입력되었는지만 검사한다.
        let receivedLines = 
            [ (prepareBlockCell 2 2 1 (Some 1))
              (prepareBlockCell 1 1 2 (Some 2)) ]
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = receivedLines }
        
        let newReceivedLines = [ (prepareBlockCell 1 2 3 None) ]
        
        let gameState = 
            GameState.Play({ currentBlock = ((Point 0 0), Map.empty)
                             nextBlock = Map.empty
                             expectedBlockCell = []
                             detailState = detailState })
        match ReceivedLinesMod.tryReceivedLines (GameEvent.ReceivedLines(newReceivedLines)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(_), io), logger) -> ()
        | _ -> Assert.Fail()
        match ReceivedLinesMod.tryReceivedLines (GameEvent.Movement(Movement.Left)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), _ -> Assert.Fail()
        | _, _ -> ()
        let gameState = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        
        let eitherUnit, gameStateResult = 
            ReceivedLinesMod.tryReceivedLines (GameEvent.ReceivedLines(newReceivedLines)) 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match eitherUnit, gameStateResult with
        | Some(_), ((GameState.PlayNoCurrentBlock(_), io), logger) -> ()
        | _ -> Assert.Fail()
        let eitherUnit, gameStateResult = 
            ReceivedLinesMod.tryReceivedLines (GameEvent.Movement(Movement.Left)) 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match eitherUnit, gameStateResult with
        | Some(_), _ -> Assert.Fail()
        | None, _ -> ()
        let gameState = 
            GameState.Stop({ currentBlock = None
                             detailState = detailState })
        
        let eitherUnit, gameStateResult = 
            ReceivedLinesMod.tryReceivedLines (GameEvent.Movement(Movement.Left)) 
            |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger)
        match eitherUnit, gameStateResult with
        | Some(_), _ -> Assert.Fail()
        | None, _ -> ()
    
    [<Test>]
    let testPushReceivedLines() : unit = 
        // IIII
        // I..I
        // I1.I
        // IIII 에, 쌓아놓은 줄
        // ....
        // ....
        // ..2.
        // .... 를 밑에서 채워넣어서
        // IIII
        // I1.I
        // I.2I
        // IIII 가 되고, 쌓아놓은 줄은 없어야 한다.
        let receivedLines = [ (prepareBlockCell 2 2 2 (Some 2)) ]
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = receivedLines }
        
        let gameState = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        
        let backgroundPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
        
        let backgroundPixelMapExpected = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 1 1 1 (Some 1))
            |> Map.add 3 (prepareBlockCell 2 2 2 (Some 3)) // 쌓은줄에서 배경으로 옮기면 drawing object id 를 새로 할당한다.
        
        let receivedLinesPixelMap = Map.empty |> Map.add 2 (prepareBlockCell 2 2 2 (Some 2))
        let receivedLinesPixelMapExpected = Map.empty
        match TimeElapsedMod.pushReceivedLines GameEvent.TimeElapsed 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger) -> 
            let backgroundPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            // 배경 그리기 검사.
            Assert.AreEqual(backgroundPixelMapExpected, backgroundPixelMapActual)
            let receivedLinesPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4ReceivedLines receivedLinesPixelMap (io ([], ())
                                                                                                     |> snd
                                                                                                     |> fst)
            // 쌓은 줄 그리기 검사.
            Assert.AreEqual(receivedLinesPixelMapExpected, receivedLinesPixelMapActual)
            // 배경 검사. 배경과 배경 그리기는 다른데, 배경은 이동가능여부를 계산하는 자료이다. 배경을 그대로 그리기에 쓰지 않는다.
            Assert.AreEqual
                (BlockManipulationMod.drawBackgroundBorder playNoCurrentBlockRec.detailState.backgroundSize 
                     (FSharpx.Collections.Map.valueList backgroundPixelMapActual |> Map.ofList), 
                 playNoCurrentBlockRec.detailState.background)
            // 쌓은 줄 검사. 쌓은 줄 그리기 역시 배경 그리기와 마찬가지이다.
            Assert.IsTrue(List.isEmpty playNoCurrentBlockRec.detailState.receivedLines)
        | _, _ -> ()
        // IIII
        // I.3I
        // I1.I
        // IIII 에, 쌓아놓은 줄
        // ....
        // ....
        // ..2.
        // .... 를 밑에서 채워넣어서
        // IIII
        // I1.I
        // I.2I
        // IIII 가 되고, 게임을 끝내면서 쌓아놓은 줄은 없어야 한다.
        let receivedLines = [ (prepareBlockCell 2 2 2 (Some 2)) ]
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
                  |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
                  |> addBlockCell (prepareBlockCell 2 1 3 (Some 3))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 4
              receivedLines = receivedLines }
        
        let gameState = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        
        let backgroundPixelMap = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
            |> Map.add 3 (prepareBlockCell 2 1 3 (Some 3))
        
        let backgroundPixelMapExpected = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 1 1 1 (Some 1))
            |> Map.add 4 (prepareBlockCell 2 2 2 (Some 4))
        
        let receivedLinesPixelMap = Map.empty |> Map.add 2 (prepareBlockCell 2 2 2 (Some 2))
        let receivedLinesPixelMapExpected = Map.empty
        match TimeElapsedMod.pushReceivedLines GameEvent.TimeElapsed 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Stop(stopRec), io), logger) -> 
            let backgroundPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, backgroundPixelMapActual)
            let receivedLinesPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4ReceivedLines receivedLinesPixelMap (io ([], ())
                                                                                                     |> snd
                                                                                                     |> fst)
            Assert.AreEqual(receivedLinesPixelMapExpected, receivedLinesPixelMapActual)
            Assert.AreEqual
                (BlockManipulationMod.drawBackgroundBorder stopRec.detailState.backgroundSize 
                     (FSharpx.Collections.Map.valueList backgroundPixelMapActual |> Map.ofList), 
                 stopRec.detailState.background)
            Assert.IsTrue(List.isEmpty stopRec.detailState.receivedLines)
        | _, _ -> ()
        // IIII
        // I..I
        // I1.I
        // IIII 에, 쌓아놓은 줄
        // ....
        // .3..
        // ..2.
        // .... 를 밑에서 채워넣어서
        // IIII
        // I3.I
        // I.2I
        // IIII 가 되고, 게임을 끝내면서 쌓아놓은 줄은 없어야 한다.
        let receivedLines = 
            [ (prepareBlockCell 2 2 2 (Some 2))
              (prepareBlockCell 1 1 3 (Some 3)) ]
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 4
              receivedLines = receivedLines }
        
        let gameState = 
            GameState.PlayNoCurrentBlock({ nextBlock = Map.empty
                                           detailState = detailState })
        
        let backgroundPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
        
        let backgroundPixelMapExpected = 
            Map.empty
            |> Map.add 4 (prepareBlockCell 1 1 3 (Some 4))
            |> Map.add 5 (prepareBlockCell 2 2 2 (Some 5))
        
        let receivedLinesPixelMap = 
            Map.empty
            |> Map.add 2 (prepareBlockCell 2 2 2 (Some 2))
            |> Map.add 3 (prepareBlockCell 1 1 3 (Some 3))
        
        let receivedLinesPixelMapExpected = Map.empty
        match TimeElapsedMod.pushReceivedLines GameEvent.TimeElapsed 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Stop(stopRec), io), logger) -> 
            let backgroundPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, backgroundPixelMapActual)
            let receivedLinesPixelMapActual = 
                List.fold BlockManipulationMod.processIOCommand4ReceivedLines receivedLinesPixelMap (io ([], ())
                                                                                                     |> snd
                                                                                                     |> fst)
            Assert.AreEqual(receivedLinesPixelMapExpected, receivedLinesPixelMapActual)
            Assert.AreEqual
                (BlockManipulationMod.drawBackgroundBorder stopRec.detailState.backgroundSize 
                     (FSharpx.Collections.Map.valueList backgroundPixelMapActual |> Map.ofList), 
                 stopRec.detailState.background)
            Assert.IsTrue(List.isEmpty stopRec.detailState.receivedLines)
        | _, _ -> ()
    
    [<Test>]
    let testTryPutNewBlock() : unit = 
        // IIII
        // I..I
        // I..I
        // IIII 에 새 벽돌 1 을 채워넣어서
        // IIII
        // I1.I
        // I..I
        // IIII 가 되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [] }
        
        let nextBlock = [ (prepareBlockCell 0 0 1 (Some 1)) ] |> Map.ofList
        
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = nextBlock
              detailState = detailState }
        
        let nextBlockPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
        let backgroundPixelMap = Map.empty
        let nextBlockPixelMapExpected = Map.empty
        let backgroundPixelMapExpected = Map.empty |> Map.add 2 (prepareBlockCell 1 1 1 (Some 2))
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        match TimeElapsedMod.tryPutNewBlock |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4NextBlock nextBlockPixelMap (io ([], ())
                                                                                             |> snd
                                                                                             |> fst)
            Assert.AreEqual(nextBlockPixelMapExpected, actualPixelMap)
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, actualPixelMap)
            // 새 벽돌은 배경에 그려져서는 안 된다. 새 벽돌이 배경에 그려지지 않음을 확인했으므로, 다음 검사에서는 이 검사를 하지 않는다.
            Assert.IsTrue(Map.filter (fun _ blockCell -> 
                              match blockCell with
                              | InvisibleBorder -> false
                              | _ -> true) playRec.detailState.background
                          |> Map.isEmpty)
        | _ -> Assert.Fail()
        // IIII
        // I2.I
        // I..I
        // IIII 새 벽돌 1 을 채워넣어서 게임이 종료되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty 
                  |> addBlockCell (prepareBlockCell 1 1 2 (Some 2))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let nextBlock = [ (prepareBlockCell 0 0 1 (Some 1)) ] |> Map.ofList
        
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = nextBlock
              detailState = detailState }
        
        let nextBlockPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
        let backgroundPixelMap = Map.empty |> Map.add 2 (prepareBlockCell 1 1 2 (Some 2))
        let nextBlockPixelMapExpected = Map.empty
        let backgroundPixelMapExpected = Map.empty |> Map.add 2 (prepareBlockCell 1 1 2 (Some 2))
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        match TimeElapsedMod.tryPutNewBlock |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Stop(stopRec), io), logger) -> 
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4NextBlock nextBlockPixelMap (io ([], ())
                                                                                             |> snd
                                                                                             |> fst)
            Assert.AreEqual(nextBlockPixelMapExpected, actualPixelMap)
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, actualPixelMap)
            Assert.AreEqual
                (BlockManipulationMod.drawBackgroundBorder stopRec.detailState.backgroundSize 
                     (FSharpx.Collections.Map.values actualPixelMap |> Map.ofSeq), stopRec.detailState.background)
        | _ -> Assert.Fail()
        // IIII
        // I.2I
        // I..I
        // IIII 새 벽돌 1 을 채워넣어서
        // IIII
        // I12I
        // I..I
        // IIII 이 되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty 
                  |> addBlockCell (prepareBlockCell 2 1 2 (Some 2))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 3
              receivedLines = [] }
        
        let nextBlock = [ (prepareBlockCell 0 0 1 (Some 1)) ] |> Map.ofList
        
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = nextBlock
              detailState = detailState }
        
        let nextBlockPixelMap = Map.empty |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
        let backgroundPixelMap = Map.empty |> Map.add 2 (prepareBlockCell 2 1 2 (Some 2))
        let nextBlockPixelMapExpected = Map.empty
        
        let backgroundPixelMapExpected = 
            Map.empty
            |> Map.add 2 (prepareBlockCell 2 1 2 (Some 2))
            |> Map.add 3 (prepareBlockCell 1 1 1 (Some 3))
        
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        match TimeElapsedMod.tryPutNewBlock |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4NextBlock nextBlockPixelMap (io ([], ())
                                                                                             |> snd
                                                                                             |> fst)
            Assert.AreEqual(nextBlockPixelMapExpected, actualPixelMap)
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, actualPixelMap)
        | _ -> Assert.Fail()
    
    [<Test>]
    let testGenerateBlock() : unit = 
        let detailState : DetailStateRec = 
            { backgroundSize = Point 5 5
              background = BlockManipulationMod.drawBackgroundBorder (Point 5 5) Map.empty
              blockSize = 5
              createBlock = 
                  (fun _ _ _ _ -> 
                  [ (prepareBlockCell 0 0 1 None) ]
                  |> Map.ofList
                  |> (fun a -> a, ((), LogMod.emptyLogger)))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 0 0), Map.empty)
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let nextBlockPixelMap = Map.empty
        let nextBlockPixelMapExpected = Map.empty |> Map.add 1 (prepareBlockCell 0 0 1 (Some 1))
        let gameState = GameState.Play(playRec)
        match TimeElapsedMod.generateNextBlock |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4NextBlock nextBlockPixelMap (io ([], ())
                                                                                             |> snd
                                                                                             |> fst)
            Assert.AreEqual(nextBlockPixelMapExpected, actualPixelMap)
        | _ -> Assert.Fail()
        // 다음 벽돌이 있는데, 또 계산하면 실패.
        let playRec : PlayRec = 
            { currentBlock = ((Point 0 0), Map.empty)
              expectedBlockCell = []
              nextBlock = [ (prepareBlockCell 0 0 1 (Some 1)) ] |> Map.ofList
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match TimeElapsedMod.generateNextBlock |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> Assert.Fail()
        | _ -> ()
    
    [<Test>]
    let testStop() : unit = 
        // IIII .... 
        // I2.I .... 
        // I1.I .3..
        // IIII .... 4 인 채로 게임이 끝났고, 다시 시작한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
                  |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
                  |> addBlockCell (prepareBlockCell 1 1 2 (Some 2))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [ (prepareBlockCell 1 2 3 (Some 3)) ] }
        
        let stopRec : StopRec = 
            { currentBlock = Some((Point 1 1), Map.ofList [ prepareBlockCell 1 1 4 (Some 4) ])
              detailState = detailState }
        
        let backgroundPixelMap = 
            Map.empty
            |> Map.add 1 (prepareBlockCell 1 2 1 (Some 1))
            |> Map.add 2 (prepareBlockCell 1 1 2 (Some 2))
            |> Map.add 4 (prepareBlockCell 1 1 4 (Some 4))
        
        let backgroundPixelMapExpected = Map.empty
        let receivedLinesPixelMap = Map.empty |> Map.add 3 (prepareBlockCell 1 2 3 (Some 3))
        let receivedLinesPixelMapExpected = Map.empty
        let gameState = GameState.Stop(stopRec)
        match StopMod.start GameEvent.Start |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger) -> 
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4ReceivedLines receivedLinesPixelMap (io ([], ())
                                                                                                     |> snd
                                                                                                     |> fst)
            Assert.AreEqual(receivedLinesPixelMapExpected, actualPixelMap)
            let actualPixelMap = 
                List.fold BlockManipulationMod.processIOCommand4Background backgroundPixelMap (io ([], ())
                                                                                               |> snd
                                                                                               |> fst)
            Assert.AreEqual(backgroundPixelMapExpected, actualPixelMap)
            Assert.AreEqual
                (BlockManipulationMod.drawBackgroundBorder playNoCurrentBlockRec.detailState.backgroundSize 
                     (FSharpx.Collections.Map.values actualPixelMap |> Map.ofSeq), 
                 playNoCurrentBlockRec.detailState.background)
        | _ -> Assert.Fail()
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = Map.ofList [ prepareBlockCell 1 1 4 (Some 4) ]
              detailState = detailState }
        
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        match StopMod.start GameEvent.Start |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | None, _ -> ()
        | _ -> Assert.Fail()
        // 게임 종료 명령을 잘 수행하는지 검사한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = 
                  BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
                  |> addBlockCell (prepareBlockCell 1 2 1 (Some 1))
                  |> addBlockCell (prepareBlockCell 1 1 2 (Some 2))
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [ (prepareBlockCell 1 2 3 (Some 3)) ] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 1 1), Map.ofList [ prepareBlockCell 1 1 1 (Some 1) ])
              expectedBlockCell = [ snd (prepareBlockCell 1 2 1 (Some 2)) ]
              nextBlock = Map.ofList [ prepareBlockCell 0 0 2 (Some 3) ]
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match StopMod.stop GameEvent.Stop |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Stop(stopRec), io), logger) -> 
            let actual = fst (snd (io ([], ()))) |> Set.ofList
            
            // 내려오는 벽돌이 있는 중, 게임이 끝나면, 
            let expected = 
                [ IOMod.IOCommand.RemoveExpectedBlock(snd (prepareBlockCell 1 2 1 (Some 2)))
                  IOMod.IOCommand.RemoveNextBlock(snd (prepareBlockCell 0 0 2 (Some 3))) ]
                |> Set.ofList
            Assert.AreEqual(expected, actual)
        | _ -> Assert.Fail()
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = Map.ofList [ prepareBlockCell 0 0 2 (Some 3) ]
              detailState = detailState }
        
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        match StopMod.stop GameEvent.Stop |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Stop(stopRec), io), logger) -> 
            let actual = fst (snd (io ([], ()))) |> Set.ofList
            let expected = [ IOMod.IOCommand.RemoveNextBlock(snd (prepareBlockCell 0 0 2 (Some 3))) ] |> Set.ofList
            Assert.AreEqual(expected, actual)
        | _ -> Assert.Fail()
    
    [<Test>]
    let testTryDown() : unit = 
        // 벽돌 내리기 검사는 testDownN 에서 했고, 여기서는 바닥에 닿지 않았을 때, 시간초기화를 SetAsWait 로서 하는지 검사한다.
        // IIII
        // I..I
        // I..I
        // IIII 에, 쌓아놓은 줄
        // 새 벽돌 1 을 채워넣어서
        // IIII
        // I1.I
        // I..I
        // IIII 가 되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 1 1), Map.ofList [ prepareBlockCell 1 1 1 (Some 1) ])
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match MovementMod.tryDown GameEvent.TimeElapsed 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(_), io), logger) -> 
            (io ([], ())
             |> snd
             |> fst)
            |> List.exists ((=) (IOMod.IOCommand.Timer(TimerCommand.SetAsWait)))
            |> Assert.IsTrue
        | _ -> Assert.Fail()
        // IIII
        // I1.I
        // I..I
        // IIII 에서 (1,2) 끌기이동해서 
        // IIII
        // I..I
        // I1.I
        // IIII 이 되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 1 1), Map.ofList [ prepareBlockCell 1 1 1 (Some 1) ])
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match MovementMod.tryDown (GameEvent.DragMovement((Point 0 -1), None)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_, block), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _ -> Assert.Fail()
        // 상하끌기이동의 경우, 실패는 없고 아무것도 하지 않기로 했으므로, 시도전후로 결과가 같아야 한다.
        match MovementMod.tryDown (GameEvent.DragMovement((Point 1 1), None)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_, block), _ -> Assert.AreEqual(block, playRec.currentBlock)
        | _ -> ()
        // 좌우끌기이동에 이어 상하끌기이동을 시도하지만, 상하끌기이동 자체는 수직위치차이만 인식하므로,
        // 대각선으로 이동시도하면 결론은 성공이어야  한다.
        match MovementMod.tryDown (GameEvent.DragMovement((Point -1 -1), None)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_, block), _ -> 
            let expected : Block = ((Point 1 2), Map.empty |> addBlockCell (prepareBlockCell 1 2 1 (Some 1)))
            Assert.AreEqual(expected, block)
        | _ -> ()
        // 이 단계에서 주어지는 GameEvent 에는 logger 가 있으면 안 된다. logger 는 앞단계에서 빠져야 한다.
        match MovementMod.tryDown (GameEvent.DragMovement((Point 1 2), Some(LogMod.emptyLogger))) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_, block), _ -> Assert.Fail()
        | _ -> ()
    
    [<Test>]
    let testTryLeftRightRotation() : unit = 
        // 왼쪽오른쪽/회전 검사는 했고, 여기서는 옆면에 닿았을 때, 이동을 시도해도 실패하지 않음을 검사한다.
        // IIII
        // I1.I
        // I..I
        // IIII 에서 왼쪽으로 이동해도 실패해서는 안 된다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 1 1), Map.ofList [ prepareBlockCell 1 1 1 (Some 1) ])
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match MovementMod.tryLeftRightRotation Map.empty (GameEvent.Movement(Movement.Left)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            // 이동중인 벽돌은, 배경에 그려져서는 안 된다.
            Assert.IsTrue(Map.filter (fun _ blockCell -> 
                              match blockCell with
                              | InvisibleBorder -> false
                              | _ -> true) playRec.detailState.background
                          |> Map.isEmpty)
            (// 알림에 관한 io command 가 만들어지는지 확인한다.
             io ([], ())
             |> snd
             |> fst)
            |> List.exists (fun ioCommand -> 
                   match ioCommand with
                   | IOMod.IOCommand.Notification -> true
                   | _ -> false)
            |> Assert.IsTrue
        | _ -> Assert.Fail()
        // IIII
        // I1.I
        // I..I
        // IIII 에서 (2,1) 끌기이동해서 
        // IIII
        // I.1I
        // I..I
        // IIII 이 되어야 한다.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 2
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 2) Map.empty
              blockSize = 1
              createBlock = (fun _ _ _ _ -> Map.empty, ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 2
              receivedLines = [] }
        
        let playRec : PlayRec = 
            { currentBlock = ((Point 1 1), Map.ofList [ prepareBlockCell 1 1 1 (Some 1) ])
              expectedBlockCell = []
              nextBlock = Map.empty
              detailState = detailState }
        
        let gameState = GameState.Play(playRec)
        match MovementMod.tryLeftRightRotation Map.empty (GameEvent.DragMovement((Point -1 0), None)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 2 1), Map.empty |> addBlockCell (prepareBlockCell 2 1 1 (Some 1)))
            Assert.AreEqual(expected, playRec.currentBlock)
        | _ -> Assert.Fail()
        // 대각선으로 이동해도, 좌우끌기이동의 계산이 우선이 되도록 했으므로, 좌우로만 이동해야 한다.
        match MovementMod.tryLeftRightRotation Map.empty (GameEvent.DragMovement((Point -1 -1), None)) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), ((GameState.Play(playRec), io), logger) -> 
            let expected : Block = ((Point 2 1), Map.empty |> addBlockCell (prepareBlockCell 2 1 1 (Some 1)))
            Assert.AreEqual(expected, playRec.currentBlock)
        | _ -> Assert.Fail()
        // 이 단계에서 주어지는 GameEvent 에는 logger 가 있으면 안 된다. logger 는 앞단계에서 빠져야 한다.
        match MovementMod.tryLeftRightRotation Map.empty 
                  (GameEvent.DragMovement((Point -1 0), Some(LogMod.emptyLogger))) 
              |> StateMod.run ((gameState, IOMod.nothingToDo), LogMod.emptyLogger) with
        | Some(_), _ -> Assert.Fail()
        | _ -> ()
