﻿namespace TetrisLibrary

module SideEffectMod = 
    open System.Windows.Media
    open DefinitionMod
    open System.Reactive.Concurrency
    open System.Windows.Input
    open System.Windows
    open System
    open System.Reactive.Subjects
    open System.Windows.Controls
    open FSharp.Control
    open System.Threading
    
    type VisualDelegate = delegate of Visual -> unit
    
    type ObjectDelegate = delegate of obj -> obj
    
    type KeyMapDelegate = delegate of KeyEventArgs -> GameEvent
    
    // TODO 두명이 같이 게임하려면 key 가 다를 수 있어야 한다.
    let mapF4KeyEvent (e : KeyEventArgs) : GameEvent = 
        match e.Key with
        | Key.Left -> GameEvent.Movement(Movement.Left)
        | Key.Right -> GameEvent.Movement(Movement.Right)
        | Key.Down -> GameEvent.Movement(Movement.Down)
        | Key.Space -> GameEvent.Movement(Movement.DropDown)
        | Key.Z -> GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
        | Key.X -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | Key.Up -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | _ -> 
            let log = LogMod.Log.Error "mapF4KeyEvent failed: unexpected key."
            GameEvent.NoOp(Some(LogMod.writeLog log (LogMod.emptyLogger)))
    
    let mapFKeyEventDelegate : KeyMapDelegate = new KeyMapDelegate(mapF4KeyEvent)
    
    let mapF4KeyEvent2 (e : KeyEventArgs) : GameEvent = 
        match e.Key with
        | Key.NumPad4 -> GameEvent.Movement(Movement.Left)
        | Key.NumPad6 -> GameEvent.Movement(Movement.Right)
        | Key.NumPad2 -> GameEvent.Movement(Movement.Down)
        | Key.NumPad0 -> GameEvent.Movement(Movement.DropDown)
        | Key.NumPad1 -> GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
        | Key.NumPad3 -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | Key.NumPad8 -> GameEvent.Movement(Movement.Rotation(RotationDirection.Clockwise))
        | _ -> 
            let log = LogMod.Log.Error "mapF4KeyEvent failed: unexpected key."
            GameEvent.NoOp(Some(LogMod.writeLog log (LogMod.emptyLogger)))
    
    let mapFKeyEvent2Delegate : KeyMapDelegate = new KeyMapDelegate(mapF4KeyEvent2)
    let mapF4StartButton (_ : RoutedEventArgs) : GameEvent = GameEvent.Start
    let mapF4StopButton (_ : RoutedEventArgs) : GameEvent = GameEvent.Stop
    
    let moveBlock (drawingObjectId : int) (point : PointRec) (visualMap : Map<int, bool * DrawingVisual>) : Map<int, bool * DrawingVisual> = 
        visualMap
        |> Map.tryFind drawingObjectId
        |> Option.fold (fun _ (_, drawingVisual) -> 
               let translateTransform : TranslateTransform = drawingVisual.Transform :?> TranslateTransform
               // 왜 이와 같이 좌표를 바꾸는가. 0,0 기준으로 그려야 하기 때문이다. 아니면 여백이 남는다.
               translateTransform.X <- float ((point.x - 1) * 10)
               translateTransform.Y <- float ((point.y - 1) * 10)) ()
        visualMap // 왜 이와 같이 해도 되나. 객체 참조는 바뀌지 않았기 때문이다.
    
    let removeBlock (removeVisualDelegate : VisualDelegate) (drawingObjectId : int) 
        (visualMap : Map<int, bool * DrawingVisual>) : Map<int, bool * DrawingVisual> = 
        visualMap
        |> Map.tryFind drawingObjectId
        |> Option.fold (fun visualMap (_, drawingVisual) -> 
               removeVisualDelegate.Invoke drawingVisual
               Map.remove drawingObjectId visualMap) visualMap
    
    let addBlock (addVisualDelegate : VisualDelegate) (visualMap : Map<int, bool * DrawingVisual>) (drawing : Drawing) 
        (drawingObjectId : int) (point : PointRec) : Map<int, bool * DrawingVisual> = 
        let drawingVisual : DrawingVisual = new DrawingVisual()
        drawingVisual.Transform <- (new TranslateTransform(float ((point.x - 1) * 10), float ((point.y - 1) * 10)) :> Transform)
        using (drawingVisual.RenderOpen()) (fun drawingContext -> drawingContext.DrawDrawing drawing)
        addVisualDelegate.Invoke drawingVisual
        Map.add drawingObjectId (true, drawingVisual) visualMap
    
    let subscribeF (scheduler : IScheduler) (resource : ObjectDelegate) (recursionSubject : Subject<GameEvent>) 
        (recursionSubject4Enemy : Subject<GameEvent>) (recursionSubject4Mouse : Subject<MouseMod.MouseEvent>) 
        (visualMap : Ref<Map<int, bool * DrawingVisual>>) (lastTimer : Ref<Option<IDisposable>>) 
        (currentPoint : Ref<Option<PointRec>>) 
        (addVisualChildBackground : VisualDelegate, removeVisualChildBackground : VisualDelegate) 
        (addVisualChildNextBlock : VisualDelegate, removeVisualChildNextBlock : VisualDelegate) 
        (addVisualChildReceivedLines : VisualDelegate, removeVisualChildReceivedLines : VisualDelegate) 
        (((oldGameState : GameState, _ : IOMod.IO), _ : LogMod.Logger), 
         ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger)) : unit = 
        let foldF4IO (visualMap : Map<int, bool * DrawingVisual>) (ioCommand : IOMod.IOCommand) : Map<int, bool * DrawingVisual> = 
            match ioCommand with
            | IOMod.IOCommand.AddBackground(point, 
                                            NormalBlockCell({ blockType = blockType; 
                                                              drawingObjectId = Some(drawingObjectId) })) -> 
                let drawing : Drawing = (resource.Invoke(sprintf "block%A" blockType)) :?> Drawing
                addBlock addVisualChildBackground visualMap drawing drawingObjectId point
            | IOMod.IOCommand.MoveBackground(point, 
                                             NormalBlockCell({ blockType = blockType; 
                                                               drawingObjectId = Some(drawingObjectId) })) -> 
                moveBlock drawingObjectId point visualMap
            | IOMod.IOCommand.RemoveBackground(NormalBlockCell({ blockType = blockType; 
                                                                 drawingObjectId = Some(drawingObjectId) })) -> 
                removeBlock removeVisualChildBackground drawingObjectId visualMap
            | IOMod.IOCommand.AddReceivedLines(point, 
                                               NormalBlockCell({ blockType = blockType; 
                                                                 drawingObjectId = Some(drawingObjectId) })) -> 
                let drawing : Drawing = (resource.Invoke(sprintf "block%A" blockType)) :?> Drawing
                addBlock addVisualChildReceivedLines visualMap drawing drawingObjectId point
            | IOMod.IOCommand.MoveReceivedLines(point, 
                                                NormalBlockCell({ blockType = blockType; 
                                                                  drawingObjectId = Some(drawingObjectId) })) -> 
                moveBlock drawingObjectId point visualMap
            | IOMod.IOCommand.RemoveReceivedLines(NormalBlockCell({ blockType = blockType; 
                                                                    drawingObjectId = Some(drawingObjectId) })) -> 
                removeBlock removeVisualChildReceivedLines drawingObjectId visualMap
            | IOMod.IOCommand.AddExpectedBlock(point, 
                                               NormalBlockCell({ blockType = blockType; 
                                                                 drawingObjectId = Some(drawingObjectId) })) -> 
                let drawing : GeometryDrawing = 
                    (resource.Invoke(sprintf "expectedBlock%A" blockType)) :?> GeometryDrawing
                addBlock addVisualChildBackground visualMap drawing drawingObjectId point
            | IOMod.IOCommand.MoveExpectedBlock(point, 
                                                NormalBlockCell({ blockType = blockType; 
                                                                  drawingObjectId = Some(drawingObjectId) })) -> 
                moveBlock drawingObjectId point visualMap
            | IOMod.IOCommand.RemoveExpectedBlock(NormalBlockCell({ blockType = blockType; 
                                                                    drawingObjectId = Some(drawingObjectId) })) -> 
                removeBlock removeVisualChildBackground drawingObjectId visualMap
            | IOMod.IOCommand.AddNextBlock(point, 
                                           NormalBlockCell({ blockType = blockType; 
                                                             drawingObjectId = Some(drawingObjectId) })) -> 
                let drawing : Drawing = (resource.Invoke(sprintf "block%A" blockType)) :?> Drawing
                
                // 이와 같이 하는 이유는, 다른 좌표들은 1,1 기준인데 이 좌표는 0,0 기준이기 때문이다.
                // 이게 왜 문제가 되는가. 그리기 명령은 1,1 기준이라 가정하고 좌표를 0,0 기준으로 다시 바꾸고 있기 때문이다.
                // 따라서 여기서 1,1 기준으로 바꿔주지 않으면 여기서의 그리기는 -1,-1 기준으로 그려지게 된다.
                let point = 
                    { point with x = point.x + 1
                                 y = point.y + 1 }
                addBlock addVisualChildNextBlock visualMap drawing drawingObjectId point
            | IOMod.IOCommand.RemoveNextBlock(NormalBlockCell({ blockType = blockType; 
                                                                drawingObjectId = Some(drawingObjectId) })) -> 
                removeBlock removeVisualChildNextBlock drawingObjectId visualMap
            | IOMod.IOCommand.Timer(timerCommand) -> 
                // 여기서 이전 timer 가 있다고 치고 무조건 풀어주니 timer 가 겹치는 문제는 없다.
                // 그런데, 왜 timer 가 겹칠까.
                lastTimer := Option.fold (fun none (disposable : System.IDisposable) -> 
                                 disposable.Dispose()
                                 none) None !lastTimer
                match timerCommand with
                | TimerCommand.SetAsWait -> 
                    lastTimer 
                    := Some
                           (scheduler.Schedule
                                (TimeSpan.FromMilliseconds(float 2000), 
                                 (fun () -> recursionSubject.OnNext(GameEvent.TimeElapsed))))
                | TimerCommand.SetAsImmediate -> 
                    scheduler.Schedule(TimeSpan.Zero, (fun () -> recursionSubject.OnNext(GameEvent.TimeElapsed))) 
                    |> ignore
                visualMap
            | IOMod.IOCommand.TransferLines(lines) -> 
                let lineLength (lines : BlockList) : int = 
                    List.map (fun (point, _) -> point.y) lines
                    |> Set.ofList
                    |> Set.count
                if (lineLength lines) > 1 then 
                    scheduler.Schedule
                        (TimeSpan.Zero, (fun () -> recursionSubject4Enemy.OnNext(GameEvent.ReceivedLines(lines)))) 
                    |> ignore
                visualMap
            | _ -> visualMap
        visualMap := List.fold foldF4IO !visualMap (fst (snd (io ([], ()))))
        match oldGameState, gameState with
        | GameState.Play(_), GameState.Play(playRec) -> 
            // 현재 벽돌이 내려오고 있다.
            lock currentPoint (fun () -> currentPoint := Some(fst playRec.currentBlock))
        // 이제 막 새 벽돌이 만들어졌다.
        | _, GameState.Play(playRec) -> lock currentPoint (fun () -> currentPoint := Some(fst playRec.currentBlock))
        | GameState.Play(playRec), _ -> // 벽돌이 내려오다가 이제 막 바닥에 앉았거나, 게임이 끝났거나.
            let drawingObjectIds = 
                (snd playRec.currentBlock)
                |> Map.toSeq
                |> Seq.collect (fun (_, blockCell) -> 
                       match blockCell with
                       | NormalBlockCell({ drawingObjectId = Some(drawingObjectId) }) -> seq { yield drawingObjectId }
                       | _ -> Seq.empty)
                |> Set.ofSeq
            
            let mapF (drawingObjectId : int) (currentVisualFlag : bool, currentVisual : DrawingVisual) : bool * DrawingVisual = 
                // 현재 visual map 에 있는 visual 이, 내려오는 벽돌의 visual 이었다면,
                // 지금은 그 visual 이 더 이상 내려오는 벽돌의 visual 아니므로, 내려오는 벽돌의 visual 이 아니라고 표시한다.
                if Set.contains drawingObjectId drawingObjectIds then (false, currentVisual)
                else (currentVisualFlag, currentVisual)
            
            visualMap := Map.map mapF !visualMap
            lock currentPoint (fun () -> currentPoint := None)
            // 더이상 내려오는 벽돌이 없으면, 실제로는 마우스를 누르고 있더라도, 내부적으로는 마우스를 누르고 있지 않은 상태로 바꾼다.
            scheduler.Schedule(TimeSpan.Zero, (fun () -> recursionSubject4Mouse.OnNext(MouseMod.MouseEvent.Up))) 
            |> ignore
        | _ -> ()
        // 게임이 끝났다.
        match oldGameState, gameState with
        | GameState.Play(_), GameState.Stop(_) -> 
            System.Console.WriteLine("stop.")
            lastTimer := Option.fold (fun none (disposable : System.IDisposable) -> 
                             disposable.Dispose()
                             none) None !lastTimer
            scheduler.Schedule(TimeSpan.Zero, (fun () -> recursionSubject4Enemy.OnNext(GameEvent.Stop))) |> ignore
        | GameState.PlayNoCurrentBlock(_), GameState.Stop(_) -> 
            System.Console.WriteLine("stop.")
            lastTimer := Option.fold (fun none (disposable : System.IDisposable) -> 
                             disposable.Dispose()
                             none) None !lastTimer
            scheduler.Schedule(TimeSpan.Zero, (fun () -> recursionSubject4Enemy.OnNext(GameEvent.Stop))) |> ignore
        | _ -> ()
    
    // System.Console.WriteLine(sprintf "%A" (fst (snd (logger ([], ())))))
    let subscribe (recursionSubject : Subject<GameEvent>, recursionSubject4Enemy : Subject<GameEvent>, 
                   recursionSubject4Mouse : Subject<MouseMod.MouseEvent>, startButton : Button, stopButton : Button, 
                   keyboardFocusable : UIElement, mouseEventSource : UIElement, resource : ObjectDelegate, 
                   addVisualChildBackground : VisualDelegate, removeVisualChildBackground : VisualDelegate, 
                   addVisualChildNextBlock : VisualDelegate, removeVisualChildNextBlock : VisualDelegate, 
                   addVisualChildReceivedLines : VisualDelegate, removeVisualChildReceivedLines : VisualDelegate, 
                   keyMap : KeyMapDelegate) : IDisposable = 
        // visualMap 과 lastTimer 는 각 사용자마다 별개로 만들어야 한다.
        let visualMap : Ref<Map<int, bool * DrawingVisual>> = ref Map.empty
        // 왜 최근 timer 를 유지해야 하는가. 예를 들어, 2초 timer 라 할때, 0.5 초 남았을 때 바닥으로 내리면 즉시 만료하는 timer 가 새로 만들어진다.
        // 따라서, 0.5 초 남은 timer 와 즉시 timer 두개가 남아있게 된다. 바닥에 내렸으면, 즉시 timer 만 동작해야 하고, 0.5 초 남은 timer 는 동작을 취소해야 한다.
        // 이 문제에 대한 단순한 관점은, timer 객체가 하나뿐이라고 보는 것이다. timer 설정을 새로 했으면, timer 객체에 설정되어 있던 기존 timer 는 지워져야 하는 것이다.
        let lastTimer : Ref<Option<IDisposable>> = ref None
        // 이 기능을 쓰는 이유는, 실제로 touch 를 해봤을 때, 각 visual 의 크기가 너무 작아서 손가락으로 정확하게 누르기가 어려웠다.
        // 그래서, 현재 내려오는 벽돌의 기준좌표를 기준으로 5x5 위치의 아무데나 누르면 끌기이동을 인식하도록 하기 위해서 아래와 같은 공유변수가 필요하다.
        // 이와 같이 하면, mouse down 을 ui thread 에서 다룰 필요가 없다.
        // 더 쉽게 touch 를 할 수 있고, scheduler 를 거쳐야 할 필요가 없어서 또 좋다. 안 좋은 점은 lock 을 써야 한다는 점이다.
        let currentPoint : Ref<Option<PointRec>> = ref None
        
        let div (point : PointRec) : PointRec = 
            { x = round (float point.x / float 10) |> int
              y = round (float point.y / float 10) |> int }
        
        let p2p (point : Point) : PointRec = 
            { x = int point.X
              y = int point.Y }
        
        // 아래와 같이, mouse down 을 제외한, up/move 는 직접 game event stream 에 섞는 것이,
        // scheduler 에 작업을 걸었다가 다시 event 를 넣도록 하는 이중작업을 피하는 방법이다. 이런 이중작업이 필요한 경우도 있지만,
        // up/move 의 경우 ui thread 에서 실행되어야 할 필요가 없다. 어떠한 동기화도 할 필요도 없다.
        let mouseStream = 
            let mouseLeftDown = 
                let mapF (e : MouseButtonEventArgs) : List<MouseMod.MouseEvent> = 
                    let lockF() : List<MouseMod.MouseEvent> = 
                        let point = div (p2p (e.GetPosition(mouseEventSource)))
                        match Some(BlockManipulationMod.adder)
                              |> FunctionalUtil.OptMod.apply (Some(point))
                              |> FunctionalUtil.OptMod.apply (Option.map BlockManipulationMod.invert !currentPoint) with
                        | Some(point) when point.x >= 0 && point.x <= 4 && point.y >= 0 && point.y <= 4 -> 
                            let log = LogMod.Log.Info "mouseLeftDownF ok."
                            let logger = LogMod.writeLog log (LogMod.emptyLogger)
                            [ MouseMod.MouseEvent.Down(point, Some(logger)) ]
                        | _ -> 
                            let log = LogMod.Log.Info "mouseLeftDownF failed."
                            let logger = LogMod.writeLog log (LogMod.emptyLogger)
                            [ MouseMod.MouseEvent.NoOp(Some(logger)) ]
                    lock currentPoint lockF
                mouseEventSource.MouseLeftButtonDown :> IObservable<MouseButtonEventArgs>
                |> Reactive.Observable.map mapF
                |> Reactive.Observable.bind FunctionalUtil.ObsMod.mapListToObservable
            
            let mouseLeftUp = 
                mouseEventSource.MouseLeftButtonUp :> IObservable<MouseButtonEventArgs> 
                |> FSharp.Control.Reactive.Observable.map (fun _ -> MouseMod.MouseEvent.Up)
            
            let mouseMove = 
                let mapF (e : MouseEventArgs) : MouseMod.MouseEvent = 
                    let lastPoint = div (p2p (e.GetPosition(mouseEventSource)))
                    MouseMod.MouseEvent.Move lastPoint
                mouseEventSource.MouseMove :> IObservable<MouseEventArgs> |> FSharp.Control.Reactive.Observable.map mapF
            // 마우스 누름 event 는, 따로 mouse down event 만 받는 stream 으로부터 mouse stream 으로 event 를 밀어서 받는다.
            Reactive.Observable.mergeArray [| mouseMove; mouseLeftUp; mouseLeftDown; recursionSubject4Mouse |]
            |> FunctionalUtil.ObsMod.scan StateMachineMod.MouseMod.preF StateMachineMod.MouseMod.scanF4Mouse 
                   ((MouseMod.MouseState.Up, FunctionalUtil.StateMod.unit()), (LogMod.emptyLogger))
            |> Reactive.Observable.bind (StateMachineMod.MouseMod.mapF >> FunctionalUtil.ObsMod.mapListToObservable)
        
        let detailState : DetailStateRec = 
            { backgroundSize = DefinitionMod.Point 10 20
              background = BlockManipulationMod.drawBackgroundBorder (DefinitionMod.Point 10 20) Map.empty
              blockSize = 5
              createBlock = BlockManipulationMod.Block5x5Mod.createBlock
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let gameState = 
            GameState.Stop({ currentBlock = None
                             detailState = detailState })
        
        // 모든 기능들이, 여기서 하나로 묶이게 된다.
        let disposable = 
            let start = startButton.Click :> IObservable<RoutedEventArgs> |> Reactive.Observable.map mapF4StartButton
            let stop = stopButton.Click :> IObservable<RoutedEventArgs> |> Reactive.Observable.map mapF4StopButton
            let key = 
                keyboardFocusable.KeyDown :> IObservable<KeyEventArgs> 
                |> Reactive.Observable.map (fun e -> keyMap.Invoke e)
            Reactive.Observable.mergeArray [| start; stop; key; recursionSubject; mouseStream |]
            |> Reactive.Observable.subscribeOn Scheduler.Default
            |> Reactive.Observable.observeOnContext SynchronizationContext.Current
            |> FunctionalUtil.ObsMod.scanWithOld StateMachineMod.GameMod.preF 
                   (StateMachineMod.GameMod.scanF BlockManipulationMod.rotationPointMap) 
                   ((gameState, DefinitionMod.IOMod.nothingToDo), DefinitionMod.LogMod.emptyLogger)
            |> Reactive.Observable.subscribe 
                   (subscribeF Scheduler.Default resource recursionSubject recursionSubject4Enemy recursionSubject4Mouse 
                        visualMap lastTimer currentPoint (addVisualChildBackground, removeVisualChildBackground) 
                        (addVisualChildNextBlock, removeVisualChildNextBlock) 
                        (addVisualChildReceivedLines, removeVisualChildReceivedLines))
        
        System.Reactive.Disposables.Disposable.Create(fun unit -> disposable.Dispose())
