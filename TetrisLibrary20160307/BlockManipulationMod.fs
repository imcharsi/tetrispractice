﻿namespace TetrisLibrary

module BlockManipulationMod = 
    open DefinitionMod
    open FunctionalUtil
    
    let printBackground (backgroundSize : PointRec) (background : BlockMap) : unit = 
        seq { yield (fun y x -> Point x y) }
        |> FunctionalUtil.SeqMod.apply (seq { 0..backgroundSize.y + 1 })
        |> FunctionalUtil.SeqMod.apply (seq { 0..backgroundSize.x + 1 })
        |> Seq.fold (fun _ point -> 
               match Map.tryFind point background with
               | Option.Some(BlockCell.NormalBlockCell(normalBlockRec)) -> 
                   System.Console.Write(sprintf "%A" normalBlockRec.blockType)
               | Option.Some(BlockCell.InvisibleBorder) -> System.Console.Write("I")
               | None -> System.Console.Write(".")
               if point.x = backgroundSize.x + 1 then System.Console.WriteLine("")) ()
    
    let printBlock (blockSize : int) (origin : PointRec, blockMap : BlockMap) : unit = 
        seq { yield (fun y x -> Point x y) }
        |> FunctionalUtil.SeqMod.apply (seq { origin.y..(origin.y + blockSize - 1) })
        |> FunctionalUtil.SeqMod.apply (seq { origin.x..(origin.x + blockSize - 1) })
        |> Seq.fold (fun _ point -> 
               match Map.tryFind point blockMap with
               | Option.Some(BlockCell.NormalBlockCell(normalBlockRec)) -> 
                   System.Console.Write(sprintf "%A" normalBlockRec.blockType)
               | Option.Some(BlockCell.InvisibleBorder) -> System.Console.Write("I")
               | None -> System.Console.Write(".")
               if point.x = origin.x + blockSize - 1 then System.Console.WriteLine("")) ()
    
    let processIOCommand4Background (pixelMap : Map<int, PointRec * BlockCell>) (ioCommand : IOMod.IOCommand) : Map<int, PointRec * BlockCell> = 
        match ioCommand with
        | IOMod.IOCommand.AddBackground(point, 
                                        BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                    drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.MoveBackground(point, 
                                         BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                     drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.RemoveBackground(BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                       drawingObjectId = Some drawingObjectId })) -> 
            Map.remove drawingObjectId pixelMap
        | _ -> pixelMap
    
    let processIOCommand4ExpectedBlock (pixelMap : Map<int, PointRec * BlockCell>) (ioCommand : IOMod.IOCommand) : Map<int, PointRec * BlockCell> = 
        match ioCommand with
        | IOMod.IOCommand.AddExpectedBlock(point, 
                                           BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                       drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.MoveExpectedBlock(point, 
                                            BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                        drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.RemoveExpectedBlock(BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                          drawingObjectId = Some drawingObjectId })) -> 
            Map.remove drawingObjectId pixelMap
        | _ -> pixelMap
    
    let processIOCommand4ReceivedLines (pixelMap : Map<int, PointRec * BlockCell>) (ioCommand : IOMod.IOCommand) : Map<int, PointRec * BlockCell> = 
        match ioCommand with
        | IOMod.IOCommand.AddReceivedLines(point, 
                                           BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                       drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.MoveReceivedLines(point, 
                                            BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                        drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.RemoveReceivedLines(BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                          drawingObjectId = Some drawingObjectId })) -> 
            Map.remove drawingObjectId pixelMap
        | _ -> pixelMap
    
    let processIOCommand4NextBlock (pixelMap : Map<int, PointRec * BlockCell>) (ioCommand : IOMod.IOCommand) : Map<int, PointRec * BlockCell> = 
        match ioCommand with
        | IOMod.IOCommand.AddNextBlock(point, 
                                       BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                   drawingObjectId = Some drawingObjectId })) -> 
            Map.add drawingObjectId (point, 
                                     BlockCell.NormalBlockCell({ blockType = blockType
                                                                 drawingObjectId = Some drawingObjectId })) pixelMap
        | IOMod.IOCommand.RemoveNextBlock(BlockCell.NormalBlockCell({ blockType = blockType; 
                                                                      drawingObjectId = Some drawingObjectId })) -> 
            Map.remove drawingObjectId pixelMap
        | _ -> pixelMap
    
    let drawBackgroundBorder (backgroundSize : PointRec) (background : BlockMap) : BlockMap = 
        // 꼭대기와 바닥의 가로
        let xSeq1 = seq { 0..backgroundSize.x + 1 }
        
        let ySeq1 = 
            seq { 
                yield 0
                yield backgroundSize.y + 1
            }
        
        // 꼭대기와 바닥 사이의 양 세로 기둥
        let ySeq2 = seq { 1..backgroundSize.y }
        
        let xSeq2 = 
            seq { 
                yield 0
                yield backgroundSize.x + 1
            }
        
        let result = 
            seq { yield Point }
            |> FunctionalUtil.SeqMod.apply xSeq1
            |> FunctionalUtil.SeqMod.apply ySeq1
            |> Seq.fold (fun result point -> Map.add point BlockCell.InvisibleBorder result) background
        
        let result = 
            seq { yield Point }
            |> FunctionalUtil.SeqMod.apply xSeq2
            |> FunctionalUtil.SeqMod.apply ySeq2
            |> Seq.fold (fun result point -> Map.add point BlockCell.InvisibleBorder result) result
        
        result
    
    let nextPoint (blockSize : int) (origin : PointRec) (point : PointRec) : PointRec = 
        // 아래와 같이 1 을 빼놓고 시작하는 것이 편하다. 정사각형 한변에는 최대 blockSize 만큼 벽돌이 있을 수 있는데,
        // 여기서 문제되는 것은, 범위계산을 x<=max 와 같이 하는 데 있다.
        // 무조건 시계방향으로만 좌표를 계산한다. 만들고나서 보니, 이 함수를 사용해서 시계반대방향으로 계산할 일이 없다.
        // 시계반대방향좌표는 아래의 sequenceRotationPoint 단계에서 계산한다.
        let blockSize = blockSize - 1
        match point.x, point.y with
        | leftX, topY when leftX = origin.x && topY = origin.y -> { point with x = leftX + 1 }
        | rightX, topY when rightX = origin.x + blockSize && topY = origin.y -> { point with y = topY + 1 }
        | leftX, bottomY when leftX = origin.x && bottomY = origin.y + blockSize -> { point with y = bottomY - 1 }
        | rightX, bottomY when rightX = origin.x + blockSize && bottomY = origin.y + blockSize -> 
            { point with x = rightX - 1 }
        | leftX, y when leftX = origin.x -> { point with y = y - 1 }
        | rightX, y when rightX = origin.x + blockSize -> { point with y = y + 1 }
        | x, topY when topY = origin.y -> { point with x = x + 1 }
        | x, bottomY when bottomY = origin.y + blockSize -> { point with x = x - 1 }
        | _ -> point
    
    // #region
    // 편의 위주로 자료를 구성했다. 결과 중 int 는, 주어진 점이 회전할 때, 거리를 뜻한다.
    // 따라서, 이 값을 보고 이 좌표를 기준으로 몇개의 값을 더 구해야 하는지 알 수 있다.
    // 이와 같은 좌표열은, 벽돌을 회전할 때, 각 벽돌이 회전하면서 지나가는 영역을 검사하기 위해 쓴다.
    // ..1..
    // ..1..
    // ..1..
    // ..1..
    // ..... 이 있고 시계방향으로 회전한다면
    // ..???
    // ..???
    // .????
    // .??..
    // ..... 가 검사대상이 되는 영역이다. 이 영역에는 다른 벽돌이 있으면 안된다.
    // #endregion
    let sequenceRotationPoint (blockSize : int) (origin : PointRec) (point : PointRec) : seq<PointRec * (int * (PointRec * PointRec))> = 
        let rec f (point : PointRec) : seq<PointRec> = 
            seq { 
                let next = nextPoint blockSize origin point
                yield point
                yield! f next
            }
        
        let prevSeq = f point |> Seq.skip 0
        let currentSeq = f point |> Seq.skip 1
        let nextSeq = f point |> Seq.skip 2
        (* // 아래와 같이 하는 것보다, 위와 같이 하는 것이 더 낫다.
        let rec innerF (point : PointRec) : seq<PointRec * (int * (PointRec * PointRec))> = 
            seq { 
                let next = nextPoint blockSize origin RotationDirection.Clockwise point
                let prev = nextPoint blockSize origin RotationDirection.CounterClockwise point
                yield point, (blockSize, (prev, next))
                yield! innerF next
            }
        innerF point
        *)
        Seq.zip prevSeq nextSeq
        |> Seq.map (fun tuple -> blockSize, tuple)
        |> Seq.zip currentSeq
    
    // 주어진 점 한개에 대한 회전영역을 구한다.
    // log 기능을 넣다보니, 많이 복잡해졌다. 이 함수는 결론에 있어서, StateOption<PointRec * List<PointRec>, unit ,Logger> 이다.
    let findSingleRotationArea (rotationPointMap : Map<PointRec, int * (PointRec * PointRec)>) 
        (rotationDirection : RotationDirection) (point : PointRec) (_ : unit, logger : LogMod.Logger) : Option<PointRec * List<PointRec>> * (unit * LogMod.Logger) = 
        let f = 
            match rotationDirection with
            | Clockwise -> snd
            | CounterClockwise -> fst
        
        let rec recF (n : int) (point : PointRec) (result : List<PointRec>, logger : LogMod.Logger) : Option<PointRec * List<PointRec>> * (unit * LogMod.Logger) = 
            if n > 0 then 
                match Map.tryFind point rotationPointMap with
                | Some(_, pointTuple) -> recF (n - 1) (f pointTuple) (point :: result, logger)
                | _ -> None, ((), (LogMod.writeLog (LogMod.Log.Error "findSingleRotationArea failed.") logger))
            else 
                match result with
                | head :: tail -> Some(head, tail), ((), logger)
                | [] -> None, ((), (LogMod.writeLog (LogMod.Log.Error "findSingleRotationArea failed.") logger))
        
        match Map.tryFind point rotationPointMap with
        | Some(n, _) -> recF n point ([], logger)
        | _ -> None, ((), (LogMod.writeLog (LogMod.Log.Error "findSingleRotationArea failed.") logger))
    
    let adder (step : PointRec) (point : PointRec) : PointRec = 
        { x = point.x + step.x
          y = point.y + step.y }
    
    let invert (point : PointRec) : PointRec = 
        { x = -point.x
          y = -point.y }
    
    let manipluateCoordinate (currentBlock : BlockMap) (step : PointRec) : BlockMap = 
        FunctionalUtil.MapMod.mapKey (adder step) currentBlock
    let testOverlap (currentBlock : BlockMap) (background : BlockMap) : bool = 
        // 겹치면 참, 안겹치면 거짓.
        Map.exists (fun point _ -> Map.containsKey point background) currentBlock
    
    let move (increment : PointRec) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.Play(playRec) -> 
            let f (point : PointRec) : PointRec = 
                { point with x = point.x + increment.x
                             y = point.y + increment.y }
            playRec.currentBlock
            |> TupleMod.map (f, FunctionalUtil.MapMod.mapKey f)
            |> TupleMod.fold (fun (point, blockMap) -> 
                   if testOverlap blockMap playRec.detailState.background then 
                       let log = LogMod.Log.Error "move failed: overlapped."
                       None, ((gameState, io), (LogMod.writeLog log logger))
                   else 
                       let log = LogMod.Log.Info "move ok."
                       Some(point, blockMap), ((gameState, io), LogMod.writeLog log logger))
        | _ -> 
            let log = LogMod.Log.Error "move failed: unexpected game state."
            None, ((gameState, io), (LogMod.writeLog log logger))
    
    // TODO downN 에서 베꼈다. 중복이 많은데, 정리해보기.
    let moveN (increment : PointRec) (n : int) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        let rec innerF (n : int) (playRec : PlayRec, io : IOMod.IO, logger : LogMod.Logger) : Option<Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
            if n > 0 then 
                match move increment |> StateMod.run ((GameState.Play(playRec), io), logger) with
                | Some(block), ((GameState.Play(playRec), io), logger) -> 
                    innerF (n - 1) ({ playRec with currentBlock = block }, io, logger)
                | None, (_, logger) -> 
                    let log = LogMod.Log.Info "moveN ok."
                    Some(playRec.currentBlock), ((gameState, io), LogMod.writeLog log logger)
                | _, (_, logger) -> 
                    let log = LogMod.Log.Error "moveN failed: unexpected game state."
                    None, ((gameState, io), LogMod.writeLog log logger)
            else Some(playRec.currentBlock), ((gameState, io), logger)
        match gameState with
        | GameState.Play(playRec) -> innerF n (playRec, io, logger)
        | _ -> 
            let log = LogMod.Log.Error "moveN failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let left : StateOptionMod.StateOption<Block, GameState * IOMod.IO, LogMod.Logger> = move (Point -1 0)
    let right : StateOptionMod.StateOption<Block, GameState * IOMod.IO, LogMod.Logger> = move (Point 1 0)
    let leftN (n : int) : StateOptionMod.StateOption<Block, GameState * IOMod.IO, LogMod.Logger> = moveN (Point -1 0) n
    let rightN (n : int) : StateOptionMod.StateOption<Block, GameState * IOMod.IO, LogMod.Logger> = moveN (Point 1 0) n
    
    let findAllRotationArea (rotationMap : Map<PointRec, int * (PointRec * PointRec)>) 
        (rotationDirection : RotationDirection) (point : PointRec, blockMap : BlockMap) (state : unit * LogMod.Logger) : Option<Map<PointRec, PointRec * List<PointRec>>> * (unit * LogMod.Logger) = 
        let negativeOrigin : PointRec = 
            { x = -point.x
              y = -point.y }
        
        let originBlockMap = manipluateCoordinate blockMap negativeOrigin
        
        // 결론에 있어서 StateMod.Result<List<Option<PointRec * (PointRec * List<PointRec>)>>, unit, LogMod.Logger> 와 같다.
        let foldF (result : List<Option<PointRec * (PointRec * List<PointRec>)>>, state : unit * LogMod.Logger) 
            (point : PointRec) : StateMod.Result<List<Option<PointRec * (PointRec * List<PointRec>)>>, unit, LogMod.Logger> = 
            findSingleRotationArea rotationMap rotationDirection point
            |> StateOptionMod.map (fun x -> point, x)
            |> StateMod.map (fun x -> x :: result)
            |> StateMod.run state
        
        let mapF (a : PointRec, (b : PointRec, c : List<PointRec>)) = 
            (adder point a), ((adder point b), (List.map (adder point) c))
        Seq.fold foldF ([], state) (FSharpx.Collections.Map.keys originBlockMap)
        |> TupleMod.map (OptionListMod.traverse id, id)
        |> TupleMod.map (Option.map (List.map mapF), id)
        |> TupleMod.map (Option.map Map.ofList, id)
    
    let internalRotate (rotationMap : Map<PointRec, int * (PointRec * PointRec)>) 
        (rotationDirection : RotationDirection) (background : BlockMap) (block : Block) (state : unit * LogMod.Logger) : Option<Block> * (unit * LogMod.Logger) = 
        let bindF (rotationMap : Map<PointRec, PointRec * List<PointRec>>) (_ : unit, logger : LogMod.Logger) : Option<Block> * (unit * LogMod.Logger) = 
            if Map.fold 
                   (fun result point (lastPoint, pointList) -> 
                   result |> FunctionalUtil.ListMod.append (point :: lastPoint :: pointList)) [] rotationMap 
               |> List.exists (fun point -> Map.containsKey point background) then 
                None, ((), LogMod.writeLog (LogMod.Log.Error "internalRotate failed: overlapped.") logger)
            else 
                let beRotatedPointMap = 
                    Map.fold (fun result point (lastPoint, _) -> Map.add lastPoint point result) Map.empty rotationMap
                let resultBlockMap = FunctionalUtil.MapMod.lookup Map.empty beRotatedPointMap (snd block)
                Some(fst block, resultBlockMap), ((), logger)
        findAllRotationArea rotationMap rotationDirection block
        |> StateOptionMod.bind bindF
        |> StateMod.run (state)
    
    let rotate (rotationMap : Map<PointRec, int * (PointRec * PointRec)>) (rotationDirection : RotationDirection) 
        ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.Play(playRec) -> 
            internalRotate rotationMap rotationDirection playRec.detailState.background playRec.currentBlock
            |> StateMod.run ((), logger)
            |> TupleMod.map (id, fun (_, logger) -> (gameState, io), logger)
        | _ -> 
            None, ((gameState, io), LogMod.writeLog (LogMod.Log.Error "rotate failed: unexpected game state.") logger)
    
    // #region
    // 무조건 성공하는데, 바닥에 닿았음을 표시할 수 있어야 한다.
    // #endregion
    let downN (n : int) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<bool * Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        let rec innerF (n : int) (playRec : PlayRec, io : IOMod.IO, logger : LogMod.Logger) : Option<bool * Block> * ((GameState * IOMod.IO) * LogMod.Logger) = 
            if n > 0 then 
                match move (Point 0 1) |> StateMod.run ((GameState.Play(playRec), io), logger) with
                | Some(block), ((GameState.Play(playRec), io), logger) -> 
                    innerF (n - 1) ({ playRec with currentBlock = block }, io, logger)
                | None, (_, logger) -> 
                    let log = LogMod.Log.Info "downN ok."
                    Some(true, playRec.currentBlock), ((gameState, io), LogMod.writeLog log logger)
                | _, (_, logger) -> 
                    let log = LogMod.Log.Error "downN failed: unexpected game state."
                    None, ((gameState, io), LogMod.writeLog log logger)
            else Some(false, playRec.currentBlock), ((gameState, io), logger)
        match gameState with
        | GameState.Play(playRec) -> innerF n (playRec, io, logger)
        | _ -> 
            let log = LogMod.Log.Error "downN failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    // 난수는 그냥 베꼈다.
    // https://en.wikipedia.org/wiki/Linear_congruential_generator
    let linearCongruentialSequence (multiplier : int) (increment : int) (modulus : int) (seed : int) : int = 
        (multiplier * seed + increment) % modulus
    // https://rosettacode.org/wiki/Linear_congruential_generator
    let appliedLCS = linearCongruentialSequence 214013 2531011 System.Int32.MaxValue
    
    let reassignDrawingObjectId (blockList : BlockList) (detailState : DetailStateRec) : BlockList * DetailStateRec = 
        let foldF (counter : int, result : BlockList) (point : PointRec, blockCell : BlockCell) : int * BlockList = 
            match blockCell with
            | NormalBlockCell({ blockType = blockType; drawingObjectId = _ }) -> 
                counter + 1, 
                (point, 
                 NormalBlockCell({ blockType = blockType
                                   drawingObjectId = Some counter }))
                :: result
            | InvisibleBorder -> counter, (point, InvisibleBorder) :: result
        
        let counter, result = List.fold foldF (detailState.counter, []) (List.rev blockList)
        result, { detailState with counter = counter }
    
    let reassignDrawingObjectIdAsMap (blockMap : BlockMap) (detailState : DetailStateRec) : BlockMap * DetailStateRec = 
        let foldF (counter : int, result : BlockMap) (point : PointRec) (blockCell : BlockCell) : int * BlockMap = 
            match blockCell with
            | NormalBlockCell({ blockType = blockType; drawingObjectId = _ }) -> 
                counter + 1, 
                Map.add point (NormalBlockCell({ blockType = blockType
                                                 drawingObjectId = Some counter })) result
            | InvisibleBorder -> counter, Map.add point InvisibleBorder result
        
        let counter, result = Map.fold foldF (detailState.counter, Map.empty) blockMap
        result, { detailState with counter = counter }
    
    let reassingDrawingObjectIdAsBlockCellList (blockMap : BlockMap) (detailState : DetailStateRec) : List<BlockCell> * DetailStateRec = 
        let foldF (result : List<BlockCell>, counter : int) (_ : PointRec) (blockCell : BlockCell) : List<BlockCell> * int = 
            match blockCell with
            | BlockCell.NormalBlockCell({ blockType = blockType; drawingObjectId = _ }) -> 
                let blockCell = 
                    BlockCell.NormalBlockCell({ blockType = blockType
                                                drawingObjectId = Some counter })
                blockCell :: result, counter + 1
            | x -> x :: result, counter
        
        let blockCellList, counter = Map.fold foldF ([], detailState.counter) blockMap
        blockCellList, { detailState with counter = counter }
    
    let rotationPointMap : Map<PointRec, int * (PointRec * PointRec)> = 
        Map.ofSeq (sequenceRotationPoint 5 (Point 0 0) (Point 0 0) |> Seq.take 16)
        |> FSharpx.Collections.Map.union (Map.ofSeq (sequenceRotationPoint 3 (Point 1 1) (Point 1 1) |> Seq.take 8))
        |> FSharpx.Collections.Map.union (Map.add (Point 2 2) (1, ((Point 2 2), (Point 2 2))) Map.empty)
    
    module Block5x5Mod = 
        let prepareBlockCell (x : int) (y : int) (blockType : int) (drawingObjectId : Option<int>) : PointRec * BlockCell = 
            (Point x y), 
            (BlockCell.NormalBlockCell({ blockType = blockType
                                         drawingObjectId = drawingObjectId }))
        
        let block0 = 
            [ (prepareBlockCell 2 0 0 None)
              (prepareBlockCell 2 1 0 None)
              (prepareBlockCell 2 2 0 None)
              (prepareBlockCell 2 3 0 None) ]
            |> Map.ofList
        
        let block1 = 
            [ (prepareBlockCell 1 1 1 None)
              (prepareBlockCell 1 2 1 None)
              (prepareBlockCell 2 2 1 None)
              (prepareBlockCell 3 2 1 None) ]
            |> Map.ofList
        
        let block2 = 
            [ (prepareBlockCell 3 1 2 None)
              (prepareBlockCell 3 2 2 None)
              (prepareBlockCell 2 2 2 None)
              (prepareBlockCell 1 2 2 None) ]
            |> Map.ofList
        
        let block3 = 
            [ (prepareBlockCell 2 1 3 None)
              (prepareBlockCell 2 2 3 None)
              (prepareBlockCell 1 2 3 None)
              (prepareBlockCell 3 2 3 None) ]
            |> Map.ofList
        
        let block4 = 
            [ (prepareBlockCell 1 1 4 None)
              (prepareBlockCell 1 2 4 None)
              (prepareBlockCell 2 2 4 None)
              (prepareBlockCell 2 3 4 None) ]
            |> Map.ofList
        
        let block5 = 
            [ (prepareBlockCell 3 1 5 None)
              (prepareBlockCell 3 2 5 None)
              (prepareBlockCell 2 2 5 None)
              (prepareBlockCell 2 3 5 None) ]
            |> Map.ofList
        
        let block6 = 
            [ (prepareBlockCell 1 1 6 None)
              (prepareBlockCell 1 2 6 None)
              (prepareBlockCell 2 1 6 None)
              (prepareBlockCell 2 2 6 None) ]
            |> Map.ofList
        
        let blockMap5x5 = 
            Map.empty
            |> Map.add 0 block0
            |> Map.add 1 block1
            |> Map.add 2 block2
            |> Map.add 3 block3
            |> Map.add 4 block4
            |> Map.add 5 block5
            |> Map.add 6 block6
        
        // drawing object id 안쓰기. 일부러 비워둬야 한다.
        // 한편, 여기서는 회전을 다루지 않는다. 써놓고 보니, 회전 기능을 여기서 다루기에는 중복이 많다.
        // generateBlock 에서 다루는 것이 좋겠다.
        let createBlock (blockType : int) (rotationCount : int) (origin : PointRec) (_ : unit, logger : LogMod.Logger) : BlockMap * (unit * LogMod.Logger) = 
            let foldF (block : Option<Block>, (_ : unit, logger : LogMod.Logger)) (_ : int) : Option<Block> * (unit * LogMod.Logger) = 
                // 일반적으로는 회전이 실패할 수 있다. 실패하면, 회전을 시도하기 직전 벽돌이 결과가 된다.
                // 그런데, 이 경우에는 회전이 실패할 수 없다. 그래서 orElse 를 쓰지 않는다.
                StateMod.unit block
                |> StateOptionMod.bind (internalRotate rotationPointMap RotationDirection.Clockwise Map.empty)
                |> StateMod.run ((), logger)
            
            let blockType = 
                (if blockType < 0 then (-blockType) % 7
                 else blockType % 7)
            
            let rotationCount = 
                if rotationCount < 0 then -rotationCount % 4
                else rotationCount % 4
            
            let result, (_, logger) = 
                match blockMap5x5
                      |> Map.tryFind blockType
                      |> Option.map (fun blockMap -> manipluateCoordinate blockMap origin) with
                | Some blockMap -> 
                    let mapF (o : Option<Block>) : BlockMap = 
                        match o with
                        | None -> manipluateCoordinate block0 origin
                        | Some(_, blockMap) -> blockMap
                    // 0..0 이면 1번을 반복한다는 말이다. 따라서 0 을 빼야 한번도 회전을 하지 않을 수 있고, 0회에서 3회 사이에서 회전할 수 있다.
                    seq { 
                        for i in 0..rotationCount do
                            yield i
                    }
                    |> Seq.filter ((<>) 0)
                    |> Seq.fold foldF (Some((Point 0 0), blockMap), ((), logger))
                    |> TupleMod.map (mapF, id)
                | None -> manipluateCoordinate block0 origin, ((), logger)
            
            let log = LogMod.Log.Info(sprintf "createBlock ok: %A" result)
            result, ((), LogMod.writeLog log logger)
