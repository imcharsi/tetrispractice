﻿namespace TetrisLibrary

module TimeElapsedMod = 
    open FunctionalUtil
    open DefinitionMod
    
    // #region
    // 쌓아놓은 줄을 바닥에서 쌓아올려서 범위 안에 배경으로서 남는 벽돌과 범위 바깥으로 넘친 벽돌을 구분한다.
    // 아래와 같이 인자를 받는 이유는, ReceivedLines GameEvent 처리에 유용하기 때문이다. 거의 모든 구절이 같고 일부만 다르기 때문에,
    // 이와 같이 하는 것이 실용적이다.
    // #endregion
    let splitBackgroundAndOverflow (background : BlockMap) (backgroundSize : PointRec) (receivedLines : BlockList) 
        (removeDrawingCommand : BlockCell -> IOMod.IOCommand) : BlockMap * List<IOMod.IOCommand> = 
        let receivedLinesCount = 
            List.map (fun (point, _) -> point.y) receivedLines
            |> Set.ofList
            |> Set.count
        
        let xList = [ 1..backgroundSize.x ]
        let bindF (tarY : int, srcY : int) : List<PointRec * PointRec> = 
            [ fun x -> Point x tarY, Point x srcY ] |> FunctionalUtil.ListMod.apply xList
        let prepareLookupMap (init : BlockMap) (backgroundMap : BlockMap) (crossMap : Map<PointRec, PointRec>) : BlockMap = 
            FunctionalUtil.MapMod.lookup init crossMap backgroundMap
        FunctionalUtil.ListMod.zip (List.rev [ (1 - receivedLinesCount)..(backgroundSize.y - receivedLinesCount) ]) 
            (List.rev [ 1..backgroundSize.y ])
        |> List.partition (fun (tarY, srcY) -> tarY > 0)
        |> TupleMod.map (List.collect bindF, List.collect bindF)
        |> TupleMod.map (Map.ofList, Map.ofList)
        // 화면에서 움직이는 drawing object 를 구분하기 위해, 일부러 테두리 없이 계산한다.
        |> TupleMod.map (prepareLookupMap Map.empty background, prepareLookupMap Map.empty background)
        |> TupleMod.map (id, FSharpx.Collections.Map.valueList >> List.map removeDrawingCommand)
    
    // 새 벽돌의 시작위치를 계산한다. 반올림에 관해서, 3/2=1 이다.
    let calculateStartPoint (backgroundSize : PointRec) (blockSize : int) : PointRec = 
        { x = int (ceil ((float backgroundSize.x / float 2) - (float blockSize / float 2)))
          y = 1 }
    
    // #region
    // 이 단계에서는 쌓아놓은 줄을 채워넣기만 한다. timer 초기화는 새 벽돌을 만드는 단계가 성공했을 때 한다.
    // 이 함수에서, 따로 검사를 해야 할 만큼 복잡해보이는 구절은 splitBackgroundAndOverflow 뿐이다.
    // TODO 번거롭지만, 나머지는 한꺼번에 묶어서 검사하기.
    // #endregion
    let pushReceivedLines (gameEvent : GameEvent) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec) -> 
            match gameEvent with
            | GameEvent.TimeElapsed -> 
                let newBackground, removedDrawingCommandList = 
                    splitBackgroundAndOverflow playNoCurrentBlockRec.detailState.background 
                        playNoCurrentBlockRec.detailState.backgroundSize playNoCurrentBlockRec.detailState.receivedLines 
                        IOMod.IOCommand.RemoveBackground
                let receivedLinesThatAllocatedId, detailState = 
                    BlockManipulationMod.reassignDrawingObjectId playNoCurrentBlockRec.detailState.receivedLines 
                        playNoCurrentBlockRec.detailState
                
                let io = 
                    io
                    // 바닥부터 쌓아올린 결과, 꼭대기부터 시작해서 넘치게 된 벽돌들은 지운다.
                    |> (fun io -> List.fold IOMod.foldF4WriteIOCommand io removedDrawingCommandList)
                    // 바닥부터 쌓아올린 결과, 원래 있었던 벽돌들 중 범위를 넘지 않은 벽돌들은 위치가 바뀌었다.
                    |> (fun io -> 
                    Map.toSeq newBackground
                    |> Seq.map IOMod.IOCommand.MoveBackground
                    |> Seq.fold IOMod.foldF4WriteIOCommand io)
                    // 쌓아놓은 줄은 이제 화면에 보이면 안된다.
                    |> (fun io -> 
                    List.fold IOMod.foldF4WriteIOCommand io 
                        (List.map IOMod.IOCommand.RemoveReceivedLines 
                             (playNoCurrentBlockRec.detailState.receivedLines |> List.map snd)))
                    // 바닥부터 새로 쌓아올렸던 줄들은 화면에 새로 보이기 시작해야 한다.
                    |> (fun io -> 
                    List.fold IOMod.foldF4WriteIOCommand io 
                        (List.map IOMod.IOCommand.AddBackground receivedLinesThatAllocatedId))
                
                let detailState = 
                    let newBackground = 
                        List.fold (fun result (point, blockCell) -> Map.add point blockCell result) newBackground 
                            receivedLinesThatAllocatedId 
                        |> BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize
                    { detailState with background = newBackground
                                       receivedLines = [] }
                
                if List.isEmpty removedDrawingCommandList then 
                    let gameState = 
                        GameState.PlayNoCurrentBlock({ playNoCurrentBlockRec with detailState = detailState })
                    let log = LogMod.Log.Info "pushReceivedLines ok: playNoCurrentBlock."
                    Some(), ((gameState, io), LogMod.writeLog log logger)
                else 
                    let gameState = 
                        GameState.Stop({ currentBlock = None
                                         detailState = detailState })
                    
                    let log = LogMod.Log.Info "pushReceivedLines ok: stop."
                    Some(), ((gameState, io), LogMod.writeLog log logger)
            | _ -> 
                let log = LogMod.Log.Error(sprintf "pushReceivedLines failed: unexpected game event. %A" gameEvent)
                None, ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "pushReceivedLines failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let tryPutNewBlock ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec) -> 
            let startPoint = 
                calculateStartPoint playNoCurrentBlockRec.detailState.backgroundSize 
                    playNoCurrentBlockRec.detailState.blockSize
            let blockMap = BlockManipulationMod.manipluateCoordinate (playNoCurrentBlockRec.nextBlock) startPoint // TODO 좌표바꾸기.
            if BlockManipulationMod.testOverlap blockMap playNoCurrentBlockRec.detailState.background then 
                // #region
                // 다음 벽돌을 배경에 겹쳐봤을 때, 겹치는 벽돌이 있으면, 게임을 종료한다.
                // 이 단계에서는, 내려오는 벽돌이 없으므로, 이에 관해서는 지우거나 이동하거나 새로 만들 것이 없고
                // 다음 벽돌을 보이지 않을 것이므로, 다음 벽돌을 지우는 것이 필요하다.
                // 아래 구절들은 단순해서 따로 분리할 필요가 없겠다.
                // #endregion
                let io = 
                    (FSharpx.Collections.Map.values blockMap)
                    |> (Seq.map IOMod.IOCommand.RemoveNextBlock)
                    |> Seq.fold IOMod.foldF4WriteIOCommand io
                
                // TODO 새 벽돌을 그려봤는데, 잘 안됐더라라는 뜻으로, currentBlock 이 있어야 한다.
                let gameState = 
                    GameState.Stop({ currentBlock = None
                                     detailState = playNoCurrentBlockRec.detailState })
                
                let log = LogMod.Log.Info "tryPutNewBlock ok: stop."
                Some(), ((gameState, io), LogMod.writeLog log logger)
            else 
                // 1. 다음 벽돌을 새 벽돌로 바꾸기
                let newBlockMap, detailState = 
                    BlockManipulationMod.reassignDrawingObjectIdAsMap blockMap playNoCurrentBlockRec.detailState
                
                let io = 
                    io
                    // 2. 다음 벽돌의 drawing object 를 지우기
                    |> (fun io -> 
                    (FSharpx.Collections.Map.values blockMap |> Seq.map IOMod.IOCommand.RemoveNextBlock) 
                    |> Seq.fold IOMod.foldF4WriteIOCommand io)
                    // 3. 새 벽돌의 drawing object 만들기
                    |> (fun io -> 
                    Seq.fold IOMod.foldF4WriteIOCommand io 
                        (Seq.map IOMod.IOCommand.AddBackground (Map.toSeq newBlockMap)))
                    // 4. 새 벽돌을 처음 내렸으면, 일정시간 기다린다.
                    |> IOStateMod.bind (IOMod.writeIOCommand (IOMod.IOCommand.Timer(TimerCommand.SetAsWait)))
                
                // 5. 예상낙하지점에 사용할 block cell 만들기.
                // 이 구절은 많이 이상한데, block cell 만 만들고, 실제 drawing command 는 다음 단계인 expectedBlock 에서 만든다.
                let expectedBlockCell, detailState = 
                    BlockManipulationMod.reassingDrawingObjectIdAsBlockCellList newBlockMap detailState
                
                let gameState = 
                    GameState.Play({ currentBlock = (startPoint, newBlockMap)
                                     expectedBlockCell = expectedBlockCell
                                     nextBlock = Map.empty // 다음 벽돌은 이 단계가 아닌 다음 단계인 generateNextBlock 에서 만든다.
                                     detailState = detailState })
                
                let log = LogMod.Log.Info "tryPutNewBlock ok: play."
                // 아래와 같이 결론을 만든 뒤, 이어서 expectedBlock 을 쓸 수도 있지만, 이 함수에 이어서 쓰든 여기서 쓰든 결론은 같은 말이다.
                Some(), ((gameState, io), LogMod.writeLog log logger)
        // #region
        // 이 함수에서 Stop 상태를 인식하도록 하는 방법과 바깥의 합성단계에서 StateEitherMod.guard 를 쓰는 방법이 있는데,
        // 두 번째 방법으로 했다.
        // #endregion
        | _ -> 
            let log = LogMod.Log.Error "unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    // #region
    // 원래 구상은, 새 벽돌 만들기 따로, 만든 새 벽돌에 대해 drawing command 만들기 따로 하는 것이었는데,
    // 아래와 같이 하나로 묶는 것이 나은 듯 하다.
    // #endregion
    let generateNextBlock ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        // 이미 다음 벽돌이 계산되어 있는데 함수가 사용되면, 실패.
        | GameState.Play(playRec) when Map.isEmpty playRec.nextBlock -> 
            let random1 = BlockManipulationMod.appliedLCS playRec.detailState.nextSeed
            let random2 = BlockManipulationMod.appliedLCS random1
            let blockMap, (_, logger) = 
                playRec.detailState.createBlock random1 random2 (Point 0 0) |> StateMod.run ((), logger) // 다음 벽돌은 0,0 을 기준으로 화면에 그린다.
            let blockMap, detailState = 
                BlockManipulationMod.reassignDrawingObjectIdAsMap blockMap 
                    { playRec.detailState with nextSeed = random2 }
            
            let gameState = 
                GameState.Play({ playRec with nextBlock = blockMap
                                              detailState = detailState })
            
            // 다음 벽돌을 그린다.
            let io = Seq.fold IOMod.foldF4WriteIOCommand io (Map.toSeq blockMap |> Seq.map IOMod.IOCommand.AddNextBlock)
            let log = LogMod.Log.Info(sprintf "generateNextBlock ok: %A" blockMap)
            Some(), ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "generateNextBlock failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
