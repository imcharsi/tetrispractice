﻿namespace TetrisLibrary


(*
module ReactiveTest = 
    open NUnit.Framework
    open TetrisGameLibraryMod
    open TetrisBlockLibraryMod
    open System.Reactive.Concurrency
    open TetrisGameLibraryModTest
    
    type A = int
    
    [<Test>]
    let testReactiveScan() : unit = 
        let backgroundSize = PointF 10 10
        let background = prepareInitialBackground backgroundSize
        
        let detailState : DetailState = 
            { background = background
              backgroundSize = backgroundSize
              speed = 1
              nextSeed = 1
              count = 1
              receivedLines = [] }
        
        let subject = new System.Reactive.Subjects.Subject<GameEvent>()
        let stream = 
            FSharp.Control.Reactive.Observable.scanInit 
                (PlayState.Stop(None, detailState), emptyDrawingPlan, emptyDrawingPlan4ReceivedLines) 
                (scanF 5 (PointF 10 10) InitialBackgroundMod.checkPointMap5x5 
                     (createBlock InitialBackgroundMod.checkPointMap5x5))
        
        let subscribeF (playState : PlayState, drawingPlan : DrawingPlan, 
                        drawingPlan4ReceivedLines : DrawingPlan4ReceivedLines) : unit = 
            System.Console.WriteLine(sprintf "thread id %A" System.Threading.Thread.CurrentThread.ManagedThreadId)
            match playState with
            | PlayState.PlayAndNeedNewBlock(detailState) -> 
                System.Console.WriteLine("play and ...")
                System.Console.WriteLine(sprintf "%A" detailState)
                printBackground detailState.backgroundSize detailState.background
                Scheduler.Default.Schedule(System.TimeSpan.FromMilliseconds(float 0), 
                                           (fun () -> 
                                           System.Console.WriteLine("elapsed.")
                                           subject.OnNext GameEvent.TimeElapsed))
                |> ignore
            | PlayState.Play(currentPoint, currentBlock, detailState) -> 
                System.Console.WriteLine("play")
                System.Console.WriteLine(sprintf "%A" currentPoint)
                printBlock currentPoint currentBlock
                printBackground detailState.backgroundSize detailState.background
                Scheduler.Default.Schedule
                    (System.TimeSpan.FromMilliseconds(float 0), (fun () -> subject.OnNext GameEvent.TimeElapsed)) 
                |> ignore
            | PlayState.Stop(lastBlock, detailState) -> 
                System.Console.WriteLine("stop")
                printBackground detailState.backgroundSize detailState.background
                match lastBlock with
                | Some(point, block) -> 
                    printBlock point block
                    System.Console.WriteLine(sprintf "%A" point)
                | None -> ()
                Scheduler.Default.Schedule(System.TimeSpan.FromMilliseconds(float 0), (fun () -> subject.OnCompleted())) 
                |> ignore
            | _ -> failwith ""
        
        let disposable = 
            subject
            |> stream
            |> FSharp.Control.Reactive.Observable.subscribeOn NewThreadScheduler.Default
            |> FSharp.Control.Reactive.Observable.observeOn Scheduler.CurrentThread
            |> FSharp.Control.Reactive.Observable.subscribe subscribeF
        
        System.Threading.Thread.Sleep(System.TimeSpan.FromMilliseconds(float 10))
        subject.OnNext GameEvent.Start
        System.Threading.Thread.Sleep(System.TimeSpan.FromSeconds(float 1))

        *)
