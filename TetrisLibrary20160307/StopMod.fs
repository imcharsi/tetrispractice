﻿namespace TetrisLibrary

module StopMod = 
    open DefinitionMod
    open FunctionalUtil
    
    let start (gameEvent : GameEvent) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.Stop(stopRec) -> 
            match gameEvent with
            | GameEvent.Start -> 
                // #region
                // 현재 내려오고 있는 벽돌이 없으므로, subscribe 단계에서 timer 를 시작하는 side-effect 를 실행해야 한다.
                // 게임 종료인 상태로 화면에 보여지고 있었던 벽돌들을 전부 지워야 한다.
                // #endregion
                let io = 
                    stopRec.detailState.background
                    |> FSharpx.Collections.Map.values
                    |> Seq.filter (fun x -> 
                           match x with
                           | BlockCell.NormalBlockCell(_) -> true
                           | _ -> false)
                    |> Seq.map IOMod.IOCommand.RemoveBackground
                    |> Seq.fold IOMod.foldF4WriteIOCommand io
                
                let io = 
                    stopRec.detailState.receivedLines
                    |> List.map (snd >> IOMod.IOCommand.RemoveReceivedLines)
                    |> List.fold IOMod.foldF4WriteIOCommand io
                
                let io = 
                    Option.map snd stopRec.currentBlock
                    |> Option.map Map.toList
                    |> Option.toList
                    |> List.collect (List.map (snd >> IOMod.IOCommand.RemoveBackground))
                    |> List.fold IOMod.foldF4WriteIOCommand io
                    |> IOStateMod.bind (IOMod.writeIOCommand (IOMod.IOCommand.Timer(TimerCommand.SetAsImmediate)))
                
                let random1 = BlockManipulationMod.appliedLCS stopRec.detailState.nextSeed
                let random2 = BlockManipulationMod.appliedLCS random1
                let blockMap, (_, logger) = 
                    stopRec.detailState.createBlock random1 random2 (Point 0 0) |> StateMod.run ((), logger)
                
                // #region
                // 이와 같은 구성을 이렇게까지 미리 예상하고 한 것은 아니었는데,
                // 이 단계 이후 실행되는 TimeElapsed 에서, 다음 벽돌의 drawing object 를 지우게 되고,
                // 또한 다음 벽돌을 새 벽돌로 사용하는 과정에서 따로 drawing object id 를 할당하게 된다.
                // 그런데, BlockCellRec 의 drawingObjectId 는 Option 이기 때문에 값을 안 써도 된다.
                // 따라서, 여기서는 다음 벽돌을 계산하고 drawingObjectId 를 안 쓰면,
                // 다음 TimeElapsed 를 처리할 때, 여기서 계산한 다음 벽돌에 대한 drawing object 는 결과적으로 지울 것이 없으면서,
                // 여기서 계산한 다음 벽돌에 대해 배경에 그릴 drawing object id 를 할당하게 된다.
                // 마치 애초에 그린 것이 없으므로, 지울 것도 없는 것과 같은 말이 된다.
                // 이와 같은 구성이 자연스러울려면, createBlock 은 기본으로 drawing object id 가 없어야 한다.
                // 이 단계에서, timer 가 즉시 만료하도록 설정해야 한다. 예를 들어 시작을 눌렸는데, 1초 있다가 게임이 시작하면 이상하다.
                // #endregion
                let gameState = 
                    GameState.PlayNoCurrentBlock({ detailState = 
                                                       { stopRec.detailState with speed = 1
                                                                                  counter = 1
                                                                                  nextSeed = random2
                                                                                  background = 
                                                                                      BlockManipulationMod.drawBackgroundBorder 
                                                                                          stopRec.detailState.backgroundSize 
                                                                                          Map.empty
                                                                                  receivedLines = [] }
                                                   nextBlock = blockMap })
                
                let log = LogMod.Log.Info "start ok."
                Some(), ((gameState, io), LogMod.writeLog log logger)
            | _ -> 
                let log = LogMod.Log.Error(sprintf "start failed: unexpected game event. %A" gameEvent)
                None, ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "start failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let stop (gameEvent : GameEvent) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState, gameEvent with
        | GameState.Play(playRec), GameEvent.Stop -> 
            let log = LogMod.Log.Error "stop ok."
            
            let stopRec : StopRec = 
                { currentBlock = Some playRec.currentBlock
                  detailState = playRec.detailState }
            
            let io = 
                playRec.expectedBlockCell
                |> List.map (IOMod.IOCommand.RemoveExpectedBlock)
                |> List.fold IOMod.foldF4WriteIOCommand io
            
            let io = 
                playRec.nextBlock
                |> FSharpx.Collections.Map.values
                |> Seq.filter (fun x -> 
                       match x with
                       | BlockCell.NormalBlockCell(_) -> true
                       | _ -> false)
                |> Seq.map IOMod.IOCommand.RemoveNextBlock
                |> Seq.fold IOMod.foldF4WriteIOCommand io
            
            Some(), ((GameState.Stop(stopRec), io), LogMod.writeLog log logger)
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), GameEvent.Stop -> 
            let log = LogMod.Log.Error "stop ok."
            
            let stopRec : StopRec = 
                { currentBlock = None
                  detailState = playNoCurrentBlockRec.detailState }
            
            let io = 
                playNoCurrentBlockRec.nextBlock
                |> FSharpx.Collections.Map.values
                |> Seq.filter (fun x -> 
                       match x with
                       | BlockCell.NormalBlockCell(_) -> true
                       | _ -> false)
                |> Seq.map IOMod.IOCommand.RemoveNextBlock
                |> Seq.fold IOMod.foldF4WriteIOCommand io
            
            Some(), ((GameState.Stop(stopRec), io), LogMod.writeLog log logger)
        | _, _ -> 
            let log = LogMod.Log.Error "stop failed: unexpected game state/event."
            None, ((gameState, io), LogMod.writeLog log logger)
