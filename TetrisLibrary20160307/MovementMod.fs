﻿namespace TetrisLibrary

module MovementMod = 
    open DefinitionMod
    open FunctionalUtil
    
    let splitFilledResultAndNotFilledResult (unionedBackground : BlockMap) (detailState : DetailStateRec) : List<int> * List<int> = 
        // #region
        // 더 이상 bool 은 필요없고, 또한 어차피 뒤집기도 해야 하므로,
        // List.map |> List.rev 보다 아래와 같이 하는 것이 조금이라도 덜 하는 방법이다.
        // 왜 뒤집어야 하는가.
        // 깨진 줄이 10, 9, 7 번 줄이고 안 깨진 줄이 8, 6, 5 번 줄이면
        // 깨진 줄은 10->10, 9->9, 7->8 번으로 줄이 바뀌어야 하고
        // 안 깨진 줄 역시 8->10, 6->9, 5->8 과 같이 바뀌어야 한다.
        // 만약 7,9,10 과 같이 놓으면, 바닥부터 줄을 맞추기 번거로워진다.
        // 7->8, 9->9, 10->10 과 같이 되도록 아래의 prepareLookupMap 의 tarList 의 시작을 계산해야 하는 번거로움이 있기 때문이다.
        // 결론은, 이 계산 결과와 섞어서 하게 될 다른 계산의 편의를 위해서 여기서 미리 뒤집어놓고 결과를 돌려준다.
        // #endregion
        let reverse = List.fold (fun result (y, _) -> y :: result) []
        [ 1..(detailState.backgroundSize.y) ]
        |> List.map (fun y -> 
               y, 
               [ Point ]
               |> FunctionalUtil.ListMod.apply [ 1..(detailState.backgroundSize.x) ]
               |> FunctionalUtil.ListMod.apply [ y ])
        |> List.map (TupleMod.map (id, List.forall (fun point -> Map.containsKey point unionedBackground)))
        |> List.partition (fun (y, isFilled) -> isFilled)
        |> TupleMod.map (reverse, reverse)
    
    // #region
    // f# pipe-line 문법을 활용하기 위해 tarList 와 srcList 를 순서를 바꿨다.
    // srcList 의 각 y 를 tarList 의 각 y 로 대응시키는 map 을 만드는 것이 이 함수의 사용목적이다.
    // #endregion
    let prepareLookupMap (detailState : DetailStateRec) (tarYList : List<int>) (srcYList : List<int>) : Map<PointRec, PointRec> = 
        FunctionalUtil.ListMod.zip tarYList srcYList
        |> List.collect (fun (srcY, tarY) -> 
               [ (fun x (y1, y2) -> Point x y1, Point x y2) ]
               |> FunctionalUtil.ListMod.apply [ 1..(detailState.backgroundSize.x) ]
               |> FunctionalUtil.ListMod.apply [ (srcY, tarY) ])
        |> Map.ofList
    
    let clearDrawingObjectId (blockCell : BlockCell) : BlockCell = 
        match blockCell with
        | BlockCell.NormalBlockCell({ blockType = blockType; drawingObjectId = _ }) -> 
            BlockCell.NormalBlockCell({ blockType = blockType
                                        drawingObjectId = None })
        | BlockCell.InvisibleBorder -> BlockCell.InvisibleBorder
    
    let mapF4TryLeftRightRotation (block : Block) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : StateOptionMod.Result<unit, GameState * IOMod.IO, LogMod.Logger> = 
        match gameState with
        | GameState.Play(playRec) -> 
            let io = 
                snd block
                |> Map.toSeq
                |> Seq.map IOMod.IOCommand.MoveBackground
                |> Seq.fold IOMod.foldF4WriteIOCommand io
            
            let gameState = GameState.Play({ playRec with currentBlock = block })
            let log = LogMod.Log.Info "mapF4TryLeftRightRotation ok."
            Some(), ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "mapF4TryLeftRightRotation failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let tryLeftRightRotation (rotationMap : Map<PointRec, int * (PointRec * PointRec)>) (gameEvent : GameEvent) 
        ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        // #region
        // compositedF 에 의해 이동시도 결론이 Left 이면 상태가 복구되어 아무것도 하지 않았던 것처럼 된다.
        // #endregion
        let compositedF = 
            // 벽돌을 움직일 수 없을 때, 표시를 한다.
            let orElseF (_ : unit) : StateOptionMod.StateOption<unit, GameState * IOMod.IO, LogMod.Logger> = 
                let bindF (_ : unit) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
                    let bindF (_ : unit) : IOMod.IO = IOMod.writeIOCommand (IOMod.IOCommand.Notification)
                    Some(), ((gameState, StateMod.bind bindF io), logger)
                StateOptionMod.unit() |> StateOptionMod.bind bindF
            StateOptionMod.bind mapF4TryLeftRightRotation
            >> StateOptionMod.keepState
            >> StateOptionMod.orElse orElseF
        
        let bindF (log : LogMod.Log) (o : Option<unit>) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
            o, ((gameState, io), LogMod.writeLog log logger)
        match gameState with
        | GameState.Play(playRec) -> 
            // 원래는, |> StateMod.run ((gameState, io), logger) 구절이 아래 match 의 결론의 입력이 되도록 했었는데,
            // 그와 같이 하면 coverage 검사가 정확하게 되지 않는다. match 전체가 StateMod.run 의 입력으로서 실행되는 것처럼 인식되는 듯 하다.
            // 따라서 번거롭지만 아래와 같이 하는 것이 좋은 듯 하다. 이와 같은 변경은, DragMovement 기능 추가에 따른 미검사부분을 확인하는 과정에서 하게 되었다.
            match gameEvent with
            | GameEvent.Movement Movement.Left -> 
                compositedF BlockManipulationMod.left |> StateMod.run ((gameState, io), logger)
            | GameEvent.Movement Movement.Right -> 
                compositedF BlockManipulationMod.right |> StateMod.run ((gameState, io), logger)
            | GameEvent.Movement(Movement.Rotation rotationDirection) -> 
                compositedF (BlockManipulationMod.rotate rotationMap rotationDirection) 
                |> StateMod.run ((gameState, io), logger)
            | GameEvent.DragMovement(diff, None) -> // TODO 검사하기.
                match diff with
                | { x = x; y = y } when x <> 0 -> 
                    if x < 0 then compositedF (BlockManipulationMod.rightN 1)
                    else compositedF (BlockManipulationMod.leftN 1)
                | _ -> 
                    let log = LogMod.Log.Error(sprintf "tryLeftRightRotation failed: not horizontal move.")
                    StateMod.bind (bindF log) (StateOptionMod.none())
                |> StateMod.run ((gameState, io), logger)
            | _ -> 
                let log = LogMod.Log.Error(sprintf "tryLeftRightRotation failed: unexpected game event %A" gameEvent)
                StateOptionMod.none()
                |> StateMod.bind (bindF log)
                |> StateMod.run ((gameState, io), logger)
        | _ -> 
            let log = LogMod.Log.Error "unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let tryDown (gameEvent : GameEvent) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : StateOptionMod.Result<bool * Block, GameState * IOMod.IO, LogMod.Logger> = 
        match gameState with
        | GameState.Play(playRec) -> 
            match gameEvent with
            | GameEvent.TimeElapsed -> 
                // #region
                // 시간만료로 인한 한줄 내리기이면, 일단 timer 가 일정시간 이후에 만료되도록 설정해놓는다.
                // 바닥에 닿았다면, 여기서의 timer 설정이 덮어쓰여질 것이고,
                // 바닥에 닿지 않았다면, 여기서의 timer 설정은 그대로 유지될 것이다.
                // 이 주석의 아래에서 다루는 내용은 상당히 복잡한 내용이다.
                // 애초에 이와 같이 복잡하게 구상을 하진 않았는데,
                // 문제의 발단은 tryLeftRightRotation 가 drawing command 만들기에 대한 최적화를 하지 않았음을 알게 되었다는 것이고,
                // tryDown 은 최적화를 하는가에 대해 살펴보던 바 최적화를 하고 있다는 결론을 알게 되는 과정에서 설명이 복잡해졌다.
                // tryDown 의 경우 tryLeftRightRotation 처럼 아무것도 그리지 않는 최적화를 인위적으로 하기가 복잡한데,
                // 그러나 최적화를 할 필요가 없다.
                // 왜냐하면, bindF4TryDown 에서 벽돌이 바닥에 닿았는지를 판단하여, 벽돌이 바닥에 닿지 않았다면 당연히 벽돌의 이동을 다뤄야 할 것이고
                // 벽돌이 바닥에 닿았다면 더 이상 벽돌의 움직임은 없게 되지만 깨지는 줄이 있다면, 내려오는 벽돌의 일부에 대한 drawing command 를 만들어야 하는 경우가 된다.
                // 깨지는 줄도 없다면, 새 벽돌 그리기는 별개로 치고, 내려오는 벽돌에 대한 drawing command 는 아무것도 만들어지지 않게 되어, 결국 이 경우에는 최적화를 한 것처럼 된다.
                // #endregion
                BlockManipulationMod.downN 1
                |> StateOptionMod.bind (IOMod.bindF4WriteIOCommand (IOMod.IOCommand.Timer(TimerCommand.SetAsWait)))
                |> StateMod.run ((GameState.Play(playRec), io), logger)
            | GameEvent.Movement(Movement.Down) -> 
                BlockManipulationMod.downN 1 |> StateMod.run ((GameState.Play(playRec), io), logger)
            | GameEvent.Movement(Movement.DropDown) -> 
                BlockManipulationMod.downN playRec.detailState.backgroundSize.y 
                |> StateMod.run ((GameState.Play(playRec), io), logger)
            | GameEvent.DragMovement(diff, None) -> // TODO 검사하기.
                match diff with
                | { y = y } when y < 0 -> 
                    BlockManipulationMod.downN 1 |> StateMod.run ((GameState.Play(playRec), io), logger)
                | _ -> 
                    let log = LogMod.Log.Info "tryDown failed: not vertical move."
                    Some(false, playRec.currentBlock), ((GameState.Play(playRec), io), LogMod.writeLog log logger)
            | _ -> 
                let log = LogMod.Log.Error "tryDown failed: unexpected game event."
                None, ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "tryDown failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    // #region
    // 첫번째 반환값은 상대방에게 넘어갈 깨진 줄이고,
    // 두번째 반환값은 화면에서 지워질 drawing object 를 포함하는 줄이고,
    // 세번째 반환값은 새 배경이다.
    // #endregion
    let calculateRemovedLines (unionedBackground : BlockMap) (detailState : DetailStateRec) : (BlockList * BlockList) * BlockMap = 
        let prepareLookupMap = prepareLookupMap detailState (List.rev [ 1..(detailState.backgroundSize.y) ])
        splitFilledResultAndNotFilledResult unionedBackground detailState
        |> TupleMod.map (prepareLookupMap, prepareLookupMap)
        |> TupleMod.map 
               ((fun removedPointMap -> 
                (FunctionalUtil.MapMod.lookup Map.empty removedPointMap detailState.background), 
                (FunctionalUtil.MapMod.lookup Map.empty removedPointMap unionedBackground)), 
                (fun remainedPointMap -> FunctionalUtil.MapMod.lookup Map.empty remainedPointMap unionedBackground))
        |> TupleMod.map ((TupleMod.map (Map.toList, Map.toList)), id)
    
    let foldF4TryDown (nextBlock : BlockMap) (detailState : DetailStateRec) (io : IOMod.IO) (logger : LogMod.Logger) 
        ((removedLines : BlockList, removedLines4DrawingPlan : BlockList), newBackground : BlockMap) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        let io = 
            io
            |> (fun io -> 
            (List.map snd removedLines4DrawingPlan)
            |> List.map IOMod.IOCommand.RemoveBackground
            |> List.fold IOMod.foldF4WriteIOCommand io)
            |> (fun io -> 
            (Map.toSeq newBackground)
            |> Seq.map IOMod.IOCommand.MoveBackground
            |> Seq.fold IOMod.foldF4WriteIOCommand io)
            |> IOStateMod.bind (IOMod.writeIOCommand (IOMod.IOCommand.Timer(TimerCommand.SetAsImmediate)))
            |> (fun io -> 
            IOStateMod.bind 
                (IOMod.writeIOCommand 
                     (IOMod.IOCommand.TransferLines(List.map (TupleMod.map (id, clearDrawingObjectId)) removedLines))) 
                io)
        
        // #region
        // 이 함수는 내려오던 벽돌이 바닥에 닿았을 때 사용된다. 따라서, drawing object 중에 움직일 만한 것은 없고, 지울 것만 있다.
        // 내려오던 벽돌에 속하던 drawing object 중 지워지지 않는 것은, 깨지지 않아서 배경에 그대로 남게 된다.
        // 이 말은, clearedLines 에 대입되는 blockCell 들은 배경으로부터 지우는 단계를 거칠 필요가 없다는 말이다.
        // 벽돌이 바닥에 닿으면, 새 timer 가 즉시 만료되도록 정한다.
        // 예를 들어, 벽돌이 바닥에 닿은 후 1 초 기다렸다가 새 벽돌이 나오도록 하면 이상하다.
        // 한편, 여기서 GameState 가 바뀌게 된다.
        // #endregion
        let playNoCurrentBlockRec : PlayNoCurrentBlockRec = 
            { nextBlock = nextBlock
              detailState = 
                  { detailState with background = 
                                         BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize 
                                             newBackground } }
        
        let gameState = GameState.PlayNoCurrentBlock(playNoCurrentBlockRec)
        
        let logger = 
            if List.isEmpty removedLines |> not then 
                let log = LogMod.Log.Info "foldF4TryDown ok: lines were breaked."
                LogMod.writeLog log logger
            else logger
        Some(), ((gameState, io), logger)
    
    // #region
    // 이 함수는 이름만 봐서는 취지를 알 수가 없는데, tryDown 을 시도한 이후의 처리를 한다.
    // 바닥에 닿았으면, 깨지는 줄이 있는지 확인하고, 줄정리를 한다.
    // 바다에 닿지 않았으면, 왼쪽오른쪽/회전과 같이 다룬다.
    // #endregion
    let bindF4TryDown (reachBottom : bool, newBlock : Block) 
        ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.Play(playRec) -> 
            if reachBottom then 
                // #region
                // 깨진 줄만 고를 때는, 겹치기 전의 배경을 기준으로 고르고
                // 남겨진 줄만 고를 때는, 겹치고 난 후의 배경을 기준으로 고른다.
                // 1111
                // 1..1
                // 11.1
                // 1111 에서
                // 1111
                // 1.11
                // 1111
                // 1111 과 같이 벽돌을 내렸을 때,
                // .1.. 과 같이 깨진 줄을 골라야 하고,
                // 1111
                // 1..1
                // 1.11
                // 1111 과 같이 배경이 남아야 한다.
                // 한편, drawing object 를 지우기 위해 겹쳐진 배경을 기준으로 또 골라야 할 필요가 있다.
                // 앞에서 했던 구상보다, 좀 더 단순해졌다.
                // foldF4TryDown 에서는 예상낙하지점 벽돌을 지울 수 없다. 미리 지워놓고 다음 동작을 하면 된다.
                // #endregion
                let io = 
                    playRec.expectedBlockCell
                    |> List.map IOMod.IOCommand.RemoveExpectedBlock
                    |> List.fold IOMod.foldF4WriteIOCommand io
                
                let unionedBackground = FSharpx.Collections.Map.union playRec.detailState.background (snd newBlock)
                let log = LogMod.Log.Info "bindF4TryDown ok: reached at bottom."
                calculateRemovedLines unionedBackground playRec.detailState 
                |> TupleMod.fold (foldF4TryDown playRec.nextBlock playRec.detailState io (LogMod.writeLog log logger))
            else 
                let log = LogMod.Log.Info "bindF4TryDown ok: not reached at bottom."
                mapF4TryLeftRightRotation newBlock ((gameState, io), (LogMod.writeLog log logger))
        | _ -> 
            let log = LogMod.Log.Error "bindF4TryDown failed: unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
    
    let expectedBlock (ioCommandF : PointRec * BlockCell -> IOMod.IOCommand) 
        ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState with
        | GameState.Play(playRec) -> 
            let bindF (commandList : List<IOMod.IOCommand>) 
                ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
                Some(), ((gameState, List.fold IOMod.foldF4WriteIOCommand io commandList), logger)
            // #region
            // TODO 만약 expectedBlockCell 의 길이와 pointList 의 길이가 다른 경우가 생긴다면.
            // 이런 일은 있어서는 안 된다. 이에 대한 대비가 없다. 일단 넘어가기.
            // #endregion
            let log = LogMod.Log.Info "expectedBlock ok."
            BlockManipulationMod.downN playRec.detailState.backgroundSize.y
            |> StateOptionMod.map snd
            |> StateOptionMod.map (fun (_, blockMap) -> FSharpx.Collections.Map.keys blockMap |> List.ofSeq)
            |> StateOptionMod.map (fun pointList -> FunctionalUtil.ListMod.zip pointList playRec.expectedBlockCell)
            |> StateOptionMod.map (List.map ioCommandF)
            |> StateOptionMod.bind bindF
            // 왜 이와 같이 하는가. 예상낙하지점을 그리면서 downN 을 여러번실행하게 되는데,
            // 이때의 이력은 남길 필요가 없다. 따라서 이와 같이 한다.
            |> StateMod.run ((gameState, io), LogMod.emptyLogger)
            |> TupleMod.map (id, (fun ((gameState, io), _) -> (gameState, io), LogMod.writeLog log logger))
        | _ -> 
            let log = LogMod.Log.Error "unexpected game state."
            None, ((gameState, io), LogMod.writeLog log logger)
