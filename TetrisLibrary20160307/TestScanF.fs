﻿namespace TetrisLibrary

module TestScanFMod = 
    open NUnit.Framework
    open Microsoft.Reactive.Testing
    open System
    open System.Reactive.Concurrency
    open System.Reactive.Subjects
    open FunctionalUtil
    open DefinitionMod
    
    [<Test>]
    let testScanF() : unit = 
        // #region
        // TODO 필요한 검사만 비교적 쉽게 하는 방법은, 상황에 맞게 배경에 벽돌 일부를 미리 그려놓는 것인데,
        // .... 예를 들어 특정 벽돌을 내리면 줄이 깨지도록 미리 벽돌을 그려놓는 등이다.
        // 검사를 실행한 결과 가장 마지막 화면은 아래와 같다. 첫화면은 배경이고, 두번째 화면은 예상낙하지점이고, 세번째 화면은 다음 벽돌이다.
        // IIIIIIIIIIII
        // I..........I
        // I..........I
        // I...111....I
        // I.....1....I
        // I..........I
        // I..........I
        // I..........I
        // I..........I
        // I..355.....I
        // I.33355....I
        // IIIIIIIIIIII
        // IIIIIIIIIIII
        // I..........I
        // I..........I
        // I..........I
        // I..........I
        // I..........I
        // I..........I
        // I..........I
        // I...111....I
        // I.....1....I
        // I..........I
        // IIIIIIIIIIII
        // .....
        // ..66.
        // ..66.
        // .....
        // .....
        // #endregion
        let testScheduler : TestScheduler = new TestScheduler()
        let subject : Subject<GameEvent> = new Subject<GameEvent>()
        
        let detailState : DetailStateRec = 
            { backgroundSize = Point 10 10
              background = BlockManipulationMod.drawBackgroundBorder (Point 10 10) Map.empty
              blockSize = 5
              createBlock = BlockManipulationMod.Block5x5Mod.createBlock
              speed = 1
              nextSeed = 257648005
              counter = 1
              receivedLines = [] }
        
        let stopRec : StopRec = 
            { currentBlock = None
              detailState = detailState }
        
        let backgroundPixelMap : Ref<Map<int, PointRec * BlockCell>> = ref Map.empty
        let expectedBlockPixelMap : Ref<Map<int, PointRec * BlockCell>> = ref Map.empty
        let nextBlockPixelMap : Ref<Map<int, PointRec * BlockCell>> = ref Map.empty
        let gameState = (GameState.Stop(stopRec), IOMod.nothingToDo), LogMod.emptyLogger
        
        let printPixelMap (detailState : DetailStateRec) (io : IOMod.IO) : unit = 
            backgroundPixelMap 
            := List.fold BlockManipulationMod.processIOCommand4Background (!backgroundPixelMap) (io ([], ())
                                                                                                 |> snd
                                                                                                 |> fst)
            nextBlockPixelMap := List.fold BlockManipulationMod.processIOCommand4NextBlock (!nextBlockPixelMap) (io 
                                                                                                                     ([], 
                                                                                                                      ())
                                                                                                                 |> snd
                                                                                                                 |> fst)
            expectedBlockPixelMap 
            := List.fold BlockManipulationMod.processIOCommand4ExpectedBlock (!expectedBlockPixelMap) (io ([], ())
                                                                                                       |> snd
                                                                                                       |> fst)
            !backgroundPixelMap
            |> FSharpx.Collections.Map.values
            |> Map.ofSeq
            |> BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize
            |> BlockManipulationMod.printBackground detailState.backgroundSize
            !expectedBlockPixelMap
            |> FSharpx.Collections.Map.values
            |> Map.ofSeq
            |> BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize
            |> BlockManipulationMod.printBackground detailState.backgroundSize
            !nextBlockPixelMap
            |> FSharpx.Collections.Map.values
            |> Map.ofSeq
            |> (fun pointMap -> BlockManipulationMod.printBlock detailState.blockSize ((Point 0 0), pointMap))
        
        let subscribeF (eitherGameState : (GameState * IOMod.IO) * LogMod.Logger) : unit = 
            match eitherGameState with
            | (GameState.Play(playRec), io), logger -> 
                System.Console.WriteLine("play")
                printPixelMap playRec.detailState io
                logger ([], ())
                |> snd
                |> fst
                |> List.rev
                |> sprintf "%A"
                |> System.Console.WriteLine
                |> ignore
            | (GameState.PlayNoCurrentBlock(playNoCurrentRec), io), logger -> 
                System.Console.WriteLine("playNoCur...")
                printPixelMap playNoCurrentRec.detailState io
                logger ([], ())
                |> snd
                |> fst
                |> List.rev
                |> sprintf "%A"
                |> System.Console.WriteLine
            | (GameState.Stop(stopRec), io), logger -> 
                System.Console.WriteLine("stop")
                printPixelMap stopRec.detailState io
                logger ([], ())
                |> snd
                |> fst
                |> List.rev
                |> sprintf "%A"
                |> System.Console.WriteLine
        
        let disposable = 
            ObsMod.scan StateMachineMod.GameMod.preF 
                (StateMachineMod.GameMod.scanF BlockManipulationMod.rotationPointMap) gameState subject
            |> FSharp.Control.Reactive.Observable.subscribeOn testScheduler
            // observeOn 에서도 testScheduler 를 쓰면, tick 을 두번 돌려야 한다.
            // |> FSharp.Control.Reactive.Observable.observeOn testScheduler
            |> FSharp.Control.Reactive.Observable.subscribe subscribeF
        
        let pushEvent (gameEvent : GameEvent) : System.IDisposable = 
            testScheduler.Schedule(TimeSpan.FromTicks(int64 1), (fun () -> subject.OnNext(gameEvent)))
        
        let eventList = 
            [ GameEvent.Start
              GameEvent.TimeElapsed
              GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
              GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
              GameEvent.Movement(Movement.DropDown)
              GameEvent.TimeElapsed
              GameEvent.Movement(Movement.Left)
              GameEvent.Movement(Movement.Left)
              GameEvent.Movement(Movement.Left)
              GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise))
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Down)
              GameEvent.Movement(Movement.Right)
              GameEvent.Movement(Movement.Down)
              GameEvent.TimeElapsed
              GameEvent.Movement(Movement.Rotation(RotationDirection.CounterClockwise)) ]
        List.fold (fun _ event -> pushEvent event |> ignore) () eventList
        // testScheduler.AdvanceBy(int64 (List.length eventList))
        testScheduler.Start()
        disposable.Dispose()
    
    [<Test>]
    let testPreF4ScanF() : unit = 
        let detailState : DetailStateRec = 
            { backgroundSize = Point 10 10
              background = BlockManipulationMod.drawBackgroundBorder (Point 10 10) Map.empty
              blockSize = 5
              createBlock = BlockManipulationMod.Block5x5Mod.createBlock
              speed = 1
              nextSeed = 257648005
              counter = 1
              receivedLines = [] }
        
        let stopRec : StopRec = 
            { currentBlock = None
              detailState = detailState }
        
        let gameState = GameState.Stop(stopRec)
        match StateMachineMod.GameMod.preF ((gameState, DefinitionMod.IOMod.nothingToDo), LogMod.emptyLogger) 
                  (GameEvent.DragMovement(Point 0 0, Some LogMod.emptyLogger)) with
        | _, GameEvent.DragMovement(_, None) -> ()
        | _ -> Assert.Fail()
        match StateMachineMod.GameMod.preF ((gameState, DefinitionMod.IOMod.nothingToDo), LogMod.emptyLogger) 
                  (GameEvent.NoOp(Some LogMod.emptyLogger)) with
        | _, GameEvent.NoOp(None) -> ()
        | _ -> Assert.Fail()
    
    [<Test>]
    let testPreF4ScanF4Mouse() : unit = 
        let mouseState = MouseMod.MouseState.Up
        match StateMachineMod.MouseMod.preF ((mouseState, StateMod.unit()), LogMod.emptyLogger) 
                  (MouseMod.MouseEvent.Down(Point 0 0, Some LogMod.emptyLogger)) with
        | _, MouseMod.MouseEvent.Down(_, None) -> ()
        | _ -> Assert.Fail()
        match StateMachineMod.MouseMod.preF ((mouseState, StateMod.unit()), LogMod.emptyLogger) 
                  (MouseMod.MouseEvent.NoOp(Some LogMod.emptyLogger)) with
        | _, MouseMod.MouseEvent.NoOp(None) -> ()
        | _ -> Assert.Fail()
    
    [<Test>]
    let testBindF4Mouse() : unit = 
        let mouseState = MouseMod.MouseState.Up
        let io1 = StateMod.unit() |> MouseMod.writeIOCommand (MouseMod.IOCommand.Move(Point 0 0))
        let io2 = StateMod.unit() |> MouseMod.writeIOCommand (MouseMod.IOCommand.NoOp)
        let logger = StateMod.unit() |> LogMod.writeLog (LogMod.Log.Info "")
        
        let f (gameEvent : GameEvent) : Option<LogMod.Logger> = 
            match gameEvent with
            | GameEvent.DragMovement(_, logger) -> logger
            | GameEvent.NoOp(logger) -> logger
            | _ -> None
        
        let expected = 
            [ GameEvent.DragMovement(Point 0 0, Some(logger)) ]
            |> List.map f
            |> List.collect (Option.map (fun l -> fst (snd (l ([], ())))) >> Option.toList)
        
        let actual = 
            StateMachineMod.MouseMod.mapF ((mouseState, io1), logger)
            |> List.map f
            |> List.collect (Option.map (fun l -> fst (snd (l ([], ())))) >> Option.toList)
        
        Assert.AreEqual(expected, actual)
        let actual = 
            StateMachineMod.MouseMod.mapF ((mouseState, io2), logger)
            |> List.map f
            |> List.collect (Option.map (fun l -> fst (snd (l ([], ())))) >> Option.toList)
        Assert.AreEqual(expected, actual)
        let actual = 
            StateMachineMod.MouseMod.mapF ((mouseState, io2), LogMod.emptyLogger)
            |> List.map f
            |> List.collect (Option.map (fun l -> fst (snd (l ([], ())))) >> Option.toList)
        Assert.AreEqual([ [] ], actual)
    
    [<Test>]
    let testScanF4Mouse() : unit = 
        let mouseState = MouseMod.MouseState.Up
        let io = StateMod.unit()
        // Up->Down
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) 
                (MouseMod.MouseEvent.Down(Point 0 0, None))
        let actual = fst (snd (resultIo ([], ())))
        let expected = [ MouseMod.IOCommand.NoOp ]
        Assert.AreEqual(expected, actual)
        match resultState with
        | MouseMod.MouseState.Down(point, _) when point = (Point 0 0) -> ()
        | _ -> Assert.Fail()
        // Up->Move
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) 
                (MouseMod.MouseEvent.Move(Point 0 0))
        let actual = fst (snd (resultIo ([], ())))
        let expected = []
        Assert.AreEqual(expected, actual)
        match resultState with
        | MouseMod.MouseState.Up -> ()
        | _ -> Assert.Fail()
        // Down->Up
        let mouseState = MouseMod.MouseState.Down(Point 0 0, Point 0 0)
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) (MouseMod.MouseEvent.Up)
        let actual = fst (snd (resultIo ([], ())))
        let expected = [ MouseMod.IOCommand.NoOp ]
        Assert.AreEqual(expected, actual)
        match resultState with
        | MouseMod.MouseState.Up -> ()
        | _ -> Assert.Fail()
        // Down->Move
        let mouseState = MouseMod.MouseState.Down(Point 0 0, Point 0 0)
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) 
                (MouseMod.MouseEvent.Move(Point 1 1))
        let actual = fst (snd (resultIo ([], ())))
        let expected = [ MouseMod.IOCommand.Move(Point -1 -1) ]
        Assert.AreEqual(expected, actual)
        match resultState with
        | MouseMod.MouseState.Down(firstPoint, lastPoint) when firstPoint = (Point 0 0) && lastPoint = (Point 1 1) -> ()
        | _ -> Assert.Fail()
        // Down->Move. 작게 움직여서 아무것도 하지 않은것으로 다루기.
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) 
                (MouseMod.MouseEvent.Move(Point 0 0))
        let actual = fst (snd (resultIo ([], ())))
        let expected = []
        Assert.AreEqual(expected, actual)
        match resultState with
        | MouseMod.MouseState.Down(firstPoint, lastPoint) when firstPoint = (Point 0 0) && lastPoint = (Point 0 0) -> ()
        | _ -> Assert.Fail()
        // Up->Up
        let mouseState = MouseMod.MouseState.Up
        let (resultState, resultIo), resultLogger = 
            StateMachineMod.MouseMod.scanF4Mouse ((mouseState, io), LogMod.emptyLogger) (MouseMod.MouseEvent.Up)
        let actual = fst (snd (resultIo ([], ())))
        let expected = [ MouseMod.IOCommand.NoOp ]
        Assert.AreEqual(expected, actual)
        let filterF (c : LogMod.Log) : bool = 
            match c with
            | LogMod.Log.Error(_) -> true
            | _ -> false
        
        let actual = fst (snd (resultLogger ([], ())))
        List.exists filterF (actual) |> Assert.IsTrue
        List.exists filterF (actual) |> Assert.IsTrue
        Assert.AreEqual(1, List.length actual)
        match resultState with
        | MouseMod.MouseState.Up -> ()
        | _ -> Assert.Fail()
    
    [<Test>]
    let testReceivedLines() : unit = 
        // 이 검사는 장황하다. 또한, 각 검사의 예상결과를 마치 그림을 그리듯이 검사 곳곳에 써놓아서,
        // 검사의 내용이 바뀌게 되는 경우 일일이 다시 바꿔줘야 한다. 아주 불편한데, 다른 방법은 없나.
        let detailState : DetailStateRec = 
            { backgroundSize = Point 2 7
              background = BlockManipulationMod.drawBackgroundBorder (Point 2 7) Map.empty
              blockSize = 1 // 이 크기는 회전할 때 필요하다. 회전하지 않을 것이므로, 실제 사용할 벽돌 크기와 다르게 설정한다.
              createBlock = 
                  (fun _ _ _ _ -> 
                  Map.ofList [ TestBasic.prepareBlockCell 0 0 1 None
                               TestBasic.prepareBlockCell 0 1 1 None ], ((), LogMod.emptyLogger))
              speed = 1
              nextSeed = 1
              counter = 1
              receivedLines = [] }
        
        let stopRec : StopRec = 
            { currentBlock = None
              detailState = detailState }
        
        let backgroundPixelMap : Ref<Map<int, PointRec * BlockCell>> = ref Map.empty
        let receivedLinesPixelMap : Ref<Map<int, PointRec * BlockCell>> = ref Map.empty
        let gameState = (GameState.Stop(stopRec), IOMod.nothingToDo), LogMod.emptyLogger
        
        let printPixelMap (detailState : DetailStateRec) (io : IOMod.IO) : unit = 
            let ioList = (List.rev (fst (snd (io ([], ())))))
            System.Console.WriteLine(sprintf "%A" ioList)
            backgroundPixelMap 
            := List.fold BlockManipulationMod.processIOCommand4Background (!backgroundPixelMap) ioList
            receivedLinesPixelMap 
            := List.fold BlockManipulationMod.processIOCommand4ReceivedLines (!receivedLinesPixelMap) ioList
            !backgroundPixelMap
            |> FSharpx.Collections.Map.values
            |> Map.ofSeq
            |> BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize
            |> BlockManipulationMod.printBackground detailState.backgroundSize
            !receivedLinesPixelMap
            |> FSharpx.Collections.Map.values
            |> Map.ofSeq
            |> BlockManipulationMod.drawBackgroundBorder detailState.backgroundSize
            |> BlockManipulationMod.printBackground detailState.backgroundSize
        
        let rotationPointMap : Map<PointRec, int * (PointRec * PointRec)> = 
            Map.empty |> Map.add (Point 0 0) (1, ((Point 0 0), (Point 0 0)))
        // 최초의 정지상태에서 게임을 시작한다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap gameState GameEvent.Start
        
        // 내려오는 벽돌이 없는 상태에서 줄을 보낸다. 줄을 쌓기만 하고 배경으로 옮겨지진 않는다.
        // 왜냐하면, scanF 에서 pushReceivedLines 는 orElse 로서 실행되기 때문이다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap ((resultGameState, IOMod.nothingToDo), LogMod.emptyLogger) 
                (GameEvent.ReceivedLines([ (TestBasic.prepareBlockCell 2 6 2 None)
                                           (TestBasic.prepareBlockCell 2 7 3 None) ]))
        
        let expectedPixelMap = 
            Map.empty
            |> Map.add 2 (TestBasic.prepareBlockCell 2 6 2 (Some 2))
            |> Map.add 1 (TestBasic.prepareBlockCell 2 7 3 (Some 1))
        
        match resultGameState with
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec) -> 
            printPixelMap playNoCurrentBlockRec.detailState resultIO
            Assert.AreEqual(expectedPixelMap, !receivedLinesPixelMap)
        | _ -> Assert.Fail()
        // 시간을 만료시킨다. 따라서 쌓아놓은 줄이 배경으로 옮겨지고, 새 벽돌이 만들어진다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap ((resultGameState, IOMod.nothingToDo), LogMod.emptyLogger) 
                GameEvent.TimeElapsed
        
        let expectedPixelMap = 
            Map.empty
            |> Map.add 3 (TestBasic.prepareBlockCell 2 6 2 (Some 3))
            |> Map.add 4 (TestBasic.prepareBlockCell 2 7 3 (Some 4))
            |> Map.add 6 (TestBasic.prepareBlockCell 1 2 1 (Some 6))
            |> Map.add 5 (TestBasic.prepareBlockCell 1 1 1 (Some 5))
        match resultGameState with
        | GameState.Play(playRec) -> 
            printPixelMap playRec.detailState resultIO
            Assert.AreEqual(Map.empty, !receivedLinesPixelMap)
            Assert.AreEqual(expectedPixelMap, !backgroundPixelMap)
        | _ -> Assert.Fail()
        // 벽돌이 내려오고 있는 중에 줄을 쌓으면, 그 줄은 배경으로 옮겨지지 않고 내려오는 벽돌이 바닥에 닿을 때까지 쌓아놓는다.
        // 쌓아놨던 줄이 위로 올라가는지에 관한 검사는 다른 검사에서 했으므로, 여기서는 생략한다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap ((resultGameState, IOMod.nothingToDo), LogMod.emptyLogger) 
                (GameEvent.ReceivedLines([ (TestBasic.prepareBlockCell 1 6 4 None)
                                           (TestBasic.prepareBlockCell 1 7 5 None) ]))
        
        let expectedPixelMap = 
            Map.empty
            |> Map.add 11 (TestBasic.prepareBlockCell 1 7 5 (Some 11))
            |> Map.add 12 (TestBasic.prepareBlockCell 1 6 4 (Some 12))
        
        match resultGameState with
        | GameState.Play(playRec) -> 
            printPixelMap playRec.detailState resultIO
            Assert.AreEqual(expectedPixelMap, !receivedLinesPixelMap)
        | _ -> Assert.Fail()
        // 내려오는 벽돌을 바닥으로 내린다. 아직 쌓아놓은 줄을 배경으로 옮기지 않는다. 
        // 또한, 깨진 줄이 잘 계산되었는지 확인한다. 이 줄들은 다른 사용자게임에 관련된 stream 에 ReceivedLines 로서 주어진다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap ((resultGameState, IOMod.nothingToDo), LogMod.emptyLogger) 
                (GameEvent.Movement(Movement.DropDown))
        
        let expectedPixelSet = 
            [ (TestBasic.prepareBlockCell 2 6 2 None)
              (TestBasic.prepareBlockCell 2 7 3 None) ]
            |> Set.ofList
        match resultGameState with
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec) -> 
            printPixelMap playNoCurrentBlockRec.detailState resultIO
            Assert.AreEqual(Map.empty, !backgroundPixelMap)
            Assert.AreEqual(expectedPixelMap, !receivedLinesPixelMap)
            let transferLines = 
                (List.collect (fun ioCommand -> 
                     match ioCommand with
                     | IOMod.IOCommand.TransferLines(lines) -> lines
                     | _ -> []) (fst (snd (resultIO ([], ())))))
                |> Set.ofList
            Assert.AreEqual(expectedPixelSet, transferLines)
        | _ -> Assert.Fail()
        // 쌓아놓은 줄을 배경으로 옮긴다. 이 작업은 새 벽돌을 화면에 만들기에 앞서서 같이 수행된다.
        let (resultGameState, resultIO), resultLogger = 
            StateMachineMod.GameMod.scanF rotationPointMap ((resultGameState, IOMod.nothingToDo), LogMod.emptyLogger) 
                (GameEvent.TimeElapsed)
        
        let expectedPixelMap = 
            Map.empty
            |> Map.add 13 (TestBasic.prepareBlockCell 1 6 4 (Some 13))
            |> Map.add 14 (TestBasic.prepareBlockCell 1 7 5 (Some 14))
            |> Map.add 16 (TestBasic.prepareBlockCell 1 2 1 (Some 16))
            |> Map.add 15 (TestBasic.prepareBlockCell 1 1 1 (Some 15))
        match resultGameState with
        | GameState.Play(playRec) -> 
            printPixelMap playRec.detailState resultIO
            Assert.AreEqual(expectedPixelMap, !backgroundPixelMap)
            Assert.AreEqual(Map.empty, !receivedLinesPixelMap)
        | _ -> Assert.Fail()
