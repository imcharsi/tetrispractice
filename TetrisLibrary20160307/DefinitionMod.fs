﻿namespace TetrisLibrary

module DefinitionMod = 
    open FunctionalUtil
    
    module LogMod = 
        // https://jessehu.wordpress.com/2009/11/17/log4j-levels-all-trace-debug-info-warn-error-fatal-off/
        type Log = 
            | All of string
            | Trace of string
            | Debug of string
            | Info of string
            | Warn of string
            | Error of string
            | Fatal of string
        
        type Logger = StateMod.State<unit, List<Log>, unit>
        
        let emptyLogger : Logger = StateMod.unit()
        
        let writeLog (log : Log) (logger : Logger) : Logger = 
            let f (logList : List<Log>, _ : unit) : unit * (List<Log> * unit) = (), (log :: logList, ())
            IOStateMod.bind f logger
    
    type PointRec = 
        { x : int
          y : int }
    
    let Point (x : int) (y : int) : PointRec = 
        { x = x
          y = y }
    
    type BlockCellRec = 
        { blockType : int
          drawingObjectId : Option<int> }
    
    // #region
    // 여기서의 벽돌 종류는, 화면에 출력되는 형태를 구분하지 않고, 기능을 구분하는 용도로 쓰기.
    // 예를 들면, 특정 벽돌은 아이템을 갖고 있어서 해당 벽돌을 깨뜨리면 아이템을 쓸 수 있게 된다거나 하는 식으로 하고 싶을 때 쓴다.
    // 이 벽돌이 예상낙하지점에 그려질 벽돌이기 때문에 반투명하게 그려야 한다는 판단은 여기서 하지 않기.
    // #endregion
    type BlockCell = 
        | NormalBlockCell of BlockCellRec
        | InvisibleBorder
    
    type BlockMap = Map<PointRec, BlockCell>
    
    type Block = PointRec * BlockMap
    
    // #region
    // 아래는 그냥 tuple 로 두는 것이 편하다. Map.toList Map.ofList 로 자주 쓴다.
    // #endregion
    type BlockList = List<PointRec * BlockCell>
    
    type RotationDirection = 
        | Clockwise
        | CounterClockwise
    
    type Movement = 
        | Left
        | Right
        | Down
        | DropDown
        | Rotation of RotationDirection
    
    // TODO 난수를 직접 정할 수 있도록 하면 검사할 때 편하다. random:int->int 넣기.
    type DetailStateRec = 
        { backgroundSize : PointRec
          background : BlockMap
          blockSize : int
          // 회전까지 한 벽돌을 만든다. 무조건 성공한다.
          createBlock : int -> int -> PointRec -> (unit * LogMod.Logger) -> (BlockMap * (unit * LogMod.Logger))
          speed : int
          nextSeed : int
          counter : int
          receivedLines : BlockList }
    
    // #region
    // 이와 같이 하면 매 event 처리의 결과로서,
    // timer 가 즉시 만료되도록 정할 수도 있고,
    // 시간간격에 따라 만료되도록 할 수도 있고,
    // timer 가 설정되지 않도록 정할 수도 있다.
    // #endregion
    type TimerCommand = 
        | SetAsImmediate
        | SetAsWait
    
    type PlayRec = 
        { currentBlock : Block
          expectedBlockCell : List<BlockCell>
          nextBlock : BlockMap
          detailState : DetailStateRec }
    
    type PlayNoCurrentBlockRec = 
        { nextBlock : BlockMap
          detailState : DetailStateRec }
    
    type StopRec = 
        { currentBlock : Option<Block>
          detailState : DetailStateRec }
    
    type GameState = 
        // #region ...
        // Either 의 Right 에서 사용된다. 따라서, 실패의 경우는 다루지 않는다.
        // drawingPlan 은 매 scanF 의 계산결과를 subscribe 단계로 알리기 위해 쓴다.
        // 따라서 매 scanF 실행마다 drawingPlan 입력값은 무시된다.
        // TODO 상태마다 구조가 달라서, 매 state monadic 함수마다 상태를 구분하는 조건문을 써야 한다. 그래서 중복이 많다. 일단 넘어가기.
        // #endregion
        | Play of PlayRec
        | PlayNoCurrentBlock of PlayNoCurrentBlockRec
        | Stop of StopRec
    
    // #region
    // TODO 화면갱신기능이 필요한가.
    // #endregion
    type GameEvent = 
        | Start
        | Stop
        | Movement of Movement
        | TimeElapsed
        | ReceivedLines of BlockList
        // 마우스로 누른 벽돌의 drawing object id 와, 해당 벽돌의 이동후 대상위치를 받는다.
        | DragMovement of PointRec * Option<LogMod.Logger>
        | NoOp of Option<LogMod.Logger> // Observable.map 을 쓴다면, 반드시 결론이 나와야 하므로, 아무것도 하지 않는 game event 가 있어야 한다.
    
    module IOMod = 
        // #region
        // 아래의 명령 중 삭제에 관한 명령에서는 좌표를 사용하지 않는다.
        // #endregion
        type IOCommand = 
            | AddBackground of PointRec * BlockCell
            | RemoveBackground of BlockCell
            | MoveBackground of PointRec * BlockCell
            | AddNextBlock of PointRec * BlockCell
            | RemoveNextBlock of BlockCell
            | AddReceivedLines of PointRec * BlockCell
            | RemoveReceivedLines of BlockCell
            | MoveReceivedLines of PointRec * BlockCell
            | AddExpectedBlock of PointRec * BlockCell
            | RemoveExpectedBlock of BlockCell
            | MoveExpectedBlock of PointRec * BlockCell
            | Timer of TimerCommand
            | TransferLines of BlockList
            | Notification
        
        // #region
        // 현재 내려오는 벽돌이 바닥에 닿았을 경우의 위치를 미리 보여주는 기능도 넣는다.
        // 이 type 이, 마치 State monad 의 state 처럼 사용된다.
        // 왜냐하면, 앞단계에서 했던 계산결과 drawingPlan 을 뒷단계에서 바꾸기도 하기 때문이다.
        // 이름이 햇갈릴 수 있는데, clearedLines 는, 내가 깨뜨려서 상대방에게 넘어갈 줄이다.
        // 이와 같이 하는 이유는, 줄 전달은 side-effect 로서 상대방의 stream 에 event 를 넣는 방식으로 해야 하기 때문이다.
        // 여태까지 해왔던 것이 IO monad 였다. 그래서 DrawingPlanRec 를 지우고, IO monad 로 바꿨다.
        // #endregion
        // environment 객체를, list 가 아닌 다른 것으로 하는 것이 취지에 맞는데, 일단은 이와 같이 하기.
        type IOCommandList = List<IOCommand>
        
        type IO = StateMod.State<unit, IOCommandList, unit>
        
        let writeIOCommand (ioCommand : IOCommand) (ioCommandList : IOCommandList, _ : unit) : unit * (IOCommandList * unit) = 
            (), (ioCommand :: ioCommandList, ())
        let nothingToDo : IO = StateMod.unit()
        let foldF4WriteIOCommand (io : IO) (ioCommand : IOCommand) : IO = IOStateMod.bind (writeIOCommand ioCommand) io
        let bindF4WriteIOCommand (ioCommand : IOCommand) (r : 'r) 
            ((gameState : GameState, io : IO), logger : LogMod.Logger) : Option<'r> * ((GameState * IO) * LogMod.Logger) = 
            Some r, ((gameState, IOStateMod.bind (writeIOCommand ioCommand) io), logger)
    
    module MouseMod = 
        type MouseEvent = 
            | Down of PointRec * Option<LogMod.Logger>
            | Up
            | Move of PointRec
            | NoOp of Option<LogMod.Logger>
        
        type MouseState = 
            | Down of PointRec * PointRec // 마우스를 누른 시작위치, 가장 최근의 이동위치
            | Up
        
        type IOCommand = 
            | Move of PointRec
            | NoOp
        
        type IO = StateMod.State<unit, List<IOCommand>, unit>
        
        let writeIOCommand (ioCommand : IOCommand) (io : IO) (commandList : List<IOCommand>, _ : unit) : unit * (List<IOCommand> * unit) = 
            (), (ioCommand :: commandList, ())
