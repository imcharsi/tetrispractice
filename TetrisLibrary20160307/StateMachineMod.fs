﻿namespace TetrisLibrary

module StateMachineMod = 
    // 또 다시 크게 바꿨다. 이번에는 StateEither 를 써보기로 했다.
    open DefinitionMod
    open FunctionalUtil
    
    let keepStateAndUnitSomeness (stateOption : StateOptionMod.StateOption<unit, GameState * IOMod.IO, LogMod.Logger>) : StateOptionMod.StateOption<unit, GameState * IOMod.IO, LogMod.Logger> = 
        StateOptionMod.keepStateAndSomeness (fun _ -> ()) stateOption
    
    module GameMod = 
        // 애초의 구상과는 다르게, scan 의 상태는 Option 이거나 Either 일 필요도 없다.
        // 계산 단계에서 성공실패의 뜻으로 Option<unit> 을 많이 쓴다. bool 을 쓰는 것도 가능한데, 일단 Option 으로 쓰기.
        // log 의 양이 많아져도 상관없다. subscriber 에서 logger 만 따로 떼어내서, 다시 log 를 그리는 stream 에 넣으면 된다.
        // 이 stream 의 subscribeOn 에서 다중 thread 를 쓰면 된다.
        // 임의로 만든 Observable.scan 에서 상태 중 일부를 매번 갱신해준다. 따라서 받은 io 와 logger 에 대해 작업을 하면 된다.
        // 또한 이와 같이 하는 것이 일종의 의존성 주입과 같은 활용을 하는 방법이다.
        // TODO 설명을 다시 쓰기!!!!!!!!!!
        let scanF (rotationMap : Map<PointRec, int * (PointRec * PointRec)>) 
            (gameState : (GameState * IOMod.IO) * LogMod.Logger) (gameEvent : GameEvent) : (GameState * IOMod.IO) * LogMod.Logger = 
            match gameEvent with
            | GameEvent.NoOp(None) -> gameState // 이와 같이 하여, 대다수의 NoOp 은 처리하지 않고 logger 만 남긴다.
            | gameEvent -> 
                match gameState with
                | (GameState.Play(playRec), io), logger -> 
                    // #region
                    // 아래 단계들에서도, keepState 를 쓰는 것이 유용한데,
                    // 왜냐하면 expectedBlock 을 제외한 각각의 단계들이 기준으로 할 상태는 최초의 상태이기 때문이다.
                    // 각 함수를 최초의 상태를 기준으로 실행해보고 안되면 다음 함수를 또 다시 최초의 상태를 기준으로 실행해본다.
                    // 왼쪽오른쪽과 회전만 다룬다. 이외의 이동을 시도할 경우 실패한다.
                    // #endregion
                    StateOptionMod.keepState (MovementMod.tryLeftRightRotation rotationMap gameEvent)
                    // #region
                    // 앞의 시도가 실패했을 때, 한줄내리기, 바닥내리기, 시간만료를 다룬다.
                    // #endregion
                    |> StateOptionMod.orElse 
                           (fun () -> 
                           MovementMod.tryDown gameEvent 
                           |> StateOptionMod.bind (MovementMod.bindF4TryDown >> StateOptionMod.keepState))
                    // #region
                    // 앞의 시도 중 아무거나 성공했을 때 예상낙하지점 계산을 한다.
                    // 다음 줄에서는 상태를 유지하는 것이 필요한데, 왜냐하면 게임상태가 Play 가 아니면 expectedBlock 은 Left 가 되기 때문이다.
                    // 따라서, Some(PlayNoCurrentBlock) 이 주어지면, None 가 되었다가 다시 상태가 복구되어 Some(PlayNoCurrentBlock) 가 된다.
                    // 한편, PlayNoCurrentBlock 으로서 상태가 주어질 수도 있는데, 벽돌이 깨질 수 있기 때문이다.
                    // #endregion
                    |> StateOptionMod.bind4UnitState 
                           (MovementMod.expectedBlock IOMod.IOCommand.MoveExpectedBlock |> keepStateAndUnitSomeness)
                    // #region
                    // 앞의 시도가 모두 실패했을 때만, 쌓은 줄 넘겨받기를 시도한다.
                    // #endregion
                    |> StateOptionMod.orElse (fun () -> ReceivedLinesMod.tryReceivedLines gameEvent)
                    |> StateOptionMod.orElse (fun () -> StopMod.stop gameEvent)
                    // 여태까지의 계산결과가 반영된 GameState 상태가 State<'a,'s> 의 'a 로 되어야 한다.
                    |> StateMod.bind StateOptionMod.getStateAsA
                    |> StateMod.run ((GameState.Play(playRec), io), logger)
                    // State<'a,'s> 의 'a 가 구하고자 하는 결론이다. 여기서의 'a 는 getState 에 의해 'a='s 이다.
                    |> fst
                | (GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger -> 
                    // 우선 줄넘겨받기를 시도한다. 결과는 Some(PlayNoCurrentBlock) 이거나 None 이다. 상태는 유지한다.
                    StateOptionMod.keepState (ReceivedLinesMod.tryReceivedLines gameEvent)
                    // 이전 단계가 None 일때만 실행된다.
                    |> StateOptionMod.orElse 
                           (fun () -> // #region
                           // 쌓은줄 채우기를 시도한다. 결과는
                           // Some(PlayNoCurrentBlock) 쌓은줄을 채워넣었고 넘침이 없었거나
                           // Some(Stop) 쌓은줄을 채워넣었고 넘침이 있어서 게임이 끝났거나
                           // None GameEvent 가 TimeElapsed 가 아니거나.
                           // pushReceivedLines 가 Some 이면 이 후 단계도 모두 Some 가 된다. 여기서 None 일 때만 이 후 단계도 None 가 된다.
                           // #endregion
                           TimeElapsedMod.pushReceivedLines gameEvent
                           // #region
                           // 애초에 입력이 None 라면 bind 의 인자인 tryPutNewBlock 은 실행되지 않고, 결론은 bind 의 실행결과로서 None 가 된다.
                           // 새 벽돌 만들기를 시도한다. 결과는
                           // Some(Play) 배경에 새 벽돌을 만들었거나
                           // Some(Stop) 배경에 새 벽돌을 만들 자리가 없어서 게임이 끝났거나 아니면 이미 게임이 끝났기 때문에 할 것이 없거나.
                           // 아래 본문구절의 의미가 햇갈릴 수 있는데, 우선 bind 의 정의에 따라 인자함수는 f:a->F[b] 이고,
                           // bind 의 인자는 TimeElapsedMod.tryPutNewBlock >> StateEitherMod.keepStateAndRightness 이고,
                           // a->State, State->State 이므로 합성하면 a->State 가 되어 bind 함수의 정의에 맞게 되고,
                           // 이 합성함수의 반환값은, tryPutNewBlock 의 반환 State 가 아니라 keepStateAndRightness 의 반환 State 가 된다.
                           // 따라서 bind 의 내부구현방식에 의해 이 반환 State 에 대해 상태를 주면, keepStateAndRightness 의 의미에 따라 상태가 다루어지게 된다.
                           // 한편, 아래와 같이 구성하면 어떤가.
                           // |> StateEitherMod.bind (TimeElapsedMod.tryPutNewBlock) 
                           // |> StateEitherMod.keepStateAndRightness
                           // 이와 같이 하면, 두 단계를 거친 결과 항상 Right 가 되어, 본문에서 하고자 했던 바와는 결론이 달라진다.
                           // 본문에서 하고자 하는 바는, 입력이 None 이면 결론도 None 입력이 Some 이면 내부계산결과가 None 이도 결론은 Some 이다.
                           // #endregion
                           |> StateOptionMod.bind4UnitState (TimeElapsedMod.tryPutNewBlock |> keepStateAndUnitSomeness)
                           // #region
                           // 애초에 입력이 None 라면 bind 의 인자인 expectedBlock 은 실행되지 않고, 결론은 bind 의 실행결과로서 None 가 된다.
                           // 예상낙하지점계산을 시도한다. 결과는
                           // Some(Play) 예상낙하지점을 계산했거나
                           // Some(Stop) 게임이 끝났기 때문에 할 것이 없거나.
                           // #endregion
                           |> StateOptionMod.bind4UnitState 
                                  (MovementMod.expectedBlock IOMod.IOCommand.AddExpectedBlock 
                                   |> keepStateAndUnitSomeness)
                           // #region
                           // 애초에 입력이 None 라면 bind 의 인자인 generateNextBlock 은 실행되지 않고, 결론은 bind 의 실행결과로서 None 가 된다.
                           // 다음 벽돌계산을 시도한다. 결과는
                           // Some(Play) 다음 벽돌을 계산했거나
                           // Some(Stop) 이미 게임이 끝났기 때문에 할 것이 없거나.
                           // 아래 문장을 지나서도 계속 None 이라면, 애초에 GameEvent 가 TimeElapsed 가 아니었다는 뜻이다.
                           // #endregion
                           |> StateOptionMod.bind4UnitState 
                                  (TimeElapsedMod.generateNextBlock |> keepStateAndUnitSomeness))
                    |> StateOptionMod.orElse (fun () -> StopMod.stop gameEvent)
                    |> StateMod.bind StateOptionMod.getStateAsA
                    // #region
                    // 이 단계에 이르러, 결론은 Play 이거나 Stop 이거나 둘 중 하나이다.
                    // 위 문장에 이르러, None 이 주어질 수도 있는데, 이 경우 결론은 아무것도 하지 않은 것이 되고 이 말은,
                    // 이 상황에서 주어질 거라 기대했던 GameEvent 가 아닌 다른 GameEvent 가 주어졌다는 말이다.
                    // 구체적으로, ReceivedLines 이거나 TimeElapsed 둘 중 하나이어야 한다.
                    // #endregion
                    |> StateMod.run ((GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), io), logger)
                    |> fst
                | (GameState.Stop(stopRec), io), logger -> 
                    StopMod.start gameEvent
                    |> StateMod.bind StateOptionMod.getStateAsA
                    |> StateMod.run ((GameState.Stop(stopRec), io), logger)
                    |> fst
        
        let preF ((gameState : GameState, _ : IOMod.IO), (_ : LogMod.Logger)) (gameEvent : GameEvent) : ((GameState * IOMod.IO) * LogMod.Logger) * GameEvent = 
            match gameEvent with
            | GameEvent.DragMovement(point, Some(logger)) -> 
                ((gameState, DefinitionMod.IOMod.nothingToDo), logger), GameEvent.DragMovement(point, None)
            | GameEvent.NoOp(Some(logger)) -> 
                ((gameState, DefinitionMod.IOMod.nothingToDo), logger), GameEvent.NoOp(None)
            | _ -> ((gameState, DefinitionMod.IOMod.nothingToDo), DefinitionMod.LogMod.emptyLogger), gameEvent
    
    module MouseMod = 
        let scanF4Mouse ((mouseState : MouseMod.MouseState, io : MouseMod.IO), (logger : LogMod.Logger)) 
            (mouseEvent : MouseMod.MouseEvent) : (MouseMod.MouseState * MouseMod.IO) * LogMod.Logger = 
            match mouseState, mouseEvent with
            // 마우스를 누른채로 움직이기.
            | MouseMod.MouseState.Down(firstPoint, lastPoint), MouseMod.MouseEvent.Move(movePoint) -> 
                if lastPoint <> movePoint then 
                    let log = LogMod.Log.Info "scanF4Mouse: mouse move."
                    let diff = (BlockManipulationMod.adder lastPoint (BlockManipulationMod.invert movePoint))
                    let command = MouseMod.IOCommand.Move(diff)
                    (// 아주 햇갈리게 되어 있다. 관련 설명은 side effect 의 currentPoint 설명에 있다.
                     // 여기서, 계산결과는 가장 최근 이동위치와 지금의 이동위치간의 차이이다.
                     // 차이가 있으면 이동이 있었다고 인식한다.
                     // 최근 위치가 현재 이동위치보다 작다면, 결과는 음수가 된다.
                     // 차이를 뜻하는 음수의 결과가 GameEvent.DragMovement 로서 주어지면
                     // 좌우 이동의 경우 오른쪽으로 이동하고
                     // 상하이동의 경우 아래로 이동한다.
                     MouseMod.MouseState.Down(firstPoint, movePoint), MouseMod.writeIOCommand command io), 
                    (LogMod.writeLog log logger)
                else ( // 움직임의 차이가 크지 않아서 게임에 영향을 줄 정도가 아니면 log 를 남기지 않는다.
                       mouseState, io), (logger)
            // 마우스를 떼기.
            | MouseMod.MouseState.Down(_), MouseMod.MouseEvent.Up -> 
                let log = LogMod.Log.Info "scanF4Mouse: mouse up."
                let command = MouseMod.IOCommand.NoOp
                (MouseMod.MouseState.Up, MouseMod.writeIOCommand command io), (LogMod.writeLog log logger)
            // 마우스를 누르기.
            | MouseMod.MouseState.Up, MouseMod.MouseEvent.Down(point, None) -> 
                let log = LogMod.Log.Info "scanF4Mouse: mouse down."
                let command = MouseMod.IOCommand.NoOp
                (MouseMod.MouseState.Down(point, point), MouseMod.writeIOCommand command io), 
                (LogMod.writeLog log logger)
            // 마우스에서 손을 뗀 채로 움직이는 것은 log 를 남기지 않는다.
            | MouseMod.MouseState.Up, MouseMod.MouseEvent.Move(_) -> (MouseMod.MouseState.Up, io), (logger)
            | _ -> 
                let log = LogMod.Log.Error(sprintf "scanF4Mouse: unexpected state.")
                let command = MouseMod.IOCommand.NoOp
                (mouseState, MouseMod.writeIOCommand command io), (LogMod.writeLog log logger)
        
        let mapF ((_ : MouseMod.MouseState, io : MouseMod.IO), (logger : LogMod.Logger)) : List<GameEvent> = 
            fst (snd (io ([], ())))
            // 왜 뒤집나. 가장 최근에 쓰여진 io command 가 가장 앞에 온다. 그래서 뒤집어야 한다.
            // 그런데, 사실상 안뒤집어도 되는데 io command 는 한개만 있기 때문이다.
            // 아래와 같은 구절은 문제가 있다. 모든 명령들에 대해 logger 가 똑같다.
            // io 를 실행한 결과 3개의 결과가 나왔다면 각 결과 모두가 같은 logger 를 공유하게 된다.
            // 따라서, io 의 실행결과는 무조건 한개가 되도록 해야 아래의 구절이 말이 된다.
            |> List.rev
            |> List.map (fun command -> 
                   match command with
                   | MouseMod.IOCommand.Move(a) -> GameEvent.DragMovement(a, Some(logger))
                   | MouseMod.IOCommand.NoOp -> GameEvent.NoOp(Some(logger)))
        
        let preF ((mouseState : MouseMod.MouseState, _ : MouseMod.IO), _ : LogMod.Logger) 
            (mouseEvent : MouseMod.MouseEvent) : ((MouseMod.MouseState * MouseMod.IO) * LogMod.Logger) * MouseMod.MouseEvent = 
            match mouseEvent with
            | MouseMod.MouseEvent.Down(point, Some(logger)) -> 
                ((mouseState, FunctionalUtil.StateMod.unit()), logger), MouseMod.MouseEvent.Down(point, None)
            | MouseMod.MouseEvent.NoOp(Some(logger)) -> 
                ((mouseState, FunctionalUtil.StateMod.unit()), (logger)), MouseMod.MouseEvent.NoOp(None)
            | _ -> ((mouseState, FunctionalUtil.StateMod.unit()), (LogMod.emptyLogger)), mouseEvent
