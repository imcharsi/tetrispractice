﻿namespace TetrisLibrary

module ReceivedLinesMod = 
    open FunctionalUtil
    open DefinitionMod
    
    let mergeReceivedLinesAndNewReceivedLines (detailState : DetailStateRec) (io : IOMod.IO) 
        (newReceivedLines : BlockList) : DetailStateRec * IOMod.IO = 
        let remainedReceivedLines, removedDrawingCommandList = 
            TimeElapsedMod.splitBackgroundAndOverflow (Map.ofList detailState.receivedLines) detailState.backgroundSize 
                newReceivedLines IOMod.IOCommand.RemoveReceivedLines |> TupleMod.map (Map.toList, id)
        let newReceivedLines, detailState = BlockManipulationMod.reassignDrawingObjectId newReceivedLines detailState
        
        let io = 
            io
            // 바닥부터 쌓아올린 결과, 꼭대기부터 시작해서 넘치게 된 벽돌들은 지운다.
            |> (fun io -> List.fold IOMod.foldF4WriteIOCommand io removedDrawingCommandList)
            // 바닥부터 쌓아올린 결과, 원래 있었던 벽돌들 중 범위를 넘지 않은 벽돌들은 위치가 바뀌었다.
            |> (fun io -> 
            List.fold IOMod.foldF4WriteIOCommand io (List.map IOMod.IOCommand.MoveReceivedLines remainedReceivedLines))
            // 바닥부터 새로 쌓아올렸던 줄들은 화면에 새로 보이기 시작해야 한다.
            |> (fun io -> 
            List.fold IOMod.foldF4WriteIOCommand io (List.map IOMod.IOCommand.AddReceivedLines newReceivedLines))
        { detailState with receivedLines = FunctionalUtil.ListMod.append newReceivedLines remainedReceivedLines }, io
    
    // #region
    // 상대방으로부터 넘겨받은 줄을, 이미 쌓아놨던 줄과 합친다.
    // #endregion
    let tryReceivedLines (gameEvent : GameEvent) ((gameState : GameState, io : IOMod.IO), logger : LogMod.Logger) : Option<unit> * ((GameState * IOMod.IO) * LogMod.Logger) = 
        match gameState, gameEvent with
        // 한줄만 받았을 때 아무것도 하지 않아야 하는데, 여기서 하지 않고, side effect 에서 하기로 한다.
        | GameState.Play(playRec), GameEvent.ReceivedLines(newReceivedLines) -> 
            let detailState, io = mergeReceivedLinesAndNewReceivedLines playRec.detailState io newReceivedLines
            let gameState = GameState.Play({ playRec with detailState = detailState })
            let log = LogMod.Log.Info "tryReceivedLines ok."
            Some(), ((gameState, io), LogMod.writeLog log logger)
        | GameState.PlayNoCurrentBlock(playNoCurrentBlockRec), GameEvent.ReceivedLines(newReceivedLines) -> 
            let detailState, io = 
                mergeReceivedLinesAndNewReceivedLines playNoCurrentBlockRec.detailState io newReceivedLines
            let gameState = GameState.PlayNoCurrentBlock({ playNoCurrentBlockRec with detailState = detailState })
            let log = LogMod.Log.Info "tryReceivedLines ok."
            Some(), ((gameState, io), LogMod.writeLog log logger)
        | _ -> 
            let log = LogMod.Log.Error "tryReceivedLines failed: unexpected game state/event."
            None, ((gameState, io), LogMod.writeLog log logger)
