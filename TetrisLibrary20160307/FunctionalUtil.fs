﻿namespace TetrisLibrary

module FunctionalUtil = 
    let swapParam (f : 'a -> 'b -> 'c) : 'b -> 'a -> 'c = 
        let f (b : 'b) (a : 'a) : 'c = f a b
        f
    
    module ChoiceMod = 
        // FSharpx.Collections 의 의존성 문제때문에 임시로 만들었는데, 해결방법을 알게 되었다.
        // 지워도 되는데, 혹시 다른 이유로 또 필요할 수 있으니 일단 놔둔다.
        let bind (f : 'a -> Choice<'b, 'c>) (aChoice : Choice<'a, 'c>) : Choice<'b, 'c> = 
            match aChoice with
            | Choice1Of2 a -> f a
            | Choice2Of2 c -> Choice2Of2 c
        
        let unit (a : 'a) : Choice<'a, 'c> = Choice1Of2 a
        let map (f : 'a -> 'b) : Choice<'a, 'c> -> Choice<'b, 'c> = bind (f >> unit)
        let apply (fChoice : Choice<'a -> 'b, 'c>) (aChoice : Choice<'a, 'c>) : Choice<'b, 'c> = 
            bind (fun f -> map f aChoice) fChoice
        
        let is1Of2 (choice : Choice<'a, 'b>) : bool = 
            match choice with
            | Choice1Of2 _ -> true
            | Choice2Of2 _ -> false
        
        let orElse (choice : Choice<'a, 'b>) (f : unit -> Choice<'a, 'b>) : Choice<'a, 'b> = 
            if is1Of2 choice then choice
            else f()
    
    module EitherMod = 
        // Choice 라는 이름은 햇갈린다. FSharpx 에서는 Choice1Of2 가 Right 의 의미로 사용되고, Choice2Of2 가 Left 의 의미로 사용된다.
        type Either<'l, 'r> = 
            | Left of 'l
            | Right of 'r
        
        let unit (r : 'r) : Either<'l, 'r> = Either.Right r
        let left (l : 'l) : Either<'l, 'r> = Either.Left l
        let right = unit
        
        let bind (f : 'r1 -> Either<'l, 'r2>) (r1Either : Either<'l, 'r1>) : Either<'l, 'r2> = 
            match r1Either with
            | Either.Right r1 -> f r1
            | Either.Left l -> Either.Left l
        
        let map (f : 'r1 -> 'r2) : Either<'l, 'r1> -> Either<'l, 'r2> = bind (f >> unit)
        let apply (r1Either : Either<'l, 'r1>) : Either<'l, 'r1 -> 'r2> -> Either<'l, 'r2> = 
            bind (fun f -> map f r1Either)
        let applyM (r1Either : Either<'l, 'r1>) : Either<'l, 'r1 -> Either<'l, 'r2>> -> Either<'l, 'r2> = 
            bind (fun f -> bind f r1Either)
        
        let bindLeft (f : 'l1 -> Either<'l2, 'r>) (l1Either : Either<'l1, 'r>) : Either<'l2, 'r> = 
            match l1Either with
            | Either.Left l1 -> f l1
            | Either.Right r -> Either.Right r
        
        let mapLeft (f : 'l1 -> 'l2) : Either<'l1, 'r> -> Either<'l2, 'r> = bindLeft (f >> Left)
        let applyLeft (l1Either : Either<'l1, 'r>) : Either<'l1 -> 'l2, 'r> -> Either<'l2, 'r> = 
            bindLeft (fun f -> mapLeft f l1Either)
        
        let isLeft (either : Either<'l, 'r>) : bool = 
            match either with
            | Either.Right _ -> false
            | Either.Left _ -> true
        
        let isRight (either : Either<'l, 'r>) : bool = isLeft either |> not
        
        let orElse (either2 : 'l -> Either<'l, 'r>) (either1 : Either<'l, 'r>) : Either<'l, 'r> = 
            match either1 with
            | Right r -> Right r
            | Left l -> either2 l
        
        let ofOption (l : unit -> 'l) (o : Option<'r>) : Either<'l, 'r> = 
            match o with
            | Some r -> Right r
            | None -> Left(l())
        
        let lift (f : 'r1 -> 'r2 -> 'r3) : Either<'l, 'r1> -> Either<'l, 'r2> -> Either<'l, 'r3> = 
            let innerF (r1Either : Either<'l, 'r1>) (r2Either : Either<'l, 'r2>) : Either<'l, 'r3> = 
                unit f
                |> apply r1Either
                |> apply r2Either
            innerF
        
        let liftM (f : 'r1 -> 'r2 -> Either<'l, 'r3>) : Either<'l, 'r1> -> Either<'l, 'r2> -> Either<'l, 'r3> = 
            let innerF (r1Either : Either<'l, 'r1>) (r2Either : Either<'l, 'r2>) : Either<'l, 'r3> = 
                unit f
                |> apply r1Either
                |> applyM r2Either
            innerF
    
    module ListMod = 
        let apply (aList : List<'a>) : List<'a -> 'b> -> List<'b> = List.collect (fun f -> List.map f aList)
        
        let zip (aList : List<'a>) (bList : List<'b>) : List<'a * 'b> = 
            let rec foldF (aList : List<'a>) (bList : List<'b>) (result : List<'a * 'b>) : List<'a * 'b> = 
                match aList, bList with
                | aHead :: aTail, bHead :: bTail -> foldF aTail bTail ((aHead, bHead) :: result)
                | _, _ -> List.rev result
            foldF aList bList []
        
        // #region
        // list1 의 앞부분부터 시작해서 끝부분까지 순서대로 list2 의 앞에 붙인다.
        // 따라서 [1 2 3] [4 5 6] 이면 [3 2 1 4 5 6] 이 된다.
        // #endregion
        let append (appendingList : List<'a>) (appendedList : List<'a>) : List<'a> = 
            let rec innerF (result : List<'a>) (tail : List<'a>) : List<'a> = 
                match tail with
                | head :: tail -> innerF (head :: result) tail
                | [] -> result
            innerF appendedList appendingList
    
    module MapMod = 
        let mapKey (f : 'a -> 'b) (map : Map<'a, 'c>) : Map<'b, 'c> = 
            Map.fold (fun result a c -> Map.add (f a) c result) Map.empty map
        
        let lookup (result : Map<'a, 'c>) (abMap : Map<'a, 'b>) (bcMap : Map<'b, 'c>) : Map<'a, 'c> = 
            let foldF (result : Map<'a, 'c>) (key : 'a) (value : 'b) : Map<'a, 'c> = 
                match Map.tryFind value bcMap with
                | Some c -> Map.add key c result
                | None -> result
            Map.fold foldF result abMap
        
        let lookupKey (referenceMap : Map<'a, 'a>) (sourceMap : Map<'a, 'b>) : Map<'a, 'b> = 
            let foldF (result : Map<'a, 'b>) (key : 'a) (anotherKey : 'a) : Map<'a, 'b> = 
                let result = 
                    result
                    |> Map.remove key
                    |> Map.remove anotherKey
                // key 에 대응하는 값이 sourceMap 에 없으면, anotherKey 에 대응하는 값이 이미 있었다 해도 지우도록 한다.
                // 반드시 이렇게 해야 하는 논리적인 이유는 없는데, 게임을 만드는 데 있어서 이와 같은 계산이 필요하므로 일단 이렇게 한다.
                // 예를 들어, referenceMap{1->2} 이고 sourceMap{2->'a'} 이면 결과는 {} 가 되어야 한다.
                // 왜냐하면, 1 에 대응하는 값을 2 에 대응하도록 하고 싶은데, 1 에 대응하는 값은 없고 2 에 대응하는 값만 있기 때문이다.
                match Map.tryFind key sourceMap with
                | Some b -> result |> Map.add anotherKey b
                | None -> result
            Map.fold foldF sourceMap referenceMap
    
    (*
    module StateMod = 
        let bind (f : 'a -> FSharpx.State.State<'b, 's>) (state : FSharpx.State.State<'a, 's>) : FSharpx.State.State<'b, 's> = 
            let innerF (s : 's) : 'b * 's = 
                let a, s = state s
                (f a) s
            innerF
        
        let unit (a : 'a) : FSharpx.State.State<'a, 's> = FSharpx.State.state { return a }
        let run (s : 's) (state : FSharpx.State.State<'a, 's>) : 'a * 's = state s
        *)
    module StateMod = 
        // rs = recoverable state, us = unrecoveable state
        // 이와 같이 형을 쓰고, keepState 를 같이 섞으면 logger 와 같이, 복구되어서는 안되는 성격의 상태를 쓸 수 있다.
        // 일반적인 state 는 us = unit 으로 하면 된다.
        // 그런데, 상태의 일부는 복구하고 일부는 놔두는 유형은 이와 같은 방식으로는 다룰 수 없다.
        // 복구할수 있는 상태와 복구할수 없는 상태로 분리될 수 있을 때를 전제한다.
        type State<'a, 'rs, 'us> = 'rs * 'us -> ('a * ('rs * 'us))
        
        type Result<'a, 'rs, 'us> = 'a * ('rs * 'us)
        
        let unit (a : 'a) : State<'a, 'rs, 'us> = (fun rsus -> a, rsus)
        
        let bind (f : 'a1 -> State<'a2, 'rs, 'us>) (a1State : State<'a1, 'rs, 'us>) (rsus : 'rs * 'us) : 'a2 * ('rs * 'us) = 
            let a1, rsus = a1State rsus
            f a1 rsus
        
        let map (f : 'a1 -> 'a2) : State<'a1, 'rs, 'us> -> State<'a2, 'rs, 'us> = bind (f >> unit)
        let apply (a1State : State<'a1, 'rs, 'us>) : State<'a1 -> 'a2, 'rs, 'us> -> State<'a2, 'rs, 'us> = 
            bind (fun f -> map f a1State)
        
        // StateEither 의 keepState 와 keepStateAndRightness 를 좀 더 일반화했다.
        // predF = true 이면, 결과가 성공이라는 뜻이다.
        let keepState (predF : 'a -> bool) (aF : 'a -> 'rs -> 'rs -> 'a) (state : State<'a, 'rs, 'us>) 
            (rs : 'rs, us : 'us) : 'a * ('rs * 'us) = 
            match state (rs, us) with
            | a, (rs, us) when predF a -> a, (rs, us)
            | a, (dirtyRS, us) -> aF a dirtyRS rs, (rs, us)
        
        let orElse (predF : 'a -> bool) (state2 : 'a -> State<'a, 'rs, 'us>) (state1 : State<'a, 'rs, 'us>) 
            (rs : 'rs, us : 'us) : 'a * ('rs * 'us) = 
            match state1 (rs, us) with
            | a, rsus when predF a -> a, rsus
            | a, rsus -> state2 a rsus
        
        let getState (state : State<'a, 'rs, 'us>) : State<'rs * 'us, 'rs, 'us> = bind (fun _ rsus -> rsus, rsus) state
        let run (rsus : 'rs * 'us) (state : State<'a, 'rs, 'us>) : 'a * ('rs * 'us) = state rsus
    
    module IOStateMod = 
        let bind (f : StateMod.State<unit, 'env, unit>) (io : StateMod.State<unit, 'env, unit>) : StateMod.State<unit, 'env, unit> = 
            StateMod.bind (fun () -> f) io
    
    module TupleMod = 
        let unit (a : 'a, b : 'b) : 'a * 'b = a, b
        // #region
        // tuple 은 bind 를 정의할 수 없는 듯 하다.
        // bind(fa : 'a1->'c1*c2)(fb : 'b1->'d1*d2)(a1 : 'a1, b1 : 'b1):('c1*c2)*('d1*'d2) = ...
        // 와 같이 써야 하는데, higher kinded type 이라는 개념 정의를 놓고 봤을 때, 입력형과 출력형이 다르다.
        // 출력형은 예를 들어 'e*'f 와 같이 되어야 한다. 'e 와 'f 가 또 다른 tuple 이라는 것이 미리 결정되어서는 안 된다.
        // 따라서, bind 보다는 다음과 같이 fold 와 map 을 같이 쓰는 것이 bind 의 취지에 좀 더 맞을 듯 하다.
        // map (fa,fb) (a,b) |> fold ((fold ffa),(fold ffb))
        // #endregion
        let map (fa : 'a1 -> 'r2, fb : 'b1 -> 'b2) (a1 : 'a1, b1 : 'b1) : 'r2 * 'b2 = fa a1, fb b1
        let fold (fa1a2 : 'a1 * 'r2 -> 'b) (a1 : 'a1, a2 : 'r2) : 'b = fa1a2 (a1, a2)
    
    module StateEitherMod = 
        type StateEither<'l, 'r, 'rs, 'us> = StateMod.State<EitherMod.Either<'l, 'r>, 'rs, 'us>
        
        type Result<'l, 'r, 'rs, 'us> = EitherMod.Either<'l, 'r> * ('rs * 'us)
        
        let unit (r : 'r) : StateEither<'l, 'r, 'rs, 'us> = StateMod.unit (EitherMod.Right r)
        let left (l : 'l) : StateEither<'l, 'r, 'rs, 'us> = StateMod.unit (EitherMod.Left l)
        let right (r : 'r) : StateEither<'l, 'r, 'rs, 'us> = unit r
        
        let bind (f : 'r1 -> StateEither<'l, 'r2, 'rs, 'us>) (r1StateEither : StateEither<'l, 'r1, 'rs, 'us>) : StateEither<'l, 'r2, 'rs, 'us> = 
            let stateF (rsus : 'rs * 'us) : EitherMod.Either<'l, 'r2> * ('rs * 'us) = 
                match r1StateEither rsus with
                | EitherMod.Right r1, rsus -> f r1 rsus
                | EitherMod.Left l, rsus -> left l rsus
            stateF
        
        // 편의 기능이다. 예를 들어, 앞의 State 가 StateEither<'l, unit, 's> 라면
        // 뒤의 state 함수는 (_:unit)(state:'s):Either*'s 와 같은 형식을 갖춰야 하는데,
        // 첫째 인자 unit 은 사실상 아무것도 하는것이 없다. 따라서, 이러한 경우에 맞도록 조합기를 쓰는 것이 편하다.
        let bind4UnitState (f : StateEither<'l, 'r2, 'rs, 'us>) (r1StateEither : StateEither<'l, unit, 'rs, 'us>) : StateEither<'l, 'r2, 'rs, 'us> = 
            bind (fun _ -> f) r1StateEither
        let map (f : 'r1 -> 'r2) : StateEither<'l, 'r1, 'rs, 'us> -> StateEither<'l, 'r2, 'rs, 'us> = bind (f >> unit)
        let apply (r1StateEither : StateEither<'l, 'r1, 'rs, 'us>) : StateEither<'l, 'r1 -> 'r2, 'rs, 'us> -> StateEither<'l, 'r2, 'rs, 'us> = 
            bind (fun f -> map f r1StateEither)
        let applyM (r1StateEither : StateEither<'l, 'r1, 'rs, 'us>) : StateEither<'l, 'r1 -> StateEither<'l, 'r2, 'rs, 'us>, 'rs, 'us> -> StateEither<'l, 'r2, 'rs, 'us> = 
            bind (fun f -> bind f r1StateEither)
        
        let lift (f : 'r1 -> 'r2 -> 'r3) : StateEither<'l, 'r1, 'rs, 'us> -> StateEither<'l, 'r2, 'rs, 'us> -> StateEither<'l, 'r3, 'rs, 'us> = 
            let innerF (r1StateEither : StateEither<'l, 'r1, 'rs, 'us>) (r2StateEither : StateEither<'l, 'r2, 'rs, 'us>) : StateEither<'l, 'r3, 'rs, 'us> = 
                unit f
                |> apply r1StateEither
                |> apply r2StateEither
            innerF
        
        let orElse (stateEither2 : 'l -> StateEither<'l, 'r, 'rs, 'us>) (stateEither1 : StateEither<'l, 'r, 'rs, 'us>) 
            (rsus : 'rs * 'us) : EitherMod.Either<'l, 'r> * ('rs * 'us) = 
            match stateEither1 rsus with
            | EitherMod.Right r, rsus -> EitherMod.Right r, rsus
            | EitherMod.Left l, rsus -> stateEither2 l rsus
        
        // #region
        // 계산이 실패했으면, 그 상태는 버린다.
        // #endregion
        let keepState (stateEither : StateEither<'l, 'r, 'rs, 'us>) : StateEither<'l, 'r, 'rs, 'us> = 
            StateMod.keepState (EitherMod.isRight) (fun either _ _ -> either) stateEither
        
        // #region
        // 특수한 용도로 쓴다. 이번 tetris 연습에서는 이 기능이 유용하다. 이와 같은 용법이 유용한 다른 경우가 있을지 모르겠다.
        // 좀 더 일반화 했다. 함수 f 의 두번째 s 는 Left 결과의 상태이고, 세번째 인자는 keepStateAndRightness 로 주어진 상태이다.
        // 실패값과 실패당시의 상태, 애초의 상태 세개를 섞어서, Right 값을 계산한다.
        // #endregion
        let keepStateAndRightness (f : 'l * 'rs * 'rs -> 'r) (stateEither : StateEither<'l, 'r, 'rs, 'us>) : StateEither<'l, 'r, 'rs, 'us> = 
            let aF (either : EitherMod.Either<'l, 'r>) (dirtyRS : 'rs) (rs : 'rs) : EitherMod.Either<'l, 'r> = 
                match either with
                | EitherMod.Left l -> EitherMod.Right(f (l, dirtyRS, rs))
                | x -> x
            StateMod.keepState (EitherMod.isRight) aF stateEither
        
        let getState (either : EitherMod.Either<'l, 'r>) (s : 's) : EitherMod.Either<'l, 's> * 's = 
            FSharpx.State.returnM (EitherMod.map (fun _ -> s) either) s
    
    module SeqMod = 
        let apply (aSeq : seq<'a>) : seq<'a -> 'b> -> seq<'b> = Seq.collect (fun f -> Seq.map f aSeq)
    
    module EitherListMod = 
        let traverse (f : 'r1 -> EitherMod.Either<'l, 'r2>) (list : List<'r1>) : EitherMod.Either<'l, List<'r2>> = 
            let rec innerF (list : List<'r1>) (result : List<'r2>) : EitherMod.Either<'l, List<'r2>> = 
                match list with
                | head :: tail -> 
                    match f head with
                    | EitherMod.Right r2 -> innerF tail (r2 :: result)
                    | EitherMod.Left l -> EitherMod.Left l
                | [] -> EitherMod.Right(List.rev result)
            innerF list []
    
    module OptionListMod = 
        let traverse (f : 'r1 -> Option<'r2>) (list : List<'r1>) : Option<List<'r2>> = 
            let rec innerF (list : List<'r1>) (result : List<'r2>) : Option<List<'r2>> = 
                match list with
                | head :: tail -> 
                    match f head with
                    | Some r2 -> innerF tail (r2 :: result)
                    | None -> None
                | [] -> Some(List.rev result)
            innerF list []
    
    module ObsMod = 
        let scan (preF : 's -> 'a -> ('s * 'a)) (scanF : 's -> 'a -> 's) (s : 's) (observable : System.IObservable<'a>) : System.IObservable<'s> = 
            let s : Ref<'s> = ref s
            let o : System.Object = new System.Object()
            
            let createF (observer : System.IObserver<'s>) : System.IDisposable = 
                let onNext (a : 'a) : unit = 
                    let lockF() : unit = 
                        let newS, a = preF !s a
                        s := scanF newS a
                        observer.OnNext !s
                    lock o lockF
                FSharp.Control.Reactive.Observable.subscribeWithCallbacks onNext observer.OnError observer.OnCompleted 
                    observable
            System.Reactive.Linq.Observable.Create createF
        
        let scanWithOld (preF : 's -> 'a -> ('s * 'a)) (scanF : 's -> 'a -> 's) (s : 's) 
            (observable : System.IObservable<'a>) : System.IObservable<'s * 's> = 
            let newS : Ref<'s> = ref s
            let o : System.Object = new System.Object()
            
            let createF (observer : System.IObserver<'s * 's>) : System.IDisposable = 
                let onNext (a : 'a) : unit = 
                    let lockF() : unit = 
                        let oldS, a = preF !newS a
                        newS := scanF oldS a
                        observer.OnNext(oldS, !newS)
                    lock o lockF
                FSharp.Control.Reactive.Observable.subscribeWithCallbacks onNext observer.OnError observer.OnCompleted 
                    observable
            System.Reactive.Linq.Observable.Create createF
        
        let mapListToObservable (list : List<'a>) : System.IObservable<'a> = 
            let createF (observer : System.IObserver<'a>) : System.IDisposable = 
                let disposable = new System.Reactive.Disposables.BooleanDisposable()
                
                // 왜 fold 를 쓰지 않나. 중간에 멈추는 것이 필요하기 때문이다.
                // 중간에 멈추는 fold 를 만들수도 있지만 당장은 이와 같이 하는 것이 더 편하다.
                let rec innerF (tail : List<'a>) : unit = 
                    if not disposable.IsDisposed then 
                        match tail with
                        | head :: tail -> 
                            observer.OnNext head
                            innerF tail
                        | [] -> ()
                innerF list
                observer.OnCompleted()
                disposable :> System.IDisposable
            System.Reactive.Linq.Observable.Create createF
    
    module StateOptionMod = 
        type StateOption<'a, 'rs, 'us> = StateMod.State<Option<'a>, 'rs, 'us>
        
        type Result<'a, 'rs, 'us> = Option<'a> * ('rs * 'us)
        
        let unit (a : 'a) : StateOption<'a, 'rs, 'us> = StateMod.unit (Some a)
        let none() : StateOption<'a, 'rs, 'us> = StateMod.unit (None)
        let some (a : 'a) : StateOption<'a, 'rs, 'us> = unit a
        
        let bind (f : 'a1 -> StateOption<'a2, 'rs, 'us>) (a1StateOption : StateOption<'a1, 'rs, 'us>) : StateOption<'a2, 'rs, 'us> = 
            let stateF (rsus : 'rs * 'us) : Option<'a2> * ('rs * 'us) = 
                match a1StateOption rsus with
                | Some a1, rsus -> f a1 rsus
                | None, rsus -> None, rsus
            stateF
        
        // 편의 기능이다. 예를 들어, 앞의 State 가 StateEither<'l, unit,'rs,'us> 라면
        // 뒤의 state 함수는 (_:unit)(state:'s):Either*'s 와 같은 형식을 갖춰야 하는데,
        // 첫째 인자 unit 은 사실상 아무것도 하는것이 없다. 따라서, 이러한 경우에 맞도록 조합기를 쓰는 것이 편하다.
        let bind4UnitState (f : StateOption<'a2, 'rs, 'us>) (a1StateOption : StateOption<unit, 'rs, 'us>) : StateOption<'a2, 'rs, 'us> = 
            bind (fun _ -> f) a1StateOption
        let map (f : 'a1 -> 'a2) : StateOption<'a1, 'rs, 'us> -> StateOption<'a2, 'rs, 'us> = bind (f >> unit)
        let apply (a1StateOption : StateOption<'a1, 'rs, 'us>) : StateOption<'a1 -> 'a2, 'rs, 'us> -> StateOption<'a2, 'rs, 'us> = 
            bind (fun f -> map f a1StateOption)
        let applyM (a1StateOption : StateOption<'a1, 'rs, 'us>) : StateOption<'a1 -> StateOption<'a2, 'rs, 'us>, 'rs, 'us> -> StateOption<'a2, 'rs, 'us> = 
            bind (fun f -> bind f a1StateOption)
        
        let lift (f : 'a1 -> 'a2 -> 'a3) : StateOption<'a1, 'rs, 'us> -> StateOption<'a2, 'rs, 'us> -> StateOption<'a3, 'rs, 'us> = 
            let innerF (a1StateOption : StateOption<'a1, 'rs, 'us>) (a2StateOption : StateOption<'a2, 'rs, 'us>) : StateOption<'a3, 'rs, 'us> = 
                unit f
                |> apply a1StateOption
                |> apply a2StateOption
            innerF
        
        let orElse (stateOption2 : unit -> StateOption<'a, 'rs, 'us>) (stateOption1 : StateOption<'a, 'rs, 'us>) 
            (rsus : 'rs * 'us) : Option<'a> * ('rs * 'us) = 
            match stateOption1 rsus with
            | Some a, rsus -> Some a, rsus
            | None, rsus -> stateOption2 () rsus
        
        let keepState (stateOption : StateOption<'a, 'rs, 'us>) : StateOption<'a, 'rs, 'us> = 
            StateMod.keepState (Option.isSome) (fun option _ _ -> option) stateOption
        
        let keepStateAndSomeness (f : 'rs * 'rs -> 'a) (stateOption : StateOption<'a, 'rs, 'us>) : StateOption<'a, 'rs, 'us> = 
            let aF (_ : Option<'a>) (dirtyRS : 'rs) (rs : 'rs) : Option<'a> = Some(f (dirtyRS, rs))
            StateMod.keepState (Option.isSome) aF stateOption
        
        let getState (option : Option<'a>) (rsus : 'rs * 'us) : Option<'rs * 'us> * ('rs * 'us) = 
            StateMod.unit (Option.map (fun _ -> rsus) option) rsus
        let getStateAsA (option : Option<'a>) (rsus : 'rs * 'us) : ('rs * 'us) * ('rs * 'us) = rsus, rsus
    
    module OptMod = 
        let apply (o : Option<'a>) : Option<'a -> 'b> -> Option<'b> = Option.bind (fun f -> Option.map f o)
